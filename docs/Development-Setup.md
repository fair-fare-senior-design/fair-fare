# Fair Fare Development Setup

* [Development Setup](#development-setup)
  * Backend
    * [Setting Up Backend](#setting-up-backend-with-docker)
    * [Running Backend](#running-backend)
  * Frontend
    * [Setting Up Frontend](#setting-up-frontend)
    * [Running Frontend](#running-frontend)
    * [How to Run iOS App on iPhone Through Xcode](#how-to-run-ios-app-on-iphone-through-xcode)
  * [Companion Site](#companion-site)
* [Git Workflow](#git-workflow)
* [Linting](#linting)
* [Populating Development Database](#populating-development-database)
* [Destroying and Rebuilding Development Database](#destroying-and-rebuilding-development-database)
* [React Native Tips](#react-native-tips)

## Development Setup

### Backend

#### Setting Up Backend With Docker
* Prerequisites:
  * [Docker](https://docs.docker.com/v17.12/docker-for-mac/install/)
    * Download includes [Docker-Compose](https://docs.docker.com/compose/).

#### Running Backend
* To spin up the database:
  ```
  cd fair-fare
  docker-compose -f docker/docker-compose.yml -f docker/docker-compose-dev.yml up --build db
  ```
* To create tables for the database with Flyway migrations:
  ```
  cd fair-fare
  docker-compose -f docker/docker-compose.yml -f docker/docker-compose-dev.yml up --build flyway
  ```
* To spin up the backend:
  ```
  cd fair-fare
  docker-compose -f docker/docker-compose.yml -f docker/docker-compose-dev.yml up --build backend
  ```

### Frontend

#### Setting Up Frontend
* Initial setup for frontend:
  ```
  brew install node
  brew install watchman
  npm install -g react-native-cli
  ```
    * You also need to install [Xcode and Xcode Command-line Tools](https://apps.apple.com/us/app/xcode/id497799835?mt=12).
    * Once Xcode and Xcode Command-line Tools have been installed, open the Xcode application and navigate to `Xcode > Preferences > Locations`.
    * Confirm that the `Locations` tab looks like the following:

      ![alt text](img/xcode-locations.png)

    * Close all Xcode windows.
    * Open `frontend/ios/frontend.xcworkspace` in Xcode, and do `File > Workspace Settings > Build System > Legacy Build System`.
    ```
    sudo gem install cocoapods
    ```


#### Running frontend
  ```
  cd fair-fare/frontend
  npm install
  cd ios
  pod install
  cd ..
  react-native link
  ```
  * Open `fair-fare/frontend/.env` file in a text editor, and set the API_URL environment variable to `http://<your IP address>:8999`:
    ```
    API_URL=
    ```
  * Open `fair-fare/frontend/ios` folder from Xcode:
    `File > Open... > `
  * Once the workspace has loaded, select the Play button:

    ![alt text](img/xcode-play-button.png)


#### How to Run iOS App on iPhone Through Xcode
1. Add your Apple ID in Xcode `Preferences` > `Accounts` > `Add Apple ID`
   ![alt text](img/xcode-preferences.png)
2. In Xcode, double-click the `frontend` directory in the left-side pane, and select `Signing and Capabilities`.
   ![alt text](img/xcode-signing.png)
3. Enable signing to "Automatically," and select the team created when you added your Apple ID in Step 1.
4. Change the Bundle Identifier to be `<your-full-name-with-dashes>-fairfare` (has to be unique).
5. Plug your iPhone into your laptop, and wait for your device to appear in the following list (my iPhone is called `SSIP`):
   ![alt text](img/xcode-simulator.png)
6. Select your phone from the list, unlock your phone, and select `Trust This Device` when it pops up.
7. On your iPhone, go to `Settings` > `General` > `Device Management`, and select `Apple Development:...`.
8. Grant trust from your device to the Xcode application running on your laptop from that screen.
9. Finally, press `Play` in Xcode!

### Companion Site

```
cd fair-fare/site
npm install
npm start
```

* `npm start` will automatically open a new browser window at `localhost:3000`, which will display the site.

## Git Workflow
  * How to clone this repository:
    ```
    git clone https://gitlab.com/fair-fare-senior-design/fair-fare.git
    ```
  * Make a new branch:
    ```
    git branch -m implement-something-new
    ```
  * Push your changes to the repository:
    ```
    git add .
    git reset docker/docker-compose.yml         // DON'T COMMIT SECRET VARIABLES TO GIT
    git commit -m "Talk about what you did here."
    git push origin implement-something-new
    ```
  * To merge your branch with Master, go to [Merge Requests](https://gitlab.com/fair-fare-senior-design/fair-fare/merge_requests) and create a new one.
  * To pull the latest changes from Master to your local copy of the repository:
    ```
    git fetch origin
    git pull
    ```


## Linting
  * Frontend
    ```
    cd frontend
    npm run lint
    ```

  * Backend
    - If using Docker:
      While your backend container is still up and running, open a new command window:
      ```
      docker exec -it docker_backend bash
      flake8
      ```


## Populating Development Database
  * Make sure you have the database running in Docker, and that you've run the Flyway container to populate its tables.
  * Restart the API with Docker as well.
  * Once the database and the API are up and running, open a new command window:
    ```
    docker exec -it docker_backend_1 bash
    python3 initialize_database.py
    ```
  * **Notes:**
    * `docker exec -it docker_backend_1 bash` is a command that allows you to enter the backend Docker container and run bash commands inside it.
    * `docker_backend_1` is the name of the container, but it could also be `docker_backend` (Docker could change it randomly).
    * `initialize_database.py` does the following:
      * Creates a new user account with the credentials found in `docker/docker-compose.yml` (you can change these if you want)
      * Reads a JSON object that can be found in `backend/src/db/initial_data.py`, which contains company and product data.
      * For each company in the list:
        * It adds the company to the database, and it returns the CompanyModel (for later access to the company's ID).
        * For each product in the company's 'products' list:
          * It adds the product to the database.

## Destroying and Rebuilding Development Database
  * Any time a database change has been pushed to master (e.g., if a migration has been changed or a new one has been added), you need to destroy your current Docker containers that are running MariaDB and Flyway.
  * Stop your running backend Docker containers (`db`, `flyway`, `backend`), and then:
  ```
  docker container ls -a            // lists all currently running or "hung" Docker containers
  ```
  ![alt text](img/docker-container-list.png)
  * Locate the `mariadb:10.3.14-bionic` and `boxfuse/flyway:2.5.4` containers by ID:
    * In my case, those are `37fd6e636b3a` and `424ba42c5e1a`.
    * Now, remove them:
    ```
    docker container rm -f 37fd6e636b3a 424ba42c5e1a
    ```
  * The next time you run the docker-compose commands from the development setup section, you will have a completely clean database.
  * Then, you should run the `initialize_database.py` file to populate your clean database.

## React Native Tips

* [Using Custom Fonts](https://hiddentao.com/archives/2017/03/10/get-custom-fonts-working-in-react-native)
* [React Native Styling Best Practices](https://thoughtbot.com/blog/structure-for-styling-in-react-native)
* [React Native Style Layouts](https://facebook.github.io/react-native/docs/flexbox)
