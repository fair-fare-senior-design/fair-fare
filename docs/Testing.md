# Testing

## Configuration

* If you want to write new tests, you must make sure the path to those tests is included in the configuration file: `fair-fare/backend/src/setup.cfg`
* For example, if your new test is located at `fair-fare/backend/src/controllers/tests`, you would add `controllers/tests` to the last line of `setup.cfg`:
  ```
  [nosetests]

  exe = True
  tests = services/tests, db/dao/tests
  ```

## How to Run Backend Tests

1. Bring up the test database:
   ```
   cd fair-fare
   docker-compose -f docker/docker-compose-test.yml up --build db
   ```
2. Bring up the test Flyway (for initializing the test database with necessary tables):
   ```
   cd fair-fare
   docker-compose -f docker/docker-compose-test.yml up --build flyway
   ```
3. Now, you are ready to run all of the backend unit tests:
   ```
   cd fair-fare
   docker-compose -f docker/docker-compose-test.yml up --build testbackend
   ```