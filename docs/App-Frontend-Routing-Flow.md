# App Frontend Routing Flow

[Redux](https://redux.js.org/introduction/getting-started) is used to keep track of the logged-in user's [JSON web token](https://jwt.io/introduction/) and associated information so that the user's credentials are only sent once to the API.
On each API call from then on, the JSON web token returned from the backend log-in endpoint will be used in place of credentials.

* [Log In](#log-in)
* [Sign Out](#sign-out)


## **Log In**
  * `frontend/App.js`
    ```javascript
    // Call checkLoggedIn() from frontend/app/services/AuthService.js.
    componentDidMount() {
      let loggedIn = checkLoggedIn();
      this.setState({loggedIn: loggedIn, checkedLoggedIn: true});
    }
    ```
  * `frontend/app/Reducers.js`
    ```javascript
    /* loginReducer takes current state and an action as parameters:
         - action: {
             type: // could be 'LOG_IN', or 'LOG_OUT'
             data: {
               'blob': // object with info about the user,
               'hashedBlob': // signed JSON web token,
               'success': // true or false
             }
           }
       If the type is 'LOG_IN', the reducer will return the JWT information for that logged-in user.
       If the type is 'LOG_OUT', the reducer will return null.
    */
    var loginReducer = function(state = {jwtInfo: null}, action) {
      switch (action.type) {
        case 'LOG_IN': {
          return {
            jwtInfo: action.data,
          }
          break;
        }
        case 'LOG_OUT': {
          return {
            jwtInfo: null,
          }
          break;
        }
        return state;
      }
    };

    export default loginReducer;
    ```
  * `frontend/store.js`
    ```javascript
    /* Creates a new Redux store that can be imported anywhere in the application */
    import { createStore } from 'redux';

    import loginReducer from './app/Reducers';
    
    const store = createStore(loginReducer);
    store.dispatch({type: 'LOG_OUT'});
    
    export default store;
    ```
  * `frontend/app/services/AuthService.js`
    ```javascript
    // Check if jwtInfo exists in Redux store (means user is logged in).
    export function checkLoggedIn() {
      let info = store.getState().jwtInfo;
    
      if (info === null) {
        // If store.getState().jwtInfo doesn't exist yet...
        return false;
      } else {
        return true;
      }
    }
    ```
  * `frontend/App.js`
    ```javascript
    // Call createRootNavigator() from Router.js, passing loggedIn variable.
    const Layout = createRootNavigator(this.state.loggedIn);
    ```
  * `frontend/app/Router.js`
    ```javascript
    // Create a navigator that switches between Auth and App layouts based on loggedIn variable
    export const createRootNavigator = (loggedIn = false) => {
      return createAppContainer(
        createSwitchNavigator(
          {
            Auth: {
              screen: AuthNavigation,
            },
            App: {
              screen: AppNavigation,
            },
          },
          {
            initialRouteName: loggedIn ? 'App' : 'Auth',
          },
        ),
      );
    };
    ```
  * `frontend/App.js`
    ```javascript
    // Render resultant layout
    return <Layout />
    ```
  * `frontend/app/navigation/AppNavigation.js`
    * Displays bottom tab navigator and defaults to homepage


## **Sign Out**
  * `frontend/app/screens/Profile/Profile.js`
    ```javascript
    // On button press, call logOut() from Auth.js and then navigate to Auth layout
    handleLogOut = () => {
        logOut();
        this.props.navigation.navigate('Auth');
    };
    ```
  * `frontend/app/services/AuthService.js`
    ```javascript
    // Log out by through a dispatch action on the Redux store
    export function logOut() {
      store.dispatch({
        type: 'LOG_OUT',
        data: null
      });
    }
    ```
