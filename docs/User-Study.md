# User Study

## Possible Survey Questions

1. Are you interested in becoming more educated on the meat and dairy industries?
2. Do you consciously consider the source of your groceries?
3. If you answered yes to any of the previous questions, would you be interested in using an app that would assist in these areas?
