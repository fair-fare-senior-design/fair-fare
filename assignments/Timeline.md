# Timeline

This timeline corresponds with our task list, which also covers our milestones. For a complete list of milestones, you can look at our [Milestones file](https://gitlab.com/fair-fare-senior-design/fair-fare/blob/master/Milestones.md).

|Summary                                         |Issue Type|Timeline|
|------------------------------------------------|----------|--------|
|Create Mockup of Site                           |Task      |11/9/2019 - 11/16/2019|
|Implement Barcode Result View                   |Task      |3/1/2020 - 3/15/2020|
|Deploy SQL Database to Amazon EC2 Instance      |Task      |1/1/2020 - 1/8/2020|
|Deploy Site to Amazon S3                        |Task      |2/1/2020 - 2/28/2020|
|Implement Navigation and Routing                |Task      |11/17/2019 - 11/24/2019|
|Create Example View                             |Task      |1/9/2020 - 1/16/2020|
|Create Example Component                        |Task      |1/9/2020 - 1/16/2020|
|Implement Navigation and Routing                |Task      |1/9/2020 - 1/16/2020|
|Create Example Data Service                     |Task      |1/1/2020 - 1/8/2020|
|Create Example DAO                              |Task      |1/1/2020 - 1/8/2020|
|Create Example Data Model                       |Task      |1/1/2020 - 1/8/2020|
|Create Example REST Endpoint                    |Task      |1/1/2020 - 1/8/2020|
|Hold Meeting About Frontend Architecture        |Task      |11/1/2019 - 11/8/2019|
|Hold Meeting About Backend Architecture         |Task      |11/1/2019 - 11/8/2019|
|Develop Data Access Objects (DAOs)              |Task      |1/9/2020 - 1/16/2020|
|Develop Data Models                             |Task      |1/9/2020 - 1/23/2020|
|Develop SQL Database Service                    |Task      |1/9/2020 - 1/16/2020|
|Determine Frontend Framework                    |Task      |11/1/2019 - 11/8/2019|
|Research Python Flask Best Practices            |Task      |11/1/2019 - 11/8/2019|
|Design algorithm for rating a product           |Task      |11/17/2019 - 11/24/2019|
|Develop REST endpoints to interact with database|Task      |2/1/2020 - 2/28/2020|
|Create Mockup of App Layout                     |Task      |11/9/2019 - 11/16/2019|
|Research UX UI design                           |Task      |11/1/2019 - 11/8/2019|
|Correspond with Local Grocery Store(s)          |Task      |3/1/2020 - 3/8/2020|
|Send Survey to Potential Users                  |Task      |11/17/2019 - 11/24/2019|
|Implement Project Members Section               |Task      |12/11/2019 - 12/18/2019|
|Implement Data Sources Section                  |Task      |12/11/2019 - 12/18/2019|
|Implement Rating Algorithm Section              |Task      |12/11/2019 - 12/18/2019|
|Implement Mission Statement Section             |Task      |12/11/2019 - 12/18/2019|
|Insert Sample Data into Database                |Task      |12/20/2019 - 12/25/2019|
|Collect Data from Sample Products               |Task      |12/11/2019 - 12/18/2019|
|Create Database Tables                          |Task      |12/11/2019 - 12/18/2019|
|Construct Database Schema                       |Task      |12/11/2019 - 12/18/2019|
|Research SQL Best Practices                     |Task      |11/1/2019 - 11/8/2019|
|Implement User Profile View                     |Task      |2/9/2020 - 2/16/2020|
|Implement Barcode Scanning                      |Task      |2/1/2020 - 2/8/2020|
|Implement Sign-Up View                          |Task      |1/17/2020 - 1/24/2020|
|Implement Login View                            |Task      |1/17/2020 - 1/24/2020|
|Research React Native Best Practices            |Task      |11/1/2019 - 11/8/2019|
