# Effort Matrix

|Summary                                         |Issue Type|Elle Effort|Miranda Effort|Sydney Effort|
|------------------------------------------------|----------|-----------|--------------|-------------|
|Create Mockup of Site                           |Task      |33         |33            |34           |
|Implement Barcode Result View                   |Task      |10         |70            |20           |
|Deploy SQL Database to Amazon EC2 Instance      |Task      |33         |33            |34           |
|Deploy Site to Amazon S3                        |Task      |0          |0             |100          |
|Implement Navigation and Routing                |Task      |0          |0             |100          |
|Create Example View                             |Task      |0          |0             |100          |
|Create Example Component                        |Task      |0          |0             |100          |
|Implement Navigation and Routing                |Task      |0          |0             |100          |
|Create Example Data Service                     |Task      |0          |0             |100          |
|Create Example DAO                              |Task      |0          |0             |100          |
|Create Example Data Model                       |Task      |0          |0             |100          |
|Create Example REST Endpoint                    |Task      |0          |0             |100          |
|Hold Meeting About Frontend Architecture        |Task      |33         |33            |34           |
|Hold Meeting About Backend Architecture         |Task      |33         |33            |34           |
|Develop Data Access Objects (DAOs)              |Task      |34         |33            |33           |
|Develop Data Models                             |Task      |50         |10            |40           |
|Develop SQL Database Service                    |Task      |100        |0             |0            |
|Determine Frontend Framework                    |Task      |0          |0             |100          |
|Research Python Flask Best Practices            |Task      |34         |33            |33           |
|Design algorithm for rating a product           |Task      |34         |33            |33           |
|Develop REST endpoints to interact with database|Task      |51         |0             |49           |
|Create Mockup of App Layout                     |Task      |0          |100           |0            |
|Research UX UI design                           |Task      |10         |80            |10           |
|Correspond with Local Grocery Store(s)          |Task      |33         |33            |34           |
|Send Survey to Potential Users                  |Task      |33         |33            |34           |
|Implement Project Members Section               |Task      |33         |33            |34           |
|Implement Data Sources Section                  |Task      |33         |33            |34           |
|Implement Rating Algorithm Section              |Task      |33         |33            |34           |
|Implement Mission Statement Section             |Task      |33         |33            |34           |
|Insert Sample Data into Database                |Task      |33         |33            |34           |
|Collect Data from Sample Products               |Task      |33         |33            |34           |
|Create Database Tables                          |Task      |10         |10            |80           |
|Construct Database Schema                       |Task      |0          |0             |100          |
|Research SQL Best Practices                     |Task      |20         |20            |60           |
|Implement User Profile View                     |Task      |10         |70            |20           |
|Implement Barcode Scanning                      |Task      |10         |70            |20           |
|Implement Sign-Up View                          |Task      |10         |70            |20           |
|Implement Login View                            |Task      |10         |70            |20           |
|Research React Native Best Practices            |Task      |33         |34            |33           |
