# User-Stories

1. As a consumer, I want to see a rating based on ethical and environmental impact for the products I am buying, so that I can be a more conscious consumer.

2. As a user, I want to have access to the thought process behind the rating system, so that I can be more informed on the impact these products have.

3. As a user, I want to use my mobile device to see these ratings, so that I can use this at my own convenience.

4. As a local merchant, I want community members to be informed on the integrity of my products, so that they know they have the option to buy ethically sourced products.

5. As a vendor, I want access to the information that curates my products' ratings, so that I can see how legitimate the ratings system is. 