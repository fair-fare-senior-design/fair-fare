# Milestones

This page details our list of milestones. To see a timeline for these events, please visit our [timeline.](https://gitlab.com/fair-fare-senior-design/fair-fare/blob/master/Timeline.md)

[Database](#datbase)  
[Backend](#backend)   
[Frontend](#frontend)   
[User Study](#user-study)   
[Static Site](#static-site)   


## Database
- Put sample product data into SQL database. The main product categories we want to include are chicken, turkey, beef, pork, fish, eggs, milk, and yogurt. We want to have somewhere between 3-5 different products for each category.


## Backend
- Backend architecture is decided and modeled. This is an early milestone that we want to have completed before any implementation is started.
- Product rating algorithm is defined.
- User login and signup backend functionality is complete.
- Product view backend functionality is complete.
- Company view backend functionality is complete.
 -Deploy the backend.

## Frontend
- Frontend architecture is decided and modeled. This is an early milestone that we want to have completed before any implementation is started.
- Mockup of iOS app is complete.
- App navigation and routing are implemented.
- User login and sign-up pages are complete.
- Barcode scanning on frontend is implemented.
- Product information page is complete.
- Company information page is complete.
- Deploy frontend.

## User Study
- Make and send out user surveys. This will include questions about user interest in ethically-sourced meat and dairy products.

## Static Site
- Mockup of website is complete.
- Site navigation and routing are implemented. We will be using parallax.
- Mission statement section is complete.
- Why It Matters section is complete.
- Data sources and rating algorithm is complete.
- Project members information section is complete.
- Deploy the site.
