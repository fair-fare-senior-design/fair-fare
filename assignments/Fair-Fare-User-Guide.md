# Fair Fare User Guide
Last Updated February 1, 2020


## Table of Contents
* [Overview](#overview)
* [Getting Started](#getting-started)
  * [Signing Up](#signing-up)
  * [Requirements](#requirements)
    * [Email and Username](#email-and-username)
    * [Password](#password)
  * [Logging In](#logging-in)
* [Using the App](#using-the-app)  
  * [Home Page](#home-page)
  * [Product Search](#product-search)
    * [Product Details](#product-details)
  * [Barcode Scanner](#barcode-scanner)
  * [Favorites](#favorites)
  * [Profile](#profile)
* [Customization](#customization)
  * [Updating Your Profile](#updating-your-profile)
    * [Deleting Your Account](#deleting-your-account)
  * [Customizing the Rating Algorithm](#customizing-the-rating-algorithm)




## Overview
Fair Fare strives to be a user-friendly application that provides a simple way to view the quality of common meat and dairy products. This document’s purpose is to guide you through the set-up and usage of the Fair Fare application so that you can start (or continue) your path to becoming a more conscious consumer.


## Getting Started
Upon opening the app, you will be prompted to create an account or will be given the option at the bottom of the screen to log in.

### Signing up
To create an account with Fair Fare, you will need a valid email, a custom username, and a strong password. Fill out the sign-up form with this information and click the “submit” button to create your account. Upon submitting your new account details, you will be asked to log in to begin using Fair Fare.

### Requirements
If any of your information does not meet the requirements, the field box with appear red to indicate the error.

#### Email and Username
The only additional requirements for the email and password fields are that they must be unique in the Fair Fare database. In other words, they cannot already be in use with Fair Fare. If your desired email or username is already taken, an error message will appear indicating that the email or username is already in use.

#### Password
In order to help you create a more secure password, Fair Fare requires that your password be at least 8 characters long including at least 1 uppercase letter, 1 lowercase letter, and 1 number. If you fail to meet these requirements during the sign-up process, an error message will appear to remind you of the password requirements.

### Logging In
If you have an existing account with Fair Fare, you can head to the log-in page by clicking the “Login In” button at the bottom of the sign-up screen. Enter your username and password and click the “Log In” button to sign in to your Fair Fare account. If you do not enter a matching username and password combination, the field boxes with light up red to indicate that an existing account does not exist with the credentials that you entered. For security purposes, the error will not indicate which field is incorrect.


## Using the App
Once you are logged into Fair Fare, you will see a menu at the bottom of the screen with 5 tabs. More details for these tabs are listed below.

### Home Page
After logging in to the app, the first page that will be displayed is the home page. This page provides Fair Fare’s mission statement and a link to learn more about the thought process behind Fair Fare.

### Product Search
After logging in, tap the search icon in the bottom navigation bar. You will be redirected to a search page. Here, you have the option to search for a company name, product name, or person. Select whichever category best fits your search, then type your keyword into the search bar. If it is in our database, the product name will be returned under the search bar. You can then click on the name to see the Product Details page.

### Person Search
After logging in, tap the search icon in the bottom navigation bar. You will be redirected to a search page. Here, you have the option to search for a company name, product name, or person. Select 'Person' then type the name of the person you would like to search into the search bar. If it is in our database, the person will be returned under the search bar. You can then click on the name to follow them and view their profile and favorites.

#### Product Details
Upon searching or scanning a product within the app, you will be redirected to the Product Details page. This page will show a photo of the product, its name, overall rating, along with the environmental, ethical, and health ratings. If there is any unknown information regarding the environmental, ethical, or health ratings, this product will be flagged and you will see a pop-up that informs you of the missing data. 

### Barcode Scanner
After logging in, tap the center icon in the bottom navigation bar. This will open a camera view. Align your camera with the barcode that you would like to scan. When the barcode is scanned, you will be redirected to the Product Details page for the scanned item.

### Favorites
While looking at a product details page, click the heart icon in the top right corner to add the product to your favorites. In order to view all favorited products, click the heart icon in the bottom navigation bar. This will redirect you to a page of your favorited products.

### Profile
Your profile page is a way for you to share more about you and your journey to conscious consumption. Your profile can display your username or desired name, the date you joined, a short bio, your favorite brands, and an avatar image. The list of your favorite brands is generated based on the brands you save to your favorites most often. On your profile page, you will also find options to log out of your account and update your profile near the top of the screen.


### Updating Your Profile
To update your profile, click the “Edit Profile” button in the top right corner of the profile page. Here, you will be able to upload a profile picture, edit your display name, and change your bio. If you do not wish to change your display name, your username will be shown as the default.

#### Deleting Your Account
If you wish to delete your account, click the “Edit Profile” button in the top right corner of the profile page. Click the “Delete My Account” button at the bottom of the page. You will see a pop-up asking you to confirm the deletion of your account.

