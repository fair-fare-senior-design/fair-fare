# Senior Design: Fair Fare

* [Contact Information](#contact-information)
* [Professional Biography](#professional-biography)
* [Our Goals](#our-goals)
* [Project Outline](#project-outline)

## Contact Information
[Miranda Dolan](#miranda-dolan)
* dolanmk@mail.uc.edu
* (419)-302-2757

***

[Ellen Russo](#ellen-russo)
* russoen@mail.uc.edu
* (513)-582-4619

***

[Sydney Stallman](#sydney-stallman)
* stallmsa@mail.uc.edu
* (859)-496-7707

***

Faculty Advisor: Professor Nan Niu
* niunn@mail.uc.edu

## Professional Biography

#### Miranda Dolan
I have spent all five of my co-op rotations at Siemens Software. In my time there I worked in three separate organizations, each focusing on different products and goals.

In my first two rotations, I was the designated lead on pilot project for an aggregated logging solution which included creating the proof of concept for implementing a specific external application, maintaining and customizing that application to fit the organization's needs, and exploring potential adaptations for the application.

In my third rotation, I participated in the development process of the new sketching environment for Siemens’s CAD software,
NX, by creating educational media to demonstrate functionality, collecting and analyzing user data, and providing usability feedback to product definition engineers.

In my fourth and fifth rotations, I contributed to the core development of Siemens’s CAE software, Simcenter, by committing effective and maintainable object-oriented code for production, reducing defects in legacy code, and working as a developer on a highly collaborative Scrum team.

#### Ellen Russo
My most recent co-op was at Ethicon Endo-Surgery in the R&D Department of Energy. I worked at Ethicon as a software engineer from January of 2019 to August 2019. In the first part of this co-op I got to explore the world of embedded systems. I used a Xilinx board, object-oriented C++, RTI Data Distribution Services, and QNX operating system to transfer data between electrosurgical devices in a timely and secure manner, test timing of message transmission, and find solutions to ethernet connectivity issues. As I completed my embedded systems projects, I began to developing software on a raspberry pi for use in our team prototype. While developing on the raspberry pi, I worked on integration of the team prototype and an external visualization system by pursuit of RTI Connext Data Distribution Services and OpenDDS interoperability on the project's backplane.

Prior to my co-op at Ethicon, I worked at Cincinnati Children's Hospital in the Department of Behavioral & Developmental Neuropsychiatry as a biomedical software engineer from August 2017- January 2019. At Children's, I developed a user-facing application to automate the cleaning and visualization of electroencephalograph data prior to analysis. I also built paradigms for use research visits, where we primarily researched Fragile X syndrome. I also designed and built custom hardware for use in the neurophysiology lab.

#### Sydney Stallman

I completed all 5 of my co-ops with [Siemens Software Inc.](https://www.plm.automation.siemens.com/global/en/), all with the Security Test Tools team under the Lifecycle Collaboration Software group.

My first rotation was from January 2017 to May 2017. I gained introductory experience with Agile software methodology, and I resolved legacy C++ defects in the code base.

My second rotation was from August 2017 to December 2017. I developed a UI for an internal code coverage tool, implemented the RESTful API with Python Flask, and developed the frontend with Angular 4.

My third rotation was from May 2018 to August 2018. I cultivated my backend web development experience, implemented Python unit testing, enhanced project documentation, and added style linters.

My double rotation was from January 2019 to August 2019. I refactored and added several new features to an existing web development project. The tech stack was a ReactJS frontend, Python Flask API, and Neo4j graphical database. I implemented Docker + GitLab CI/CD, gained experience with Jenkins for automated tasks, and enhanced my presentation skills.

## Our Goals

For our project we seek to gain experience in both web and mobile development by implementing a database, API, and user-friendly front-end using the Scrum framework to execute the Agile development methodology.

## Project Outline

Title: Fair Fare

### Project Background Description

The project will require the team to create a website and a mobile app. The website will be static and consist mostly of information pertaining to the project such as algorithms used and data sources. The mobile app will be made up of a user-friendly front-end, a SQL database on the back-end, and a RESTful API that will sit in the middle.

### Project Problem Statement

The industries behind meat, fish, and dairy products have a massive impact on the environment and often raise ethical concerns. Consumers want to know where their products are coming from and how they are manufactured.

### Inadequacy of Current Solutions to Problem

Labels on certain products can often be misleading to consumers. We wanted to provide a way to disseminate all of the data currently available into the consumers' hands.

### Background Skills/Interests Applicable to Problem
  * SQL database development
  * Python RESTful API development
  * iOS application development (Swift)
  * Algorithm development

### Your Project Team Approach to Problem

Our team will create an iOS app that allows users to scan products such as meat, fish, and dairy by their barcodes and see some sort of rating that represents the integrity of the product. That rating will be based on a multitude of factors such as the product's carbon footprint and the ethics of the source company. Most of the data will be gathered from existing databases, which the team will then consolidate and use with the rating algorithm. The website will be created primarily to inform users on the team's project and the reasoning behind it.

The goal of the project is to advance our skills in database implementation and IOS development, as well as, create a deliverable that educates consumers and supports ethical manufactures. 

By the end of the project, our team aims to demo the application by scanning variations of the same product and displaying the different ratings with an explanation behind the differences.