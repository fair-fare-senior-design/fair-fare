FROM node:latest

RUN npm install -g grunt-cli \
    && npm install -g brunch \
    && npm install -g bower \
    && npm install -g yarn \
    && npm install -g create-react-native-app \
    && npm install -g react-native-cli \
    && npm install -g expo-cli

RUN groupadd -g 459 dockeruser && \
    useradd -r -u 459 -g dockeruser -m dockeruser

USER dockeruser

RUN mkdir /home/dockeruser/app

WORKDIR /home/dockeruser/app

COPY ./package.json /home/dockeruser/app

RUN npm install

COPY . /home/dockeruser/app

EXPOSE 3000
EXPOSE 19000
EXPOSE 19001
