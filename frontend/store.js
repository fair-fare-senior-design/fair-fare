import {createStore} from 'redux';

import loginReducer from './app/Reducers';

const store = createStore(loginReducer);
store.dispatch({type: 'LOG_OUT'});

export default store;
