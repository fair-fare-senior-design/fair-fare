import React from 'react';
import {
  View,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  Modal,
  Alert,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';

import store from '../../../store';
import theme from '../../Theme';
import styles from './Styles';

import {getAccount} from '../../services/UserService';

import UserModal from '../UserModal/UserModal';

export default class FolloweeModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      followees: this.props.followees,
      selectedFollowee: null,
      userModalVisible: false,
      isLoading: false,
    };
  }

  handleSelectFollowee = followee => {
    const options = {
      jwtInfo: store.getState().jwtInfo,
      username: followee.username,
    };

    const self = this;

    getAccount(options, function(response) {
      self.setState({
        selectedFollowee: response.body,
        userModalVisible: true,
      });
    });
  };

  closeUserModal = () => {
    this.setState({
      selectedFollowee: null,
      userModalVisible: false,
    });
  };

  formatFollowees = () => {
    return this.state.followees.map((followee, index) => (
      <View style={styles.followee__container} key={index}>
        <TouchableOpacity onPress={() => this.handleSelectFollowee(followee)}>
          <View style={styles.followee__view}>
            <View style={styles.followee__image__view}>
              <Image
                style={styles.user__image}
                source={{uri: followee.profilePictureUrl}}
              />
            </View>
            <View style={styles.followee__user__view}>
              <Text style={styles.user__text__title}>{followee.firstName}</Text>
              <Text style={styles.user__text__subtitle}>
                @{followee.username}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    ));
  };

  render() {
    const followees = this.formatFollowees();

    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
        <View style={styles.modal__view}>
          {this.state.userModalVisible ? (
            <UserModal
              jwtInfo={store.getState().jwtInfo}
              user={this.state.selectedFollowee}
              showActivityIndicator={this.state.showActivityIndicator}
              modalVisible={this.state.userModalVisible}
              closeModal={this.closeUserModal}
            />
          ) : (
            <>
              <View style={styles.modal__header__view}>
                <View style={styles.modal__header}>
                  <View style={styles.close__button__view}>
                    <TouchableOpacity onPress={() => this.props.closeModal()}>
                      <Icon name="close" size={30} color={theme.LIGHT_GREY} />
                    </TouchableOpacity>
                  </View>
                  <View style={styles.modal__title__view}>
                    <Text style={styles.modal__title__text}>Following</Text>
                  </View>
                </View>
              </View>
              <SafeAreaView style={styles.modal__body__view}>
                <View style={styles.scroll__body__view}>
                  <ScrollView contentContainerStyle={styles.scroll__body}>
                    {followees}
                  </ScrollView>
                </View>
              </SafeAreaView>
            </>
          )}
        </View>
      </Modal>
    );
  }
}
