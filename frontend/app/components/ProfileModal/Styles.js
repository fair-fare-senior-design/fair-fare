import {StyleSheet} from 'react-native';

import theme from '../../Theme';

const styles = StyleSheet.create({
  modal__view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.LIGHT_GREY,
  },
  modal__view__grey: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.DARK_GREEN,
  },
  modal__loader: {
    // backgroundColor: theme.DARK_GREY,
  },
  modal__header__view: {
    flex: 1,
    flexDirection: 'row',
    width: '100%',
  },
  modal__body__view: {
    flex: 8,
    flexDirection: 'column',
    width: '100%',
  },
  modal__footer__view: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cancel__button__view: {
    backgroundColor: theme.LIGHT_GREY,
    width: '33.3%',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  picture__view: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  modal__title__view: {
    width: '33.3%',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  save__button__view: {
    backgroundColor: theme.LIGHT_GREY,
    width: '33.3%',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  cancel__button__text: {
    fontSize: 17.5,
    fontFamily: 'SFProDisplay-Light',
    color: theme.LIGHT_ORANGE,
    padding: 2.5,
  },
  delete__account__button__text: {
    fontSize: 17.5,
    fontFamily: 'SFProDisplay-Bold',
    color: theme.ERROR_RED,
    padding: 5,
  },
  modal__title__text: {
    fontSize: 17.5,
    fontFamily: 'SFProDisplay-Bold',
    color: theme.DARK_GREY,
    padding: 2.5,
  },
  save__button__text: {
    fontSize: 17.5,
    fontFamily: 'SFProDisplay-Light',
    color: theme.LIGHT_ORANGE,
    padding: 2.5,
  },
  modal__form__container__view: {
    width: '100%',
    marginTop: 40,
  },
  form__view: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    margin: 20,
  },
  form__label__view: {
    flex: 1,
  },
  form__input__view: {
    flex: 3,
  },
  form__blank__view: {
    flex: 1,
  },
  form__label__text: {
    fontSize: 15,
    fontFamily: 'SFProDisplay-Bold',
    color: theme.DARK_GREY,
    padding: 2.5,
  },
  form__input: {
    borderBottomColor: theme.DARK_GREY,
    borderBottomWidth: StyleSheet.hairlineWidth,
    fontSize: 15,
    fontFamily: 'SFProDisplay-Light',
    color: theme.DARK_GREY,
    padding: 2.5,
  },
  modal__loading__text: {
    fontSize: 17.5,
    fontFamily: 'SFProDisplay-Bold',
    color: theme.LIGHT_GREY,
    padding: 2.5,
  },
  picture__button: {
    height: 150,
    width: 150,
  },
  user__image: {
    flex: 1,
    width: '100%',
    height: undefined,
    aspectRatio: 1,
    borderRadius: 150 / 2,
  },
});

export default styles;
