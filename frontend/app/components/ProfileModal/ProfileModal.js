import React from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Modal,
  Alert,
  Image,
  ActivityIndicator,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';

import theme from '../../Theme';
import styles from './Styles';

import {logOut} from '../../services/AuthService';
import {deleteAccount} from '../../services/UserService';

export default class ProfileModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: this.props.firstName,
      bio: this.props.bio,
      chosenImage: {
        uri: this.props.profilePictureUrl,
      },
    };
  }

  saveUserData = () => {
    let options = {
      jwtInfo: this.props.jwtInfo,
      user: {
        firstName: this.state.firstName,
        bio: this.state.bio,
      },
      picture: this.state.chosenImage,
    };

    this.props.handleUpdateUser(options);
  };

  openDeleteAccountAlert = () => {
    Alert.alert('Delete Account', 'Are you absolutely sure?', [
      {
        text: 'Yes',
        onPress: () => this.callDeleteAccount(),
      },
      {
        text: 'No',
      },
    ]);
  };

  callDeleteAccount = () => {
    const filter = {
      jwtInfo: this.props.jwtInfo,
    };

    const self = this;

    deleteAccount(filter, function(response) {
      logOut();
      self.props.navigation.navigate('Auth');
    });
  };

  showImageSelector = () => {
    const options = {
      title: 'Select Avatar',
      customButtons: [],
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const image = {
          data: {uri: 'data:image/jpeg;base64,' + response.data},
          uri: response.uri,
          type: response.type,
        };

        this.setState({chosenImage: image});
      }
    });
  };

  render() {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
        <View
          style={
            this.props.showActivityIndicator
              ? styles.modal__view__grey
              : styles.modal__view
          }>
          {this.props.showActivityIndicator ? (
            <View style={styles.modal__loader}>
              <ActivityIndicator size="large" color="rgb(242, 239, 234)" />
              <Text style={styles.modal__loading__text}>
                Updating Your Profile...
              </Text>
            </View>
          ) : (
            <>
              <View style={styles.modal__header__view}>
                <View style={styles.cancel__button__view}>
                  <TouchableOpacity onPress={() => this.props.closeModal()}>
                    <Text style={styles.cancel__button__text}>Cancel</Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.modal__title__view}>
                  <Text style={styles.modal__title__text}>Edit Profile</Text>
                </View>
                <View style={styles.save__button__view}>
                  <TouchableOpacity onPress={() => this.saveUserData()}>
                    <Text style={styles.save__button__text}>Save</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.modal__body__view}>
                <View style={styles.modal__form__container__view}>
                  <View style={styles.picture__view}>
                    <View style={styles.picture__view}>
                      <TouchableOpacity
                        style={styles.picture__button}
                        onPress={() => this.showImageSelector()}>
                        <Image
                          style={styles.user__image}
                          source={{uri: this.state.chosenImage.uri}}
                        />
                      </TouchableOpacity>
                      <Text style={styles.form__label__text}>
                        Change Profile Picture
                      </Text>
                    </View>
                    <View style={styles.form__blank__view} />
                  </View>
                  <View style={styles.form__view}>
                    <View style={styles.form__label__view}>
                      <Text style={styles.form__label__text}>First Name</Text>
                    </View>
                    <View style={styles.form__input__view}>
                      <TextInput
                        style={styles.form__input}
                        placeholder="Edit your first name"
                        placeholderTextColor={theme.DARK_GREY}
                        value={this.state.firstName}
                        onChangeText={text => this.setState({firstName: text})}
                        autoCapitalize="none"
                      />
                    </View>
                    <View style={styles.form__blank__view} />
                  </View>
                  <View style={styles.form__view}>
                    <View style={styles.form__label__view}>
                      <Text style={styles.form__label__text}>Bio</Text>
                    </View>
                    <View style={styles.form__input__view}>
                      <TextInput
                        style={styles.form__input}
                        placeholder="Edit your bio"
                        placeholderTextColor={theme.DARK_GREY}
                        value={this.state.bio}
                        onChangeText={text => this.setState({bio: text})}
                        autoCapitalize="none"
                        multiline={true}
                      />
                    </View>
                    <View style={styles.form__blank__view} />
                  </View>
                </View>
              </View>
              <View style={styles.modal__footer__view}>
                <TouchableOpacity onPress={() => this.openDeleteAccountAlert()}>
                  <Text style={styles.delete__account__button__text}>
                    Delete Account
                  </Text>
                </TouchableOpacity>
              </View>
            </>
          )}
        </View>
      </Modal>
    );
  }
}
