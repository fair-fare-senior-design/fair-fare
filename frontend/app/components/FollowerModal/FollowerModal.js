import React from 'react';
import {
  View,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  Modal,
  Alert,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';

import store from '../../../store';
import theme from '../../Theme';
import styles from './Styles';

import {getAccount} from '../../services/UserService';

import UserModal from '../UserModal/UserModal';

export default class FollowerModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      followers: this.props.followers,
      selectedFollower: null,
      userModalVisible: false,
      isLoading: false,
    };
  }

  componentDidMount() {}

  handleSelectFollower = follower => {
    const options = {
      jwtInfo: store.getState().jwtInfo,
      username: follower.username,
    };

    const self = this;

    getAccount(options, function(response) {
      self.setState({
        selectedFollower: response.body,
        userModalVisible: true,
      });
    });
  };

  closeUserModal = () => {
    this.setState({
      selectedFollower: null,
      userModalVisible: false,
    });
  };

  formatFollowers = () => {
    return this.state.followers.map((follower, index) => (
      <View style={styles.follower__container} key={index}>
        <TouchableOpacity onPress={() => this.handleSelectFollower(follower)}>
          <View style={styles.follower__view}>
            <View style={styles.follower__image__view}>
              <Image
                style={styles.user__image}
                source={{uri: follower.profilePictureUrl}}
              />
            </View>
            <View style={styles.follower__user__view}>
              <Text style={styles.user__text__title}>{follower.firstName}</Text>
              <Text style={styles.user__text__subtitle}>
                @{follower.username}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    ));
  };

  render() {
    const followers = this.formatFollowers();

    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
        <View style={styles.modal__view}>
          {this.state.userModalVisible ? (
            <UserModal
              jwtInfo={store.getState().jwtInfo}
              user={this.state.selectedFollower}
              showActivityIndicator={this.state.showActivityIndicator}
              modalVisible={this.state.userModalVisible}
              closeModal={this.closeUserModal}
            />
          ) : (
            <>
              <View style={styles.modal__header__view}>
                <View style={styles.modal__header}>
                  <View style={styles.close__button__view}>
                    <TouchableOpacity onPress={() => this.props.closeModal()}>
                      <Icon name="close" size={30} color={theme.LIGHT_GREY} />
                    </TouchableOpacity>
                  </View>
                  <View style={styles.modal__title__view}>
                    <Text style={styles.modal__title__text}>Followers</Text>
                  </View>
                </View>
              </View>
              <SafeAreaView style={styles.modal__body__view}>
                <View style={styles.scroll__body__view}>
                  <ScrollView contentContainerStyle={styles.scroll__body}>
                    {followers}
                  </ScrollView>
                </View>
              </SafeAreaView>
            </>
          )}
        </View>
      </Modal>
    );
  }
}
