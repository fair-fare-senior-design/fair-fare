import {StyleSheet} from 'react-native';

import theme from '../../Theme';

const styles = StyleSheet.create({
  modal__view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  modal__header__view: {
    flex: 0.15,
    backgroundColor: theme.LIGHT_ORANGE,
    width: '100%',
  },
  modal__header: {
    flexDirection: 'row',
    paddingTop: '10%',
  },
  modal__subheader: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modal__title__text: {
    fontSize: 17.5,
    fontFamily: 'SFProDisplay-Bold',
    color: theme.LIGHT_GREY,
    padding: 2.5,
  },
  modal__subheader__text: {
    fontSize: 20,
    fontFamily: 'SFProDisplay-Bold',
    color: theme.LIGHT_GREY,
    padding: 10,
  },
  close__button__view: {
    width: '20%',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    padding: '5%',
  },
  modal__title__view: {
    width: '60%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: '5%',
    paddingBottom: '5%',
  },
  modal__body__view: {
    flex: 1,
    backgroundColor: theme.LIGHT_GREY,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  scroll__body: {
    paddingVertical: 10,
    width: '100%',
  },
  scroll__body__view: {
    flex: 1,
    width: '100%',
  },
  follower__container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  follower__view: {
    flex: 0.25,
    flexDirection: 'row',
    backgroundColor: theme.LIGHTEST_ORANGE,
    padding: 10,
    margin: 5,
    borderRadius: 10,
    width: '85%',
  },
  follower__image__view: {
    flex: 0.25,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  follower__user__view: {
    flex: 0.75,
    alignItems: 'flex-start',
    justifyContent: 'center',
    margin: 15,
  },
  user__image: {
    flex: 1,
    width: '100%',
    height: undefined,
    aspectRatio: 1,
    borderRadius: 150 / 2,
  },
  user__text__title: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 17,
  },
  user__text__subtitle: {
    fontFamily: 'SFProDisplay-Light',
    fontSize: 16,
  },
});

export default styles;
