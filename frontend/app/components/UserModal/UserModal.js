import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Modal,
  Alert,
  ImageBackground,
  FlatList,
  ActivityIndicator,
} from 'react-native';

import store from '../../../store';
import theme from '../../Theme';
import styles from './Styles';

import {
  follow,
  unfollow,
  checkFollowing,
} from '../../services/FollowingService';
import {getAccount} from '../../services/UserService';
import {getFavorites} from '../../services/FavoriteService';

import FollowerModal from '../FollowerModal/FollowerModal';
import FolloweeModal from '../FolloweeModal/FolloweeModal';

export default class UserModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {
        firstName: this.props.user.firstName,
        username: this.props.user.username,
        pictureUrl: this.props.user.pictureUrl,
        dateJoined: this.props.user.dateJoined,
        bio: this.props.user.bio,
        followerCount: this.props.user.followerCount,
        followeeCount: this.props.user.followeeCount,
        followers: this.props.user.followers,
        followees: this.props.user.followees,
        favoriteBrands: [],
      },
      userLoaded: false,
      isFollowing: false,
      followerModalVisible: false,
      followeeModalVisible: false,
    };
  }

  componentDidMount() {
    if (this.props.user !== null) {
      const options = {
        jwtInfo: store.getState().jwtInfo,
        username: this.state.user.username,
      };

      const self = this;

      checkFollowing(
        options,
        function(response) {
          self.setState(
            {
              isFollowing: response.body.isFollowing
            },
            function() {
              getFavorites(options, function(result) {
                console.log(result);
                if (result.status === 200) {
                  self.setState(prevState => ({
                    user: {
                      ...prevState.user,
                      favoriteBrands: result.body,
                    },
                    userLoaded: true,
                  }));
                }
              });
            }
          );
        },
      );
    }
  }

  unfollowUser = () => {
    const options = {
      jwtInfo: this.props.jwtInfo,
      username: this.state.user.username,
    };

    const self = this;

    unfollow(options, function(response) {
      self.setState({isFollowing: false}, function() {
        getAccount(options, function(result) {
          if (result.status === 200) {
            self.setState(prevState => ({
              user: {
                ...prevState.user,
                followeeCount: result.body.followeeCount,
                followerCount: result.body.followerCount,
                followees: result.body.followees,
                followers: result.body.followers,
              },
              userLoaded: true,
            }));
          }
        });
      });
    });
  };

  followUser = () => {
    const options = {
      jwtInfo: this.props.jwtInfo,
      username: this.state.user.username,
    };

    const self = this;

    follow(options, function(response) {
      self.setState({isFollowing: true}, function() {
        getAccount(options, function(result) {
          if (result.status === 200) {
            self.setState(prevState => ({
              user: {
                ...prevState.user,
                followeeCount: result.body.followeeCount,
                followerCount: result.body.followerCount,
                followees: result.body.followees,
                followers: result.body.followers,
              },
              userLoaded: true,
            }));
          }
        });
      });
    });
  };

  checkIfUserLoggedIn = () => {
    if (this.state.user.username === store.getState().jwtInfo.blob.username) {
      return true;
    } else {
      return false;
    }
  };

  formatFavoriteBrands = () => {
    let favoriteBrands = [];

    for (let i = 0; i < this.state.user.favoriteBrands.length; i++) {
      favoriteBrands.push({key: this.state.user.favoriteBrands[i].company});
    }

    return favoriteBrands;
  };

  closeFollowerModal = () => {
    this.setState({followerModalVisible: false});
  };

  closeFolloweeModal = () => {
    this.setState({followeeModalVisible: false});
  };

  render() {
    let username = '@' + this.state.user.username;
    let displayName = this.state.user.username;

    if (this.state.user.firstName !== '') {
      displayName = this.state.user.firstName;
    }

    let dateJoined = this.state.user.dateJoined;
    let parts = dateJoined.split(' ');
    let dateBlurb = 'Joined ' + parts[2] + ' ' + parts[3];

    let favoriteBrands = this.formatFavoriteBrands();
    let userLoggedIn = this.checkIfUserLoggedIn();

    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
        <View style={styles.container__view}>
          {this.state.userLoaded ? (
            <>
              {this.state.followerModalVisible ? (
                <FollowerModal
                  jwtInfo={store.getState().jwtInfo}
                  followers={this.state.user.followers}
                  showActivityIndicator={this.state.showActivityIndicator}
                  modalVisible={this.followerModalVisible}
                  closeModal={this.closeFollowerModal}
                />
              ) : (
                <></>
              )}
              {this.state.followeeModalVisible ? (
                <FolloweeModal
                  jwtInfo={store.getState().jwtInfo}
                  followees={this.state.user.followees}
                  showActivityIndicator={this.state.showActivityIndicator}
                  modalVisible={this.followeeModalVisible}
                  closeModal={this.closeFolloweeModal}
                />
              ) : (
                <></>
              )}
              <View style={styles.body__view}>
                <View style={styles.body__view__top}>
                  <View style={styles.user__image__view}>
                    <ImageBackground
                      style={styles.user__image}
                      source={{uri: this.state.user.pictureUrl}}>
                      <TouchableOpacity
                        style={styles.back__button}
                        onPress={() => this.props.closeModal()}>
                        <Text style={styles.back__button__text}>Go Back</Text>
                      </TouchableOpacity>
                    </ImageBackground>
                  </View>
                </View>
                <View style={styles.body__view__bottom}>
                  <View style={styles.user__view}>
                    <View style={styles.user__title__view}>
                      <Text style={styles.user__title}>{displayName}</Text>
                    </View>
                  </View>
                  <View style={styles.user__subtitle__view}>
                    <Text style={styles.user__subtitle}>{username}</Text>
                  </View>
                  <View style={styles.following__view}>
                    <TouchableOpacity
                      onPress={() => this.setState({followerModalVisible: true})}>
                      <Text style={styles.following__text}>
                        {this.state.user.followerCount} Followers
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => this.setState({followeeModalVisible: true})}>
                      <Text style={styles.following__text}>
                        {this.state.user.followeeCount} Following
                      </Text>
                    </TouchableOpacity>
                    {userLoggedIn ? (
                      <></>
                    ) : this.state.isFollowing ? (
                      <TouchableOpacity
                        style={styles.unfollow__button}
                        onPress={() => this.unfollowUser()}>
                        <Text style={styles.unfollow__button__text}>Following</Text>
                      </TouchableOpacity>
                    ) : (
                      <TouchableOpacity
                        style={styles.follow__button}
                        onPress={() => this.followUser()}>
                        <Text style={styles.follow__button__text}>Follow</Text>
                      </TouchableOpacity>
                    )}
                  </View>
                  <View style={styles.date__view}>
                    <Text style={styles.date__title}>{dateBlurb}</Text>
                  </View>
                  <View style={styles.bio__view}>
                    <Text style={styles.bio__title}>{this.state.user.bio}</Text>
                  </View>
                  <View style={styles.favorites__view}>
                    <Text style={styles.favorites__title}>Favorite Brands</Text>
                    <FlatList
                      data={favoriteBrands}
                      renderItem={({item}) => (
                        <Text style={styles.favorites__item}>{item.key}</Text>
                      )}
                    />
                  </View>
                </View>
              </View>
            </>
          ) : (
            <View style={styles.modal__loader}>
              <ActivityIndicator size="large" color={theme.DARK_GREEN} />
              <Text style={styles.modal__loading__text}>
                Loading User...
              </Text>
            </View>
          )}
        </View>
      </Modal>
    );
  }
}
