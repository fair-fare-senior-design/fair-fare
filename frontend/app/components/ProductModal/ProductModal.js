import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Modal,
  Alert,
  Image,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import CommunityIcon from 'react-native-vector-icons/dist/MaterialCommunityIcons';

import theme from '../../Theme';
import styles from './Styles';

import {
  addFavorite,
  removeFavorite,
  checkFavorite,
} from '../../services/FavoriteService';
import {getProductByBarcode} from '../../services/ProductService';

export default class ProductModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      product: null,
      isFavorite: null,
      isLoading: this.props.showActivityIndicator,
    };
  }

  componentDidMount() {
    const options = {
      jwtInfo: this.props.jwtInfo,
      barcode: this.props.barcode,
    };

    const self = this;

    getProductByBarcode(options, function(response) {
      if (response.status === 200) {
        self.setState({product: response.body}, function() {
          const filter = {
            jwtInfo: self.props.jwtInfo,
            productId: self.state.product.productId,
          };

          checkFavorite(filter, function(result) {
            self.setState({
              isFavorite: result.body.isFavorite,
              isLoading: false,
            });
          });
        });
      }
    });
  }

  toggleFavorite = () => {
    const options = {
      jwtInfo: this.props.jwtInfo,
      productId: this.state.product.productId,
    };

    const self = this;

    if (this.state.isFavorite) {
      removeFavorite(options, function(response) {
        self.setState({isFavorite: false});
      });
    } else {
      addFavorite(options, function(response) {
        self.setState({isFavorite: true});
      });
    }
  };

  formatRating = (title, rating) => {
    const icons = [];

    const iconNames = {
      environment: 'flower-tulip',
      environmentBorder: 'flower-tulip-outline',
      ethical: 'account',
      ethicalBorder: 'account-outline',
      health: 'heart',
      healthBorder: 'heart-outline'
    };

    const icon = iconNames[title];
    const iconBorder = iconNames[title + 'Border'];

    for (let i = 0; i < rating; i++) {
      const key = Math.floor(Math.random() * 10000) + new Date().getTime();

      icons.push(
        <View style={styles.icon__container} key={key}>
          <CommunityIcon name={icon} size={30} color={theme.LIGHT_GREEN} />
        </View>
      );
    }

    for (let i = 0; i < (10 - rating); i++) {
      const key = Math.floor(Math.random() * 10000) + new Date().getTime();

      icons.push(
        <View style={styles.icon__container} key={key}>
          <CommunityIcon name={iconBorder} size={30} color={theme.LIGHT_GREEN} />
        </View>
      );
    }

    return icons;
  };

  getOverallRating = () => {
    const average = (
      this.state.product.environmentRating +
      this.state.product.ethicalRating +
      this.state.product.healthRating
    ) / 3;

    return average.toFixed(1);
  };

  showFlagAlert = () => {
    Alert.alert('Flagged Product', this.state.product.flagDescription, [
      {
        text: 'Close',
      },
    ]);
  };

  render() {
    let environmentRating = null;
    let ethicalRating = null;
    let healthRating = null;
    let overallRating = null;

    let flag = null;

    if (this.state.product !== null) {
      environmentRating = this.formatRating('environment', this.state.product.environmentRating)
      ethicalRating = this.formatRating('ethical', this.state.product.ethicalRating);
      healthRating = this.formatRating('health', this.state.product.healthRating);
      overallRating = this.getOverallRating();

      if (this.state.product.flag) {
        flag = (
          <View style={styles.flag__view}>
            <TouchableOpacity style={styles.flag__button} onPress={() => this.showFlagAlert()}>
              <CommunityIcon name="flag-variant" size={25} color={theme.DARKEST_ORANGE} />
            </TouchableOpacity>
          </View>
        );
      }
    }

    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
        <View style={styles.modal__view}>
          {this.state.isLoading ? (
            <View style={styles.modal__loader}>
              <ActivityIndicator size="large" color={theme.DARK_GREEN} />
              <Text style={styles.modal__loading__text}>
                Loading Product...
              </Text>
            </View>
          ) : (
            <>
              <View style={styles.modal__header__view}>
                <View style={styles.modal__header}>
                  <View style={styles.close__button__view}>
                    <TouchableOpacity onPress={() => this.props.closeModal()}>
                      <Icon name="close" size={30} color={theme.LIGHT_GREY} />
                    </TouchableOpacity>
                  </View>
                  <View style={styles.modal__title__view}>
                    <Text style={styles.modal__title__text}>
                      {this.state.product.companyName}
                    </Text>
                  </View>
                  <View style={styles.favorite__button__view}>
                    <TouchableOpacity onPress={() => this.toggleFavorite()}>
                      {this.state.isFavorite ? (
                        <Icon
                          name="favorite"
                          size={30}
                          color={theme.LIGHT_GREY}
                        />
                      ) : (
                        <Icon
                          name="favorite-border"
                          size={30}
                          color={theme.LIGHT_GREY}
                        />
                      )}
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.modal__subheader}>
                  <Text style={styles.modal__subheader__text}>
                    {this.state.product.productName}
                  </Text>
                  {flag}
                </View>
              </View>
              <View style={styles.modal__body__view}>
                <View style={styles.modal__body}>
                  <View style={styles.rating__container}>
                    <Text style={styles.rating__text}>Overall Rating</Text>
                    <Text style={styles.rating__number}>{overallRating} / 10</Text>
                  </View>
                  <View style={styles.rating__container}>
                    <Text style={styles.rating__text}>Environment Rating</Text>
                    <View style={styles.rating}>
                      {environmentRating}
                    </View>
                  </View>
                  <View style={styles.rating__container}>
                    <Text style={styles.rating__text}>Ethical Rating</Text>
                    <View style={styles.rating}>
                      {ethicalRating}
                    </View>
                  </View>
                  <View style={styles.rating__container}>
                    <Text style={styles.rating__text}>Health Rating</Text>
                    <View style={styles.rating}>
                      {healthRating}
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.modal__overlay__view}>
                <Image
                  style={styles.product__image}
                  source={{uri: this.state.product.pictureUrl}}
                />
              </View>
            </>
          )}
        </View>
      </Modal>
    );
  }
}
