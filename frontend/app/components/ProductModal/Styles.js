import {StyleSheet} from 'react-native';

import theme from '../../Theme';

const styles = StyleSheet.create({
  modal__view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  modal__header__view: {
    flex: 0.6,
    backgroundColor: theme.LIGHT_ORANGE,
    width: '100%',
  },
  modal__header: {
    flexDirection: 'row',
    paddingTop: '10%',
  },
  modal__subheader: {
    flex: 0.25,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modal__title__text: {
    fontSize: 17.5,
    fontFamily: 'SFProDisplay-Bold',
    color: theme.LIGHT_GREY,
    padding: 2.5,
  },
  modal__subheader__text: {
    fontSize: 22.5,
    fontFamily: 'SFProDisplay-Bold',
    color: theme.LIGHT_GREY,
    padding: 10,
  },
  close__button__view: {
    width: '20%',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    padding: '5%',
  },
  modal__title__view: {
    width: '60%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: '5%',
    paddingBottom: '5%',
  },
  favorite__button__view: {
    width: '20%',
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    padding: '5%',
  },
  modal__body__view: {
    flex: 1,
    width: '100%',
    backgroundColor: theme.LIGHT_GREY,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modal__overlay__view: {
    top: 180,
    height: '25%',
    width: undefined,
    aspectRatio: 1,
    backgroundColor: theme.WHITE,
    position: 'absolute',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    shadowRadius: 10,
    shadowOffset: {
      width: 0,
      height: -2,
    },
    shadowColor: theme.DARK_GREY,
    shadowOpacity: 0.25,
  },
  product__image: {
    padding: 20,
    width: '100%',
    height: undefined,
    aspectRatio: 1,
  },
  modal__body: {
    flex: 1,
    paddingTop: 50,
    width: '85%',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  rating__container: {
    flex: 0.2,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  rating__text: {
    flex: 0.5,
    padding: 10,
    paddingLeft: 0,
    fontSize: 15,
    fontFamily: 'SFProDisplay-Bold',
    color: theme.DARK_GREY,
  },
  rating: {
    flex: 1,
    flexDirection: 'row',
  },
  rating__number: {
    flex: 1,
    fontFamily: 'Morganite-Light',
    fontSize: 50,
    color: theme.LIGHT_GREEN,
  }
});

export default styles;
