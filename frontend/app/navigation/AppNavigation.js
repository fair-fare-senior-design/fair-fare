import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';

import Home from '../screens/Home/Home';
import Search from '../screens/Search/Search';
import BarcodeScan from '../screens/BarcodeScan/BarcodeScan';
import FavoriteList from '../screens/FavoriteList/FavoriteList';
import Profile from '../screens/Profile/Profile';

import theme from '../Theme';
import styles from '../Styles';

const App = createBottomTabNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="home" size={25} color={theme.LIGHT_GREY} />
        ),
      },
    },
    Search: {
      screen: Search,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="search" size={25} color="white" style={styles.tab} />
        ),
      },
    },
    Scan: {
      screen: BarcodeScan,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="camera" size={25} color="white" style={styles.tab} />
        ),
      },
    },
    FavoriteList: {
      screen: FavoriteList,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="favorite" size={25} color="white" style={styles.tab} />
        ),
      },
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="person" size={25} color="white" style={styles.tab} />
        ),
      },
    },
  },
  {
    tabBarOptions: {
      activeBackgroundColor: theme.DARK_GREEN,
      activeTintColor: theme.WHITE,
      inactiveBackgroundColor: theme.LIGHT_GREEN,
      inactiveTintColor: theme.WHITE,
      showLabel: false,
      style: styles.bottom__tabs,
    },
  },
  {
    defaultNavigationOptions: {
      header: null,
    },
  },
);

const AppNavigator = createAppContainer(App);

export default AppNavigator;
