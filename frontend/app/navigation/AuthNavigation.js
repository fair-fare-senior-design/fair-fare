import {createStackNavigator} from 'react-navigation-stack';

import SignUp from '../screens/SignUp/SignUp';
import LogIn from '../screens/LogIn/LogIn';

const AuthNavigation = createStackNavigator(
  {
    LogIn: {
      screen: LogIn,
      navigationOptions: {
        header: null,
      },
    },
    SignUp: {
      screen: SignUp,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    initialRouteName: 'SignUp',
  },
  {
    defaultNavigationOptions: {
      header: null,
    },
  },
);

export default AuthNavigation;
