import React from 'react';
import {createSwitchNavigator, createAppContainer} from 'react-navigation';

import AuthNavigation from './navigation/AuthNavigation';
import AppNavigation from './navigation/AppNavigation';

const Root = createSwitchNavigator(
  {
    Auth: AuthNavigation,
    App: AppNavigation,
  },
  {
    initialRouteName: 'Auth',
  },
);

const RootNavigator = createAppContainer(Root);

export default function RootNavigation() {
  return <RootNavigator />;
}
