import {StyleSheet} from 'react-native';

import theme from './Theme';

const styles = StyleSheet.create({
  view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.BLACK,
  },
  view__title: {
    color: 'white',
    backgroundColor: theme.BLACK,
    fontSize: 80,
    fontFamily: 'Morganite-Bold',
    textAlign: 'center',
    width: '100%',
  },
  view__description: {
    fontFamily: 'SFProDisplay-Light',
    fontSize: 17.5,
    color: theme.WHITE,
    textAlign: 'center',
    margin: 20,
  },
  view__input: {
    fontFamily: 'SFProDisplay-Light',
    fontSize: 17.5,
    backgroundColor: theme.DARK_GREY,
    width: '85%',
    margin: 7.5,
    padding: 20,
    color: theme.WHITE,
  },
  view__button: {
    fontSize: 17.5,
    fontWeight: 'bold',
    backgroundColor: theme.LIGHTER_ORANGE,
    textAlign: 'center',
    width: '85%',
    margin: 22.5,
  },
  view__button__register: {
    backgroundColor: theme.BLACK,
    color: theme.LIGHTER_ORANGE,
    fontSize: 17.5,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: -25,
  },
  bottom__tabs: {
    backgroundColor: theme.LIGHT_GREEN,
    shadowColor: theme.DARK_GREY,
    shadowRadius: 10,
    shadowOpacity: 0.5,
  },
});

export default styles;
