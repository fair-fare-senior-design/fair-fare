export default function loginReducer(state = {jwtInfo: null}, action) {
  switch (action.type) {
    case 'LOG_IN': {
      return {
        jwtInfo: action.data,
      };
    }
    case 'LOG_OUT': {
      return {
        jwtInfo: null,
      };
    }
  }
}
