import React from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';

import theme from '../../Theme';
import styles from './Styles';

import {logIn} from '../../services/AuthService';

export default class LogIn extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      userExists: true,
      jwtInfo: {},
    };
  }

  handleSignUp = () => {
    this.props.navigation.navigate('SignUp');
  };

  handleSubmit = () => {
    let fieldsNotEmpty = this.checkFieldsNotEmpty();

    if (fieldsNotEmpty) {
      let formData = {
        username: this.state.username,
        password: this.state.password,
      };

      const self = this;

      logIn(formData, function(response) {
        if (response.status === 500) {
          self.setState({
            username: '',
            password: '',
            userExists: false,
          });
        } else {
          self.props.navigation.navigate('App');
        }
      });
    }
  };

  checkFieldsNotEmpty = () => {
    return this.state.username !== '' && this.state.password !== '';
  };

  render() {
    let fieldStyle = this.state.userExists
      ? styles.view__input
      : styles.view__input__error;

    return (
      <View style={styles.view}>
        <Text style={styles.view__title}>Login</Text>
        <Text style={styles.view__description}>
          Welcome back, conscious consumer!
        </Text>
        <TextInput
          style={fieldStyle}
          autoCompleteType="username"
          placeholder="Enter your username"
          placeholderTextColor={theme.LIGHT_GREY}
          value={this.state.username}
          onChangeText={text => this.setState({username: text})}
          autoCapitalize="none"
        />
        <TextInput
          style={fieldStyle}
          autoCompleteType="password"
          secureTextEntry
          placeholder="Enter your password"
          placeholderTextColor={theme.LIGHT_GREY}
          value={this.state.password}
          onChangeText={text => this.setState({password: text})}
          autoCapitalize="none"
        />
        <TouchableOpacity
          style={styles.view__submit}
          title="Log In"
          onPress={() => this.handleSubmit()}>
          <Text style={styles.view__submit}>Submit</Text>
        </TouchableOpacity>
        <Text style={styles.view__description}>Don't have an account?</Text>
        <TouchableOpacity
          style={styles.view__button}
          title="Register Now"
          onPress={() => this.handleSignUp()}>
          <Text style={styles.view__button}>Register Now</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
