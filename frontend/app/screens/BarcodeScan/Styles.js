import {StyleSheet} from 'react-native';

import theme from '../../Theme';

const styles = StyleSheet.create({
  view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.LIGHT_GREY,
  },
  view__title: {
    color: theme.BLACK,
    fontSize: 130,
    fontFamily: 'Morganite-ExtraBold',
    textAlign: 'center',
    width: '100%',
  },
  camera__view: {
    flex: 1,
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  camera__button: {
    width: '100%',
  },
  camera__icon: {
    color: theme.MEDIUM_GREY,
  },
});

export default styles;
