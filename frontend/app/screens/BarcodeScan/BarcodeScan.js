import React from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';

import store from '../../../store';
import styles from './Styles';
import theme from '../../Theme';

import ProductModal from '../../components/ProductModal/ProductModal';

const PendingView = () => (
  <View>
    <Text>Waiting</Text>
  </View>
);

export default class BarcodeScan extends React.Component {
  constructor(props) {
    super(props);
 
    this.camera = null;
    this.barcodeCodes = [];

    this.state = {
      camera: {
        type: RNCamera.Constants.Type.back,
        flashMode: RNCamera.Constants.FlashMode.auto,
      },
      barcode: null,
      showActivityIndicator: false,
      modalVisible: false,
    };
  }

  componentDidMount() {
    this.subscriptions = [
      this.props.navigation.addListener('didFocus', () => {
        this.barcodeCodes = [];
      }),
    ];
  }

  onBarcodeRead = data => {
    if (data.data != null) {
      if (!this.barcodeCodes.includes(data.data)) {
        this.barcodeCodes.push(data.data);
        this.setState({
          barcode: (data.data).substring(1),
          showActivityIndicator: true,
          modalVisible: true,
        });
      }
    }

    return;
  };

  takePhoto(camera) {
    console.log(camera);
  }

  // async takePhoto(camera) {
  //   const options = {quality: 0.5, base64: true};
  //   const data = await camera.takePictureAsync(options);
  //   this.camera = null;
  // }

  onManualBarcodeEntry = () => {
    return (
      <View style={styles.form__view}>
        <View style={styles.form__label__view}>
          <Text style={styles.form__label__text}>Barcode</Text>
        </View>
        <View style={styles.form__input__view}>
          <TextInput
            style={styles.form__input}
            placeholder="Enter a UPC barcode"
            placeholderTextColor={theme.DARK_GREY}
            value={this.state.barcode}
            onChangeText={text => this.setState({barcode: text})}
            autoCapitalize="none"
          />
        </View>
        <View style={styles.form__blank__view} />
      </View>
    );
  };

  closeModal = () => {
    this.barcodes = [];

    this.setState({
      barcode: null,
      modalVisible: false,
      showActivityIndicator: false,
    });
  };

  render() {
    return (
      <View style={styles.view}>
        {this.state.modalVisible ? (
          <ProductModal
            jwtInfo={store.getState().jwtInfo}
            barcode={this.state.barcode}
            showActivityIndicator={this.state.showActivityIndicator}
            modalVisible={this.state.modalVisible}
            closeModal={this.closeModal}
          />
        ) : (
          <></>
        )}
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          defaultTouchToFocus
          flashMode={this.state.camera.flashMode}
          type={this.state.camera.type}
          mirrorImage={false}
          onBarCodeRead={this.onBarcodeRead}
          onFocusChanged={() => {}}
          onZoomChanged={() => {}}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}>
          {({camera, status, recordAudioPermissionStatus}) => {
            if (status !== 'READY') {
              return PendingView();
            }
            return (
              <View style={styles.camera__view}>
                <TouchableOpacity
                  onPress={() => this.takePhoto(camera)}
                  style={styles.camera__button}>
                  <Icon
                    name="photo-camera"
                    size={45}
                    color={theme.MEDIUM_GREY}
                    style={styles.camera__icon}
                  />
                </TouchableOpacity>
              </View>
            );
          }}
        </RNCamera>
        <View>
          <TouchableOpacity onPress={() => this.onManualBarcodeEntry} />
        </View>
      </View>
    );
  }
}
