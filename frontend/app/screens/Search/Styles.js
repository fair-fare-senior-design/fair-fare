import {StyleSheet} from 'react-native';

import theme from '../../Theme';

const styles = StyleSheet.create({
  view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.LIGHT_GREY,
  },
  title__view: {
    flex: 0.3,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    backgroundColor: theme.LIGHT_ORANGE,
  },
  title__text: {
    color: theme.LIGHT_GREY,
    fontSize: 85,
    fontFamily: 'Morganite-ExtraBold',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    letterSpacing: 2,
  },
  search__view: {
    flex: 0.7,
    width: '100%',
    backgroundColor: theme.LIGHT_GREY,
    marginTop: -17.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    shadowRadius: 10,
    shadowOffset: {
      width: 0,
      height: -2,
    },
    shadowColor: theme.DARK_GREY,
    shadowOpacity: 0.25,
  },
  search__bar__label__view: {
    flex: 0.1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  search__bar__view: {
    flex: 0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  search__categories__view: {
    flex: 0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  search__categories__container: {
    flex: 0.9,
    flexDirection: 'row',
    borderColor: theme.LIGHTER_ORANGE,
    borderWidth: 2,
  },
  search__results__view: {
    flex: 0.75,
    backgroundColor: theme.LIGHT_GREY,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  search__result__view: {
    flex: 0.25,
    flexDirection: 'row',
    width: '100%',
    backgroundColor: theme.LIGHTEST_ORANGE,
    padding: 10,
    margin: 5,
    borderRadius: 10,
  },
  scroll__search__results: {
    width: '95%',
  },
  scroll__search__results__view: {
    flex: 1,
    width: '87.5%',
  },
  search__category__unselected: {
    flex: 0.333,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme.LIGHT_GREY,
  },
  search__category__selected: {
    flex: 0.333,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme.LIGHTER_ORANGE,
  },
  search__category__selected__text: {
    color: theme.WHITE,
    fontFamily: 'SFProDisplay-Bold',
  },
  search__category__unselected__text: {
    color: theme.LIGHTER_ORANGE,
    fontFamily: 'SFProDisplay-Bold',
  },
  search__bar__label: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 15,
  },
  search__bar__input: {
    backgroundColor: theme.WHITE,
    color: theme.DARK_GREY,
    width: '90%',
    padding: 15,
  },
  search__result: {
    flex: 0.333,
    alignItems: 'center',
    justifyContent: 'center',
  },
  search__result__title__view: {
    flex: 0.75,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  search__result__rating__view: {
    flex: 0.25,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  search__result__image__view: {
    flex: 0.25,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  search__result__user__view: {
    flex: 0.75,
    alignItems: 'flex-start',
    justifyContent: 'center',
    margin: 15,
  },
  search__text__title: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 17,
  },
  search__text__subtitle: {
    fontFamily: 'SFProDisplay-Light',
    fontSize: 16,
  },
  user__image: {
    flex: 1,
    width: '100%',
    height: undefined,
    aspectRatio: 1,
    borderRadius: 150 / 2,
  },
  search__result__button: {
    width: '100%',
  },
});

export default styles;
