import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ActivityIndicator,
} from 'react-native';

import store from '../../../store';
import styles from './Styles';
import theme from '../../Theme';

import {search} from '../../services/SearchService';
import {getAccount} from '../../services/UserService';

import ProductModal from '../../components/ProductModal/ProductModal';
import UserModal from '../../components/UserModal/UserModal';

export default class Search extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedCategory: 'product',
      searchTerm: '',
      searchResults: [],
      selectedProduct: null,
      selectedUser: null,
      productModalVisible: false,
      userModalVisible: false,
      resultsLoading: false,
    };
  }

  componentDidMount() {
    this.subscriptions = [
      this.props.navigation.addListener('didFocus', () => {
        this.setState({
          selectedCategory: 'product',
          searchTerm: '',
          searchResults: [],
          selectedProduct: null,
          selectedUser: null,
          resultsLoading: false,
          productModalVisible: false,
          userModalVisible: false,
        });
      }),
    ];
  }

  componentWillUnmount() {
    this.subscriptions.forEach(sub => sub.remove());
  }

  search = () => {
    const options = {
      jwtInfo: store.getState().jwtInfo,
      category: this.state.selectedCategory,
      searchTerm: this.state.searchTerm,
    };

    const self = this;

    this.setState({resultsLoading: true});

    search(options, function(response) {
      if (response.status === 200) {
        setTimeout(function() {
          self.setState({
            searchResults: response.body,
            resultsLoading: false,
          });
        }, 1000);
      } else {
        self.setState({resultsLoading: false});
      }
    });
  };

  newSearch = category => {
    this.setState({
      selectedCategory: category,
      searchTerm: '',
      searchResults: [],
    });
  };

  handleSelectProduct = product => {
    this.setState({
      selectedProduct: product,
      productModalVisible: true,
      showActivityIndicator: true,
    });
  };

  handleSelectUser = user => {
    const options = {
      jwtInfo: store.getState().jwtInfo,
      username: user.username,
    };

    const self = this;

    getAccount(options, function(response) {
      self.setState({
        selectedUser: response.body,
        userModalVisible: true,
        showActivityIndicator: true,
      });
    });
  };

  closeProductModal = () => {
    this.setState({
      selectedProduct: null,
      productModalVisible: false,
      showActivityIndicator: false,
    });
  };

  closeUserModal = () => {
    this.setState({
      selectedUser: null,
      userModalVisible: false,
      showActivityIndicator: false,
    });
  };

  render() {
    let searchResults = <Text>These are the results.</Text>;

    if (this.state.searchResults.length > 0) {
      if (
        this.state.selectedCategory === 'company' ||
        this.state.selectedCategory === 'product'
      ) {
        searchResults = this.state.searchResults.map((result, index) => (
          <TouchableOpacity
            onPress={() => this.handleSelectProduct(result)}
            key={index}>
            <View style={styles.search__result__view}>
              <View style={styles.search__result__title__view}>
                <Text style={styles.search__text__title}>{result.name}</Text>
                <Text style={styles.search__text__subtitle}>
                  {result.company}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        ));
      } else if (this.state.selectedCategory === 'person') {
        searchResults = this.state.searchResults.map((result, index) => (
          <TouchableOpacity
            onPress={() => this.handleSelectUser(result)}
            key={index}>
            <View style={styles.search__result__view}>
              <View style={styles.search__result__image__view}>
                <Image
                  style={styles.user__image}
                  source={{uri: result.pictureUrl}}
                />
              </View>
              <View style={styles.search__result__user__view}>
                <Text style={styles.search__text__title}>
                  {result.firstName === '' ? result.username : result.firstName}
                </Text>
                <Text style={styles.search__text__subtitle}>
                  @{result.username}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        ));
      }
    }

    return (
      <View style={styles.view}>
        {this.state.productModalVisible ? (
          <ProductModal
            jwtInfo={store.getState().jwtInfo}
            barcode={this.state.selectedProduct.barcode}
            showActivityIndicator={this.state.showActivityIndicator}
            modalVisible={this.state.productModalVisible}
            closeModal={this.closeProductModal}
          />
        ) : (
          <></>
        )}
        {this.state.userModalVisible ? (
          <UserModal
            jwtInfo={store.getState().jwtInfo}
            user={this.state.selectedUser}
            showActivityIndicator={this.state.showActivityIndicator}
            modalVisible={this.state.userModalVisible}
            closeModal={this.closeUserModal}
          />
        ) : (
          <></>
        )}
        <View style={styles.title__view}>
          <Text style={styles.title__text}>Product Search</Text>
        </View>
        <View style={styles.search__view}>
          <View style={styles.search__bar__label__view}>
            <Text style={styles.search__bar__label}>
              Search by name or key word
            </Text>
          </View>
          <View style={styles.search__bar__view}>
            <TextInput
              style={styles.search__bar__input}
              placeholder="Enter your keywords"
              placeholderTextColor={theme.DARK_GREY}
              value={this.state.searchTerm}
              returnKeyType="search"
              autoCapitalize="none"
              multiline={false}
              enablesReturnKeyAutomatically
              onChangeText={text => this.setState({searchTerm: text})}
              onSubmitEditing={this.search}
            />
          </View>
          <View style={styles.search__categories__view}>
            <View style={styles.search__categories__container}>
              <View
                style={
                  this.state.selectedCategory === 'company'
                    ? styles.search__category__selected
                    : styles.search__category__unselected
                }>
                <TouchableOpacity onPress={() => this.newSearch('company')}>
                  <Text
                    style={
                      this.state.selectedCategory === 'company'
                        ? styles.search__category__selected__text
                        : styles.search__category__unselected__text
                    }>
                    Company
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={
                  this.state.selectedCategory === 'product'
                    ? styles.search__category__selected
                    : styles.search__category__unselected
                }>
                <TouchableOpacity onPress={() => this.newSearch('product')}>
                  <Text
                    style={
                      this.state.selectedCategory === 'product'
                        ? styles.search__category__selected__text
                        : styles.search__category__unselected__text
                    }>
                    Product
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={
                  this.state.selectedCategory === 'person'
                    ? styles.search__category__selected
                    : styles.search__category__unselected
                }>
                <TouchableOpacity onPress={() => this.newSearch('person')}>
                  <Text
                    style={
                      this.state.selectedCategory === 'person'
                        ? styles.search__category__selected__text
                        : styles.search__category__unselected__text
                    }>
                    Person
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <SafeAreaView style={styles.search__results__view}>
            {this.state.resultsLoading ? (
              <View style={styles.modal__loader}>
                <ActivityIndicator size="large" color={theme.DARK_GREY} />
                <Text style={styles.modal__loading__text}>
                  Getting results...
                </Text>
              </View>
            ) : (
              <>
                <View style={styles.scroll__search__results__view}>
                  <ScrollView
                    contentContainerStyle={styles.scroll__search__results}>
                    {searchResults}
                  </ScrollView>
                </View>
              </>
            )}
          </SafeAreaView>
        </View>
      </View>
    );
  }
}
