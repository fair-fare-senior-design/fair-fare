import {StyleSheet} from 'react-native';

import theme from '../../Theme';

const styles = StyleSheet.create({
  container__view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  footer__view: {
    flex: 0.25,
    flexDirection: 'row',
    width: '100%',
    padding: 10,
    backgroundColor: theme.LIGHT_GREY,
  },
  body__view: {
    flex: 8,
    width: '100%',
  },
  body__view__top: {
    flex: 0.65,
    width: '100%',
  },
  body__view__bottom: {
    flex: 1,
    width: '100%',
    shadowRadius: 10,
    shadowOffset: {
      width: 0,
      height: -2,
    },
    shadowColor: theme.DARK_GREY,
    shadowOpacity: 0.25,
  },
  user__image__view: {
    flex: 2.5,
    alignSelf: 'center',
    backgroundColor: theme.LIGHT_GREY,
    width: '100%',
  },
  user__image: {
    flex: 1,
    width: '100%',
    height: undefined,
    aspectRatio: 2,
  },
  user__view: {
    flex: 0.23,
    flexDirection: 'row',
    marginTop: -17.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingTop: 10,
    paddingLeft: 20,
    backgroundColor: theme.LIGHT_GREY,
    justifyContent: 'flex-end',
  },
  user__title__view: {
    flex: 1.75,
    height: '100%',
    backgroundColor: theme.LIGHT_GREY,
  },
  user__edit__view: {
    flex: 0.25,
  },
  user__subtitle__view: {
    flex: 0.1,
    width: '100%',
    paddingLeft: 20,
    backgroundColor: theme.LIGHT_GREY,
  },
  following__view: {
    flex: 0.1,
    flexDirection: 'row',
    width: '100%',
    padding: 5,
    paddingLeft: 20,
    backgroundColor: theme.LIGHT_GREY,
  },
  date__view: {
    flex: 0.1,
    paddingLeft: 20,
    backgroundColor: theme.LIGHT_GREY,
  },
  bio__view: {
    flex: 0.45,
    paddingLeft: 20,
    justifyContent: 'center',
    alignItems: 'flex-start',
    backgroundColor: theme.LIGHT_GREY,
  },
  favorites__view: {
    flex: 0.45,
    paddingLeft: 20,
    backgroundColor: theme.LIGHT_GREY,
  },
  following__text: {
    color: theme.DARK_GREY,
    fontSize: 15,
    fontFamily: 'SFProDisplay-Bold',
    padding: 5,
  },
  user__title: {
    fontSize: 50,
    fontFamily: 'SFProDisplay-Bold',
    padding: 5,
  },
  user__subtitle: {
    fontSize: 17.5,
    fontFamily: 'SFProDisplay-Light',
    padding: 5,
  },
  date__title: {
    color: theme.DARK_GREY,
    fontSize: 16.5,
    fontFamily: 'SFProDisplay-Light',
    padding: 5,
  },
  bio__title: {
    fontSize: 17.5,
    fontFamily: 'SFProDisplay-Light',
    padding: 5,
    width: '85%',
  },
  favorites__title: {
    fontSize: 16.5,
    fontFamily: 'SFProDisplay-Bold',
    padding: 5,
  },
  favorites__item: {
    fontSize: 15,
    fontFamily: 'SFProDisplay-Light',
    padding: 5,
  },
  logout__button__view: {
    width: '100%',
    justifyContent: 'flex-end',
  },
  logout__button: {
    borderRadius: 5,
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
  logout__button__text: {
    fontSize: 15,
    fontFamily: 'SFProDisplay-Bold',
    color: theme.LIGHT_GREY,
    padding: 5,
    paddingLeft: 10,
    paddingRight: 10,
  },
});

export default styles;
