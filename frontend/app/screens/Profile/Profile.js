import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  Alert,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';

import styles from './Styles';
import theme from '../../Theme';
import store from '../../../store';

import {logOut} from '../../services/AuthService';
import {getAccount, updateAccount} from '../../services/UserService';
import {getFavorites} from '../../services/FavoriteService';

import ProfileModal from '../../components/ProfileModal/ProfileModal';
import FollowerModal from '../../components/FollowerModal/FollowerModal';
import FolloweeModal from '../../components/FolloweeModal/FolloweeModal';

export default class Profile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      jwtInfo: store.getState().jwtInfo,
      loggedIn: true,
      profileModalVisible: false,
      followerModalVisible: false,
      followeeModalVisible: false,
      displayName: '',
      user: {
        firstName: '',
        bio: '',
        email: '',
        dateJoined: '',
        pictureUrl: '',
        followerCount: 0,
        followeeCount: 0,
        followers: [],
        followees: [],
        favoriteBrands: [],
      },
      userLoaded: false,
      showActivityIndicator: false,
    };
  }

  componentDidMount() {
    this.subscriptions = [
      this.props.navigation.addListener('didFocus', () => {
        this.getUserAccount();
      }),
    ];
  }

  componentWillUnmount() {
    this.subscriptions.forEach(sub => sub.remove());
  }

  handleLogOut = () => {
    logOut();
    this.props.navigation.navigate('Auth');
  };

  getUserAccount = () => {
    const options = {
      jwtInfo: store.getState().jwtInfo,
      username: store.getState().jwtInfo.blob.username,
    };

    const self = this;

    getAccount(options, function(response) {
      if (response.status === 200) {
        self.setState(
          {
            user: {
              firstName: response.body.firstName,
              bio: response.body.bio,
              email: response.body.email,
              dateJoined: response.body.dateJoined,
              pictureUrl: response.body.pictureUrl,
              followerCount: response.body.followerCount,
              followeeCount: response.body.followeeCount,
              followers: response.body.followers,
              followees: response.body.followees,
              favoriteBrands: [],
            },
          },
          function() {
            const filter = {
              jwtInfo: store.getState().jwtInfo,
              username: store.getState().jwtInfo.blob.username,
            };

            getFavorites(filter, function(result) {
              self.setState(prevState => ({
                user: {
                  ...prevState.user,
                  favoriteBrands: result.body,
                },
                userLoaded: true,
                showActivityIndicator: false,
                profileModalVisible: false,
              }));
            });
          },
        );
      }
    });
  };

  handleUpdateUser = options => {
    this.setState({showActivityIndicator: true});

    const self = this;

    updateAccount(options, function(response) {
      if (response.status === 200) {
        self.getUserAccount();
      }
    });
  };

  closeProfileModal = () => {
    this.setState({profileModalVisible: false});
  };

  closeFollowerModal = () => {
    this.setState({followerModalVisible: false});
  };

  closeFolloweeModal = () => {
    this.setState({followeeModalVisible: false});
  };

  openLogoutAlert = () => {
    Alert.alert('Log Out', 'Are you absolutely sure?', [
      {
        text: 'Yes',
        onPress: () => this.handleLogOut(),
      },
      {
        text: 'No',
      },
    ]);
  };

  formatFavoriteBrands = () => {
    let favoriteBrands = [];

    for (let i = 0; i < this.state.user.favoriteBrands.length; i++) {
      favoriteBrands.push({key: this.state.user.favoriteBrands[i].company});
    }

    return favoriteBrands;
  };

  render() {
    let displayName = this.state.jwtInfo.blob.username;
    if (this.state.user.firstName !== '') {
      displayName = this.state.user.firstName;
    }
    let username = '@' + this.state.jwtInfo.blob.username;

    let dateJoined = this.state.user.dateJoined;
    let parts = dateJoined.split(' ');
    let dateBlurb = 'Joined ' + parts[2] + ' ' + parts[3];

    let favoriteBrands = this.formatFavoriteBrands();

    if (this.state.userLoaded) {
      return (
        <View style={styles.container__view}>
          {this.state.profileModalVisible ? (
            <ProfileModal
              jwtInfo={this.state.jwtInfo}
              firstName={this.state.user.firstName}
              bio={this.state.user.bio}
              profilePictureUrl={this.state.user.pictureUrl}
              handleUpdateUser={this.handleUpdateUser}
              showActivityIndicator={this.state.showActivityIndicator}
              modalVisible={this.profileModalVisible}
              closeModal={this.closeProfileModal}
            />
          ) : (
            <></>
          )}
          {this.state.followerModalVisible ? (
            <FollowerModal
              jwtInfo={this.state.jwtInfo}
              followers={this.state.user.followers}
              showActivityIndicator={this.state.showActivityIndicator}
              modalVisible={this.followerModalVisible}
              closeModal={this.closeFollowerModal}
            />
          ) : (
            <></>
          )}
          {this.state.followeeModalVisible ? (
            <FolloweeModal
              jwtInfo={this.state.jwtInfo}
              followees={this.state.user.followees}
              showActivityIndicator={this.state.showActivityIndicator}
              modalVisible={this.followeeModalVisible}
              closeModal={this.closeFolloweeModal}
            />
          ) : (
            <></>
          )}
          <View style={styles.body__view}>
            <View style={styles.body__view__top}>
              <View style={styles.user__image__view}>
                <Image
                  style={styles.user__image}
                  source={{uri: this.state.user.pictureUrl}}
                />
              </View>
            </View>
            <View style={styles.body__view__bottom}>
              <View style={styles.user__view}>
                <View style={styles.user__title__view}>
                  <Text style={styles.user__title}>{displayName}</Text>
                </View>
                <View style={styles.user__edit__view}>
                  <TouchableOpacity
                    onPress={() => this.setState({profileModalVisible: true})}>
                    <Icon
                      name="create"
                      size={30}
                      color={theme.MEDIUM_GREY}
                      style={styles.user__edit__button}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.user__subtitle__view}>
                <Text style={styles.user__subtitle}>{username}</Text>
              </View>
              <View style={styles.following__view}>
                <TouchableOpacity
                  onPress={() => this.setState({followerModalVisible: true})}>
                  <Text style={styles.following__text}>
                    {this.state.user.followerCount} Followers
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.setState({followeeModalVisible: true})}>
                  <Text style={styles.following__text}>
                    {this.state.user.followeeCount} Following
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={styles.date__view}>
                <Text style={styles.date__title}>{dateBlurb}</Text>
              </View>
              <View style={styles.bio__view}>
                <Text style={styles.bio__title}>{this.state.user.bio}</Text>
              </View>
              <View style={styles.favorites__view}>
                <Text style={styles.favorites__title}>Favorite Brands</Text>
                <FlatList
                  data={favoriteBrands}
                  renderItem={({item}) => (
                    <Text style={styles.favorites__item}>{item.key}</Text>
                  )}
                />
              </View>
            </View>
          </View>
          <View />
          <View style={styles.footer__view}>
            <View style={styles.logout__button__view}>
              <TouchableOpacity
                style={styles.logout__button}
                onPress={() => this.openLogoutAlert()}>
                <Icon
                  name="power-settings-new"
                  size={30}
                  color={theme.MEDIUM_GREY}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.container__view}>
          <View style={styles.modal__loader}>
            <ActivityIndicator size="large" color={theme.DARK_GREEN} />
            <Text style={styles.modal__loading__text}>
              Loading Profile...
            </Text>
          </View>
        </View>
      );
    }
  }
}
