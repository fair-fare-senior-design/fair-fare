import {StyleSheet} from 'react-native';

import theme from '../../Theme';

const styles = StyleSheet.create({
  view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.LIGHT_GREY,
  },
  title__view: {
    flex: 0.25,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    backgroundColor: theme.LIGHT_ORANGE,
  },
  favorites__view: {
    flex: 0.75,
    width: '100%',
    backgroundColor: theme.LIGHT_GREY,
    marginTop: -17.5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    shadowRadius: 10,
    shadowOffset: {
      width: 0,
      height: -2,
    },
    shadowColor: theme.DARK_GREY,
    shadowOpacity: 0.25,
  },
  favorite__view: {
    padding: 15,
    paddingBottom: 0,
  },
  favorite__container__view__even: {
    backgroundColor: theme.LIGHTEST_ORANGE,
    padding: 15,
    borderRadius: 10,
    flexDirection: 'row',
  },
  favorite__container__view__odd: {
    backgroundColor: theme.WHITE,
    padding: 15,
    borderRadius: 10,
    flexDirection: 'row',
  },
  title__text: {
    color: theme.LIGHT_GREY,
    fontSize: 85,
    fontFamily: 'Morganite-ExtraBold',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    letterSpacing: 2,
  },
  favorite__text__title: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 15,
  },
  favorite__text__subtitle: {
    fontFamily: 'SFProDisplay-Light',
    fontSize: 14,
  },
});

export default styles;
