import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

import store from '../../../store';
import styles from './Styles';

import {getFavorites, removeFavorite} from '../../services/FavoriteService';

import ProductModal from '../../components/ProductModal/ProductModal';

export default class FavoriteList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      jwtInfo: store.getState().jwtInfo,
      selectedFavorite: null,
      favoriteProducts: [],
      favoritesLoaded: false,
      showActivityIndicator: false,
    };
  }

  componentDidMount() {
    this.subscriptions = [
      this.props.navigation.addListener('didFocus', () => {
        this.getUserFavorites();
      }),
    ];
  }

  componentWillUnmount() {
    this.subscriptions.forEach(sub => sub.remove());
  }

  getUserFavorites = () => {
    const options = {
      jwtInfo: store.getState().jwtInfo,
      username: store.getState().jwtInfo.blob.username,
    };

    const self = this;

    getFavorites(options, function(response) {
      if (response.status === 200) {
        self.setState({
          favoriteProducts: response.body,
          favoritesLoaded: true,
        });
      }
    });
  };

  handleRemoveFavorite = id => {
    const options = {
      jwtInfo: store.getState().jwtInfo,
      productId: id,
    };

    const self = this;

    removeFavorite(options, function(response) {
      const filter = {
        jwtInfo: store.getState().jwtInfo,
        username: store.getState().jwtInfo.blob.username,
      };

      getFavorites(filter, function(result) {
        if (result.status === 200) {
          self.setState({
            favoriteProducts: result.body,
            favoritesLoaded: true,
          });
        }
      });
    });
  };

  handleSelectFavorite = favorite => {
    this.setState({
      selectedFavorite: favorite,
      modalVisible: true,
      showActivityIndicator: true,
    });
  };

  closeModal = () => {
    this.setState(
      {
        selectedFavorite: null,
        modalVisible: false,
        showActivityIndicator: false,
      },
      function() {
        this.getUserFavorites();
      },
    );
  };

  render() {
    const favorites = this.state.favoriteProducts.map((favorite, index) => {
      if (index % 2 === 0) {
        return (
          <View style={styles.favorite__view} key={index}>
            {this.state.modalVisible ? (
              <ProductModal
                jwtInfo={store.getState().jwtInfo}
                barcode={this.state.selectedFavorite.barcode}
                showActivityIndicator={this.state.showActivityIndicator}
                modalVisible={this.state.modalVisible}
                closeModal={this.closeModal}
              />
            ) : (
              <></>
            )}
            <TouchableOpacity
              onPress={() => this.handleSelectFavorite(favorite)}>
              <View style={styles.favorite__container__view__even}>
                <View style={styles.favorite__product__info__view}>
                  <Text style={styles.favorite__text__title}>
                    {favorite.name}
                  </Text>
                  <Text style={styles.favorite__text__subtitle}>
                    {favorite.company}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        );
      } else {
        return (
          <View style={styles.favorite__view} key={index}>
            {this.state.modalVisible ? (
              <ProductModal
                jwtInfo={store.getState().jwtInfo}
                barcode={this.state.selectedFavorite.barcode}
                showActivityIndicator={this.state.showActivityIndicator}
                modalVisible={this.state.modalVisible}
                closeModal={this.closeModal}
              />
            ) : (
              <></>
            )}
            <TouchableOpacity
              onPress={() => this.handleSelectFavorite(favorite)}>
              <View style={styles.favorite__container__view__odd}>
                <View style={styles.favorite__product__info__view}>
                  <Text style={styles.favorite__text__title}>
                    {favorite.name}
                  </Text>
                  <Text style={styles.favorite__text__subtitle}>
                    {favorite.company}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        );
      }
    });

    return (
      <View style={styles.view}>
        <View style={styles.title__view}>
          <Text style={styles.title__text}>Favorites</Text>
        </View>
        <View style={styles.favorites__view}>{favorites}</View>
      </View>
    );
  }
}
