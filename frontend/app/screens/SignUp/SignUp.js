import React from 'react';
import {View, Text, TextInput, TouchableOpacity, Alert} from 'react-native';

import styles from './Styles';
import theme from '../../Theme';

import {signUp} from '../../services/AuthService';

export default class SignUp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      username: '',
      password: '',
      confirmPassword: '',
      signUpSuccessful: false,
      emailUnique: true,
      usernameUnique: true,
    };
  }

  handleLogIn = () => {
    this.props.navigation.navigate('LogIn');
  };

  handleSubmit = () => {
    this.setState(
      {
        emailUnique: true,
        usernameUnique: true,
      },
      function() {
        let strongPassword = this.checkStrongPassword();
        let passwordsMatch = this.checkPasswordsMatch();
        let fieldsNotEmpty = this.checkFieldsNotEmpty();

        if (strongPassword && passwordsMatch && fieldsNotEmpty) {
          let formData = {
            email: this.state.email,
            username: this.state.username,
            password: this.state.password,
          };

          const self = this;

          signUp(formData, function(response) {
            if (response.status === 200) {
              self.props.navigation.navigate('LogIn');
            } else {
              self.checkUniqueUser(response);
            }
          });
        } else if (!strongPassword) {
          Alert.alert(
            'Error',
            'Password Requirements:\n1. At least one number\n2. At least one lowercase character\n3. At least one uppercase character\n4. At least 8 characters',
            [
              {
                text: 'Okay',
                onPress: () =>
                  this.setState({
                    password: '',
                    confirmPassword: '',
                    signUpSuccessful: false,
                    emailUnique: true,
                    usernameUnique: true,
                  }),
              },
            ],
          );
        } else if (!passwordsMatch) {
          Alert.alert('Error', 'Passwords do not match!', [
            {
              text: 'Okay',
              onPress: () =>
                this.setState({
                  password: '',
                  confirmPassword: '',
                  signUpSuccessful: false,
                  emailUnique: true,
                  usernameUnique: true,
                }),
            },
          ]);
        } else if (!fieldsNotEmpty) {
          Alert.alert('Error', "Please don't leave any fields empty!", [
            {
              text: 'Okay',
              onPress: () =>
                this.setState({
                  signUpSuccessful: false,
                  emailUnique: true,
                  usernameUnique: true,
                }),
            },
          ]);
        }
      },
    );
  };

  checkPasswordsMatch = () => {
    return this.state.password === this.state.confirmPassword;
  };

  checkStrongPassword = () => {
    /* Strong password:
       1. At least one number
       2. At least one lowercase letter
       3. At least one uppercase letter
       4. At least 8 characters long
    */
    const regex = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
    return regex.test(this.state.password);
  };

  checkFieldsNotEmpty = () => {
    return (
      this.state.email !== '' &&
      this.state.username !== '' &&
      this.state.password !== '' &&
      this.state.confirmPassword !== ''
    );
  };

  checkUniqueUser = response => {
    if (response.body === 'Email already in use.') {
      this.setState({
        emailUnique: false,
        email: '',
      });
    } else if (response.body === 'Username already in use.') {
      this.setState({
        usernameUnique: false,
        username: '',
      });
    }
  };

  render() {
    let emailPlaceholder = this.state.emailUnique
      ? 'Enter your email'
      : 'Email already exists';

    let usernamePlaceholder = this.state.usernameUnique
      ? 'Enter a username'
      : 'Username already exists';

    let emailFieldStyle = this.state.emailUnique
      ? styles.view__input
      : styles.view__input__error;

    let usernameFieldStyle = this.state.usernameUnique
      ? styles.view__input
      : styles.view__input__error;

    return (
      <View style={styles.view}>
        <Text style={styles.view__title}>Sign Up</Text>
        <Text style={styles.view__description}>
          Create an account to begin your journey to more conscious consumption.
        </Text>
        <TextInput
          style={emailFieldStyle}
          autoCompleteType="email"
          placeholder={emailPlaceholder}
          placeholderTextColor={theme.LIGHT_GREY}
          value={this.state.email}
          onChangeText={text => this.setState({email: text})}
          autoCapitalize="none"
        />
        <TextInput
          style={usernameFieldStyle}
          autoCompleteType="username"
          placeholder={usernamePlaceholder}
          placeholderTextColor={theme.LIGHT_GREY}
          value={this.state.username}
          onChangeText={text => this.setState({username: text})}
          autoCapitalize="none"
        />
        <TextInput
          style={styles.view__input}
          autoCompleteType="password"
          secureTextEntry
          placeholder="Enter your password"
          placeholderTextColor={theme.LIGHT_GREY}
          value={this.state.password}
          onChangeText={text => this.setState({password: text})}
          autoCapitalize="none"
        />
        <TextInput
          style={styles.view__input}
          autoCompleteType="password"
          secureTextEntry
          placeholder="Confirm your password"
          placeholderTextColor={theme.LIGHT_GREY}
          value={this.state.confirmPassword}
          onChangeText={text => this.setState({confirmPassword: text})}
          autoCapitalize="none"
        />
        <TouchableOpacity
          style={styles.view__submit}
          title="Sign Up"
          onPress={() => this.handleSubmit()}>
          <Text style={styles.view__submit}>Submit</Text>
        </TouchableOpacity>
        <Text style={styles.view__description}>Already have an account?</Text>
        <TouchableOpacity
          style={styles.view__button}
          title="Log In"
          onPress={() => this.handleLogIn()}>
          <Text style={styles.view__button}>Log In</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
