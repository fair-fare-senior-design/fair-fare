import {StyleSheet} from 'react-native';

import theme from '../../Theme';

const styles = StyleSheet.create({
  container__view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.LIGHT_GREY,
  },
  header__view: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
  },
  body__view: {
    flex: 2,
    flexDirection: 'column',
    width: '100%',
  },
  title__view: {
    flex: 6,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginBottom: -20,
  },
  tagline__view: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  title: {
    color: theme.BLACK,
    fontSize: 130,
    fontFamily: 'Morganite-ExtraBold',
  },
  tagline: {
    color: theme.DARK_GREY,
    fontFamily: 'Morganite-Light',
    fontSize: 40,
    fontWeight: 'bold',
    letterSpacing: 4,
  },
  description__view: {
    flex: 2,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button__view: {
    flex: 0.5,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  description: {
    fontFamily: 'SFProDisplay-Light',
    fontSize: 17,
    color: theme.BLACK,
    textAlign: 'left',
    margin: 7.5,
    width: '85%',
  },
  button__text: {
    fontSize: 17.5,
    fontWeight: 'bold',
    color: theme.LIGHTER_ORANGE,
    margin: 0,
  },
});

export default styles;
