import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

import styles from './Styles';

export default class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      jwtInfo: this.props.navigation.getScreenProps('jwtInfo'),
    };
  }

  render() {
    return (
      <View style={styles.container__view}>
        <View style={styles.header__view}>
          <View style={styles.title__view}>
            <Text style={styles.title}>Fair Fare</Text>
          </View>
          <View style={styles.tagline__view}>
            <Text style={styles.tagline}>CONSUME WITH CARE</Text>
          </View>
        </View>
        <View style={styles.blank__view} />
        <View style={styles.body__view}>
          <View style={styles.description__view}>
            <Text style={styles.description}>
              Labels on certain products are often misleading to consumers in
              areas such as ethical sourcing and environmental impact. We are
              empowering consumers to make educated choices by converting the
              overabundance of data into clear and concise food choices.
            </Text>
            <Text style={styles.description}>
              The industries behind meat, fish, and dairy products have a
              significant impact on the environment, and they often raise
              ethical concerns. Consumers deserve to know where their products
              originate and how they are manufactured.
            </Text>
          </View>
          <View style={styles.button__view}>
            <TouchableOpacity>
              <Text style={styles.button__text}>Learn More</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
