import {createSwitchNavigator, createAppContainer} from 'react-navigation';

import AuthNavigation from './navigation/AuthNavigation';
import AppNavigation from './navigation/AppNavigation';

export const createRootNavigator = (loggedIn = false) => {
  return createAppContainer(
    createSwitchNavigator(
      {
        Auth: {
          screen: AuthNavigation,
        },
        App: {
          screen: AppNavigation,
        },
      },
      {
        initialRouteName: loggedIn ? 'App' : 'Auth',
      },
    ),
  );
};
