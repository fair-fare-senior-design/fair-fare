import {API_URL} from 'react-native-dotenv';

export function getAccount(options, callback) {
  const apiCall = API_URL + '/users/get-user';

  const data = {username: options.username};

  fetch(apiCall, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + options.jwtInfo.hashedBlob,
    },
    body: JSON.stringify(data),
  })
    .then(response =>
      response.json().then(result => ({status: response.status, body: result})),
    )
    .then(obj => {
      return callback(obj);
    });
}

export function updateAccount(options, callback) {
  const apiCall = API_URL + '/users/update-account';

  let formData = new FormData();

  let pictureData = {
    uri: '',
    type: '',
  };

  // Check if user wants to upload a new profile picture
  if ('data' in options.picture) {
    pictureData.uri = options.picture.data.uri;
    pictureData.type = options.picture.type;
  }

  formData.append('base64', pictureData.uri);
  formData.append('type', pictureData.type);
  formData.append('firstName', options.user.firstName);
  formData.append('bio', options.user.bio);

  fetch(apiCall, {
    method: 'POST',
    headers: {
      'Content-Type': 'multipart/form-data',
      Authorization: 'Bearer ' + options.jwtInfo.hashedBlob,
    },
    body: formData,
  })
    .then(response =>
      response.json().then(result => ({status: response.status, body: result})),
    )
    .then(obj => {
      return callback(obj);
    });
}

export function deleteAccount(options, callback) {
  const apiCall = API_URL + '/users/delete-account';

  fetch(apiCall, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + options.jwtInfo.hashedBlob,
    },
  })
    .then(response =>
      response.json().then(result => ({status: response.status, body: result})),
    )
    .then(obj => {
      return callback(obj);
    });
}
