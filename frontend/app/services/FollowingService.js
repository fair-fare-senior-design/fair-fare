import {API_URL} from 'react-native-dotenv';

export function follow(options, callback) {
  const apiCall = API_URL + '/following/follow';

  const data = {username: options.username};

  fetch(apiCall, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + options.jwtInfo.hashedBlob,
    },
    body: JSON.stringify(data),
  })
    .then(response =>
      response.json().then(result => ({status: response.status, body: result})),
    )
    .then(obj => {
      return callback(obj);
    });
}

export function unfollow(options, callback) {
  const apiCall = API_URL + '/following/unfollow';

  const data = {username: options.username};

  fetch(apiCall, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + options.jwtInfo.hashedBlob,
    },
    body: JSON.stringify(data),
  })
    .then(response =>
      response.json().then(result => ({status: response.status, body: result})),
    )
    .then(obj => {
      return callback(obj);
    });
}

export function checkFollowing(options, callback) {
  const apiCall = API_URL + '/following/check-following';

  const data = {username: options.username};

  fetch(apiCall, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + options.jwtInfo.hashedBlob,
    },
    body: JSON.stringify(data),
  })
    .then(response =>
      response.json().then(result => ({status: response.status, body: result})),
    )
    .then(obj => {
      return callback(obj);
    });
}
