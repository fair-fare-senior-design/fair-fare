import store from '../../store';

import {API_URL} from 'react-native-dotenv';

export function signUp(formData, callback) {
  const apiCall = API_URL + '/auth/sign-up';

  const data = {
    username: formData.username,
    password: formData.password,
    email: formData.email,
  };

  fetch(apiCall, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  })
    .then(response =>
      response.json().then(result => ({status: response.status, body: result})),
    )
    .then(obj => {
      return callback(obj);
    });
}

export function logIn(formData, callback) {
  const apiCall = API_URL + '/auth/log-in';

  const data = {
    username: formData.username,
    password: formData.password,
  };

  fetch(apiCall, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  })
    .then(response =>
      response.json().then(result => ({status: response.status, body: result})),
    )
    .then(obj => {
      if (obj.status === 200) {
        store.dispatch({
          type: 'LOG_IN',
          data: obj.body,
        });
      }

      return callback(obj);
    });
}

export function logOut() {
  store.dispatch({
    type: 'LOG_OUT',
    data: null,
  });
}

export function checkLoggedIn(callback) {
  let info = store.getState().jwtInfo;
  let loggedIn = false;

  if (info === null) {
    loggedIn = false;
  } else {
    loggedIn = true;
  }

  callback(loggedIn);
}
