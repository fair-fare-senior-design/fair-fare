import {API_URL} from 'react-native-dotenv';

export function search(options, callback) {
  const apiCall = API_URL + '/search/search-category';

  const data = {
    category: options.category,
    searchTerm: options.searchTerm,
  };

  fetch(apiCall, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + options.jwtInfo.hashedBlob,
    },
    body: JSON.stringify(data),
  })
    .then(response =>
      response.json().then(result => ({status: response.status, body: result})),
    )
    .then(obj => {
      return callback(obj);
    });
}
