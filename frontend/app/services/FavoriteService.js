import {API_URL} from 'react-native-dotenv';

export function getFavorites(options, callback) {
  const apiCall = API_URL + '/favorites/get-favorites';

  const data = {
    username: options.username,
  };

  fetch(apiCall, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + options.jwtInfo.hashedBlob,
    },
    body: JSON.stringify(data),
  })
    .then(response =>
      response.json().then(result => ({status: response.status, body: result})),
    )
    .then(obj => {
      return callback(obj);
    });
}

export function checkFavorite(options, callback) {
  const apiCall = API_URL + '/favorites/check-favorite';

  const data = {productId: options.productId};

  fetch(apiCall, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + options.jwtInfo.hashedBlob,
    },
    body: JSON.stringify(data),
  })
    .then(response =>
      response.json().then(result => ({status: response.status, body: result})),
    )
    .then(obj => {
      return callback(obj);
    });
}

export function addFavorite(options, callback) {
  const apiCall = API_URL + '/favorites/add-favorite';

  const data = {productId: options.productId};

  fetch(apiCall, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + options.jwtInfo.hashedBlob,
    },
    body: JSON.stringify(data),
  })
    .then(response =>
      response.json().then(result => ({status: response.status, body: result})),
    )
    .then(obj => {
      return callback(obj);
    });
}

export function removeFavorite(options, callback) {
  const apiCall = API_URL + '/favorites/remove-favorite';

  const data = {productId: options.productId};

  fetch(apiCall, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + options.jwtInfo.hashedBlob,
    },
    body: JSON.stringify(data),
  })
    .then(response =>
      response.json().then(result => ({status: response.status, body: result})),
    )
    .then(obj => {
      return callback(obj);
    });
}
