import {API_URL} from 'react-native-dotenv';

export function getProductByBarcode(options, callback) {
  const apiCall = API_URL + '/products/get-product-by-barcode';

  const data = {barcode: options.barcode};

  fetch(apiCall, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + options.jwtInfo.hashedBlob,
    },
    body: JSON.stringify(data),
  })
    .then(response =>
      response.json().then(result => ({status: response.status, body: result})),
    )
    .then(obj => {
      return callback(obj);
    });
}
