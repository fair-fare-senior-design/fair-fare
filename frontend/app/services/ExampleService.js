import {API_URL} from 'react-native-dotenv';

export function examplePost(options) {
  const apiCall = API_URL + '/example-path/insert-data';

  const data = {
    something: options.something,
    somethingElse: options.somethingElse,
  };

  fetch(apiCall, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  });
}
