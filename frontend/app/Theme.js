export default {
  LIGHTER_GREEN: 'rgb(130, 180, 150)',
  LIGHT_GREEN: 'rgb(94, 139, 111)',
  DARK_GREEN: 'rgb(67, 110, 79)',
  LIGHTEST_ORANGE: 'rgb(250, 224, 208)',
  LIGHTER_ORANGE: 'rgb(245, 173, 129)',
  LIGHT_ORANGE: 'rgb(250, 143, 77)',
  DARK_ORANGE: 'rgb(248, 120, 41)',
  DARKEST_ORANGE: 'rgb(203, 99, 34)',
  LIGHT_GREY: 'rgb(242, 239, 234)',
  MEDIUM_LIGHT_GREY: 'rgb(220, 218, 213)',
  MEDIUM_GREY: 'rgb(199, 196, 192)',
  DARK_GREY: 'rgb(51, 51, 51)',
  BLACK: 'rgb(19, 19, 19)',
  WHITE: 'rgb(248, 248, 248)',
  ERROR_RED: 'rgb(176,0,32)',
  WARNING_RED: 'rgb(200, 70, 90)',
};
