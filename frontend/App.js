import React from 'react';

import {createRootNavigator} from './app/Router';
import {checkLoggedIn} from './app/services/AuthService';

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loggedIn: false,
      checkedLoggedIn: false,
    };
  }

  componentDidMount() {
    const self = this;

    checkLoggedIn(function(response) {
      self.setState({loggedIn: response, checkedLoggedIn: true});
    });
  }

  render() {
    if (!this.state.checkedLoggedIn) {
      return null;
    }

    const Layout = createRootNavigator(this.state.loggedIn);

    return <Layout />;
  }
}
