from flask import Flask
from flask_cors import CORS

from controllers.example_controller import example_controller
from controllers.auth_controller import auth_controller
from controllers.user_controller import user_controller
from controllers.company_controller import company_controller
from controllers.product_controller import product_controller
from controllers.favorite_controller import favorite_controller
from controllers.search_controller import search_controller
from controllers.following_controller import following_controller


app = Flask(__name__)
app.config['SECRET_KEY'] = 'MySecretKey'
app.config['CORS_HEADERS'] = 'Content-Type'

CORS(app, resources={r'/*': {'origins': '*'}})

app.register_blueprint(example_controller, url_prefix='/example-path')
app.register_blueprint(auth_controller, url_prefix='/auth')
app.register_blueprint(user_controller, url_prefix='/users')
app.register_blueprint(company_controller, url_prefix='/companies')
app.register_blueprint(product_controller, url_prefix='/products')
app.register_blueprint(favorite_controller, url_prefix='/favorites')
app.register_blueprint(search_controller, url_prefix='/search')
app.register_blueprint(following_controller, url_prefix='/following')


@app.route('/', methods=['GET'])
def hello_world():
    return 'Hello, World!'


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=8080, threaded=True)
