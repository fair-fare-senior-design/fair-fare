from db.dao.favorite_dao import FavoriteDao
from db.dao.product_dao import ProductDao
from db.dao.company_dao import CompanyDao

from models.favorite_model import FavoriteModel
from models.user_model import UserModel

from utilities.uuid_utilities import UuidUtilities


class Favorite():

    @staticmethod
    def add_favorite(mysql_client, user_id, product_id):
        favorite_dao = FavoriteDao(mysql_client)

        id = UuidUtilities.generate_uuid()

        favorite = FavoriteModel(id, user_id, product_id)

        favorite_dao.add_favorite(favorite)

    @staticmethod
    def get_favorites_for_user(mysql_client, user_id):
        favorite_dao = FavoriteDao(mysql_client)
        product_dao = ProductDao(mysql_client)
        company_dao = CompanyDao(mysql_client)

        user = UserModel(user_id, None, None, None, None, None, None, None,
                         None)

        product_ids = favorite_dao.get_favorites_for_user(user)
        products = []

        for product_id in product_ids:
            product_model = product_dao.get_product(product_id)

            unformatted_company_id = UuidUtilities.unformat_uuid(
                product_model.get_company_id()
            )
            company_model = company_dao.get_company_by_id(
                unformatted_company_id
            )

            products.append({
                'id': product_model.get_id(),
                'name': product_model.get_name(),
                'company': company_model.get_name(),
                'environmentRating': product_model.get_environment_rating(),
                'ethicalRating': product_model.get_ethical_rating(),
                'healthRating': product_model.get_health_rating(),
                'picture': product_model.get_picture(),
                'barcode': product_model.get_barcode(),
                'flag': product_model.get_flag(),
                'flagDescription': product_model.get_flag_description()
            })

        return products

    @staticmethod
    def remove_favorite(mysql_client, user_id, product_id):
        favorite_dao = FavoriteDao(mysql_client)

        favorite = FavoriteModel('', user_id, product_id)

        favorite_with_id = favorite_dao.get_favorite(favorite)

        favorite_dao.remove_favorite(favorite_with_id)
