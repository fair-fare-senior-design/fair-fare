import os
import unittest
from datetime import datetime

from services.mysql_client import MySQLClient
from services.favorite import Favorite

from utilities.auth_utilities import AuthUtilities
from utilities.uuid_utilities import UuidUtilities


class FavoriteTests(unittest.TestCase):

	def setUp(self):
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			mysql_client.execute('DELETE FROM AppFavorite', ())
			mysql_client.execute('DELETE FROM AppFollowing', ())
			mysql_client.execute('DELETE FROM AppProduct', ())
			mysql_client.execute('DELETE FROM AppCompany', ())
			mysql_client.execute('DELETE FROM AppUser', ())

			# Create a user

			self.__user_id = UuidUtilities.generate_uuid()
			salt = AuthUtilities.generate_salt()
			hashed_password = AuthUtilities.hash_password('testpass', salt)
			date_joined = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

			query = 'INSERT INTO `AppUser` \
				(`AppUserId`, \
				`Email`, \
				`FirstName`, \
				`UserName`, \
				`Bio`, \
				`DateJoined`, \
				`ProfilePicture`, \
				`HashedPassword`, \
				`Salt`) \
				VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)'

			parameters = (
				self.__user_id, 'test@email.com', '', 'testuser', '', date_joined, '', hashed_password, salt
			)

			mysql_client.execute(query, parameters)

			# Create a company

			self.__company_id = UuidUtilities.generate_uuid()
			self.__product_id = UuidUtilities.generate_uuid()

			query = 'INSERT INTO `AppCompany` \
				(`AppCompanyId`, `Name`) \
				VALUES (%s, %s)'

			parameters = (
				self.__company_id,
				'Test Company'
			)

			mysql_client.execute(query, parameters)

			# Create a product for that company

			query = 'INSERT INTO `AppProduct` \
				(`AppProductId`, `CompanyId`, `Name`, `Barcode`, \
				`Picture`, `EnvironmentRating`, `EthicalRating`, \
				`HealthRating`, `Flag`, `FlagDescription`) \
				VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'

			parameters = (
				self.__product_id,
				self.__company_id,
				'Test Product',
				'000000000000',
				'',
				0,
				0,
				0,
				0,
				''
			)

			mysql_client.execute(query, parameters)

		finally:

			mysql_client.close()

	def tearDown(self):
		pass

	def test_add_favorite(self):
		'''
		test Favorite.add_favorite()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			Favorite.add_favorite(mysql_client, self.__user_id, self.__product_id)

			query = 'SELECT * FROM `AppFavorite` WHERE `UserId` = %s AND `ProductId` = %s'

			response = mysql_client.execute_query(query, (self.__user_id, self.__product_id))[0]

			self.assertNotEqual(len(response), 0)

		finally:

			mysql_client.close()

	def test_get_favorites_for_user(self):
		'''
		test Favorite.get_favorites_for_user()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			# Create a favorite between a user and a product

			query = 'INSERT INTO `AppFavorite` \
				(`AppFavoriteId`, `UserId`, `ProductId`) \
				VALUES (%s, %s, %s)'

			parameters = (
				UuidUtilities.generate_uuid(),
				self.__user_id,
				self.__product_id,
			)

			mysql_client.execute(query, parameters)

			favorites = Favorite.get_favorites_for_user(mysql_client, self.__user_id)

			self.assertEqual(favorites[0]['name'], 'Test Product')

		finally:

			mysql_client.close()

	def test_remove_favorite(self):
		'''
		test Favorite.remove_favorite()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			query = 'INSERT INTO `AppFavorite` \
				(`AppFavoriteId`, `UserId`, `ProductId`) \
				VALUES (%s, %s, %s)'

			parameters = (
				UuidUtilities.generate_uuid(),
				self.__user_id,
				self.__product_id,
			)

			mysql_client.execute(query, parameters)

			Favorite.remove_favorite(mysql_client, self.__user_id, self.__product_id)

			query = 'SELECT * FROM `AppFavorite` WHERE `UserId` = %s AND `ProductId` = %s'

			response = mysql_client.execute_query(query, (self.__user_id, self.__product_id))

			self.assertEqual(len(response), 0)

		finally:

			mysql_client.close()