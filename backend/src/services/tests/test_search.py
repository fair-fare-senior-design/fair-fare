import os
import unittest
from datetime import datetime

from services.mysql_client import MySQLClient
from services.search import Search

from utilities.auth_utilities import AuthUtilities
from utilities.uuid_utilities import UuidUtilities

from exceptions.search_exceptions import InvalidSearchCategoryException


class SearchTests(unittest.TestCase):

	def setUp(self):
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			mysql_client.execute('DELETE FROM AppFavorite', ())
			mysql_client.execute('DELETE FROM AppFollowing', ())
			mysql_client.execute('DELETE FROM AppProduct', ())
			mysql_client.execute('DELETE FROM AppCompany', ())
			mysql_client.execute('DELETE FROM AppUser', ())

			# Create a user

			self.__user_id = UuidUtilities.generate_uuid()
			salt = AuthUtilities.generate_salt()
			hashed_password = AuthUtilities.hash_password('testpass', salt)
			date_joined = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

			query = 'INSERT INTO `AppUser` \
				(`AppUserId`, \
				`Email`, \
				`FirstName`, \
				`UserName`, \
				`Bio`, \
				`DateJoined`, \
				`ProfilePicture`, \
				`HashedPassword`, \
				`Salt`) \
				VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)'

			parameters = (
				self.__user_id, 'test@email.com', '', 'testuser', '', date_joined, '', hashed_password, salt
			)

			mysql_client.execute(query, parameters)

			# Create a company

			self.__company_id = UuidUtilities.generate_uuid()
			self.__product_id = UuidUtilities.generate_uuid()

			query = 'INSERT INTO `AppCompany` \
				(`AppCompanyId`, `Name`) \
				VALUES (%s, %s)'

			parameters = (
				self.__company_id,
				'Test Company'
			)

			mysql_client.execute(query, parameters)

			# Create a product for that company

			query = 'INSERT INTO `AppProduct` \
				(`AppProductId`, `CompanyId`, `Name`, `Barcode`, \
				`Picture`, `EnvironmentRating`, `EthicalRating`, \
				`HealthRating`, `Flag`, `FlagDescription`) \
				VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'

			parameters = (
				self.__product_id,
				self.__company_id,
				'Test Product',
				'000000000000',
				'',
				0,
				0,
				0,
				0,
				''
			)

			mysql_client.execute(query, parameters)

		finally:

			mysql_client.close()

	def tearDown(self):
		pass

	def test_search(self):
		'''
		test Search.search()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			# Try to search for a person that exists by username

			result = Search.search(mysql_client, 'person', 'testuser')

			self.assertEqual(len(result), 1)

			# Try to search for a company that exists by name

			result = Search.search(mysql_client, 'company', 'Test Company')

			self.assertEqual(len(result), 1)

			# Try to search for a product that exists by name

			result = Search.search(mysql_client, 'product', 'Test Product')

			self.assertEqual(len(result), 1)

			# Try to search for a product that doesn't exist by name

			result = Search.search(mysql_client, 'product', 'Non-existent')

			self.assertEqual(len(result), 0)			

			# Try to search in a category that doesn't exist

			with self.assertRaises(InvalidSearchCategoryException) as exception:
				Search.search(mysql_client, 'food', 'something')

		finally:

			mysql_client.close()