import os
import unittest

from services.mysql_client import MySQLClient
from services.company import Company

from utilities.uuid_utilities import UuidUtilities

from exceptions.company_exceptions import CompanyExistsException
from exceptions.company_exceptions import CompanyNotFoundException
from exceptions.company_exceptions import ProductsInCompanyException


class CompanyTests(unittest.TestCase):

	def setUp(self):
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			mysql_client.execute('DELETE FROM AppFavorite', ())
			mysql_client.execute('DELETE FROM AppFollowing', ())
			mysql_client.execute('DELETE FROM AppProduct', ())
			mysql_client.execute('DELETE FROM AppCompany', ())
			mysql_client.execute('DELETE FROM AppUser', ())

			# Create a company

			query = 'INSERT INTO `AppCompany` \
				(`AppCompanyId`, `Name`) \
				VALUES (%s, %s)'

			parameters = (
				UuidUtilities.generate_uuid(),
				'Test Company'
			)

			mysql_client.execute(query, parameters)

		finally:

			mysql_client.close()

	def tearDown(self):
		pass

	def test_add_company(self):
		'''
		test Company.add_company()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			# Try to create a company with a unique name

			Company.add_company(mysql_client, 'Test Company 2')

			query = 'SELECT * FROM `AppCompany` WHERE `Name` = %s'

			response = mysql_client.execute_query(query, ('Test Company 2'))[0]

			self.assertNotEqual(len(response), 0)

			# Try to create a company with a non-unique name

			with self.assertRaises(CompanyExistsException) as exception:
				Company.add_company(mysql_client, 'Test Company')

		finally:

			mysql_client.close()

	def test_get_companies(self):
		'''
		test Company.get_companies()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			companies = Company.get_companies(mysql_client)

			self.assertEqual(companies[0]['name'], 'Test Company')

		finally:

			mysql_client.close()

	def test_get_company(self):
		'''
		test Company.get_company()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			# Try to get a company that exists

			company = Company.get_company(mysql_client, 'Test Company')

			self.assertEqual(company.get_name(), 'Test Company')

			# Try to get a company that doesn't exist

			with self.assertRaises(CompanyNotFoundException) as exception:
				Company.get_company(mysql_client, 'Test Company 2')

		finally:

			mysql_client.close()

	def test_remove_company(self):
		'''
		test Company.remove_company()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			# Try to remove a company that exists

			Company.remove_company(mysql_client, 'Test Company')

			query = 'SELECT * FROM `AppCompany` WHERE `Name` = %s'

			response = mysql_client.execute_query(query, ('Test Company'))

			self.assertEqual(len(response), 0)

			# Try to remove a company that doesn't exist

			with self.assertRaises(CompanyNotFoundException) as exception:
				Company.remove_company(mysql_client, 'Test Company 2')

		finally:

			mysql_client.close()