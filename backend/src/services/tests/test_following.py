import os
import unittest
from datetime import datetime

from services.mysql_client import MySQLClient
from services.following import Following

from models.user_model import UserModel

from utilities.auth_utilities import AuthUtilities
from utilities.uuid_utilities import UuidUtilities


class FollowingTests(unittest.TestCase):

	def setUp(self):
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			mysql_client.execute('DELETE FROM AppFavorite', ())
			mysql_client.execute('DELETE FROM AppFollowing', ())
			mysql_client.execute('DELETE FROM AppProduct', ())
			mysql_client.execute('DELETE FROM AppCompany', ())
			mysql_client.execute('DELETE FROM AppUser', ())

			# Create first user

			id = UuidUtilities.generate_uuid()
			salt = AuthUtilities.generate_salt()
			hashed_password = AuthUtilities.hash_password('testpass', salt)
			date_joined = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

			query = 'INSERT INTO `AppUser` \
				(`AppUserId`, \
				`Email`, \
				`FirstName`, \
				`UserName`, \
				`Bio`, \
				`DateJoined`, \
				`ProfilePicture`, \
				`HashedPassword`, \
				`Salt`) \
				VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)'

			self.__first_user = UserModel(id, 'test1@email.com', '', 'testuser1', '', date_joined, '', hashed_password, salt)

			parameters = (
				self.__first_user.get_id(),
				self.__first_user.get_email(),
				self.__first_user.get_first_name(),
				self.__first_user.get_username(),
				self.__first_user.get_bio(),
				self.__first_user.get_date_joined(),
				self.__first_user.get_profile_picture(),
				self.__first_user.get_hashed_password(),
				self.__first_user.get_salt()
			)

			mysql_client.execute(query, parameters)

			self.__first_user.set_id(UuidUtilities.format_uuid(self.__first_user.get_id()))

			# Create second user

			id = UuidUtilities.generate_uuid()
			salt = AuthUtilities.generate_salt()
			hashed_password = AuthUtilities.hash_password('testpass', salt)
			date_joined = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

			query = 'INSERT INTO `AppUser` \
				(`AppUserId`, \
				`Email`, \
				`FirstName`, \
				`UserName`, \
				`Bio`, \
				`DateJoined`, \
				`ProfilePicture`, \
				`HashedPassword`, \
				`Salt`) \
				VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)'

			self.__second_user = UserModel(id, 'test2@email.com', '', 'testuser2', '', date_joined, '', hashed_password, salt)

			parameters = (
				self.__second_user.get_id(),
				self.__second_user.get_email(),
				self.__second_user.get_first_name(),
				self.__second_user.get_username(),
				self.__second_user.get_bio(),
				self.__second_user.get_date_joined(),
				self.__second_user.get_profile_picture(),
				self.__second_user.get_hashed_password(),
				self.__second_user.get_salt()
			)

			mysql_client.execute(query, parameters)

			self.__second_user.set_id(UuidUtilities.format_uuid(self.__second_user.get_id()))

		finally:

			mysql_client.close()

	def tearDown(self):
		pass

	def test_follow(self):
		'''
		test Following.follow()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			Following.follow(mysql_client, self.__first_user, self.__second_user)

			query = 'SELECT `FollowerId` FROM `AppFollowing` WHERE `FolloweeId` = %s'

			response = mysql_client.execute_query(query, (UuidUtilities.unformat_uuid(self.__first_user.get_id())))[0]

			self.assertEqual(UuidUtilities.format_uuid(response[0]), self.__second_user.get_id())

		finally:

			mysql_client.close()

	def test_unfollow(self):
		'''
		test Following.unfollow()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			# Second user follows first user
			query = 'INSERT INTO `AppFollowing` \
				(`AppFollowingId`, \
				`FolloweeId`, \
				`FollowerId`) \
				VALUES (%s, %s, %s)'

			parameters = (
				UuidUtilities.generate_uuid(),
				UuidUtilities.unformat_uuid(self.__first_user.get_id()),
				UuidUtilities.unformat_uuid(self.__second_user.get_id()),
			)

			mysql_client.execute(query, parameters)

			# Try to make second user unfollow first user

			Following.unfollow(mysql_client, self.__first_user, self.__second_user)

			query = 'SELECT `FollowerId` FROM `AppFollowing` WHERE `FolloweeId` = %s'

			response = mysql_client.execute_query(query, (UuidUtilities.unformat_uuid(self.__first_user.get_id())))

			self.assertEqual(len(response), 0)

		finally:

			mysql_client.close()

	def test_get_followers(self):
		'''
		test Following.get_followers()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			# Second user follows first user
			query = 'INSERT INTO `AppFollowing` \
				(`AppFollowingId`, \
				`FolloweeId`, \
				`FollowerId`) \
				VALUES (%s, %s, %s)'

			parameters = (
				UuidUtilities.generate_uuid(),
				UuidUtilities.unformat_uuid(self.__first_user.get_id()),
				UuidUtilities.unformat_uuid(self.__second_user.get_id()),
			)

			mysql_client.execute(query, parameters)

			self.__first_user.set_id(UuidUtilities.unformat_uuid(self.__first_user.get_id()))

			followers = Following.get_followers(mysql_client, self.__first_user)

			self.assertEqual(len(followers), 1)

		finally:

			mysql_client.close()

	def test_get_followees(self):
		'''
		test Following.get_followees()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			# Second user follows first user
			query = 'INSERT INTO `AppFollowing` \
				(`AppFollowingId`, \
				`FolloweeId`, \
				`FollowerId`) \
				VALUES (%s, %s, %s)'

			parameters = (
				UuidUtilities.generate_uuid(),
				UuidUtilities.unformat_uuid(self.__first_user.get_id()),
				UuidUtilities.unformat_uuid(self.__second_user.get_id()),
			)

			mysql_client.execute(query, parameters)

			self.__second_user.set_id(UuidUtilities.unformat_uuid(self.__second_user.get_id()))

			followees = Following.get_followees(mysql_client, self.__second_user)

			self.assertEqual(len(followees), 1)

		finally:

			mysql_client.close()

	def test_check_following(self):
		'''
		test Following.check_following()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			# Second user follows first user
			query = 'INSERT INTO `AppFollowing` \
				(`AppFollowingId`, \
				`FolloweeId`, \
				`FollowerId`) \
				VALUES (%s, %s, %s)'

			parameters = (
				UuidUtilities.generate_uuid(),
				UuidUtilities.unformat_uuid(self.__first_user.get_id()),
				UuidUtilities.unformat_uuid(self.__second_user.get_id()),
			)

			mysql_client.execute(query, parameters)

			self.__first_user.set_id(UuidUtilities.unformat_uuid(self.__first_user.get_id()))
			self.__second_user.set_id(UuidUtilities.unformat_uuid(self.__second_user.get_id()))

			following = Following.check_following(mysql_client, self.__first_user, self.__second_user)

			self.assertTrue(following)

		finally:

			mysql_client.close()