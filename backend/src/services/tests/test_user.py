import os
import unittest
from datetime import datetime

from services.mysql_client import MySQLClient
from services.user import User

from utilities.uuid_utilities import UuidUtilities
from utilities.auth_utilities import AuthUtilities


class UserTests(unittest.TestCase):

	def setUp(self):
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			mysql_client.execute('DELETE FROM AppFavorite', ())
			mysql_client.execute('DELETE FROM AppFollowing', ())
			mysql_client.execute('DELETE FROM AppProduct', ())
			mysql_client.execute('DELETE FROM AppCompany', ())
			mysql_client.execute('DELETE FROM AppUser', ())

			# Create a user

			id = UuidUtilities.generate_uuid()
			salt = AuthUtilities.generate_salt()
			hashed_password = AuthUtilities.hash_password('testpass', salt)
			date_joined = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

			query = 'INSERT INTO `AppUser` \
				(`AppUserId`, \
				`Email`, \
				`FirstName`, \
				`UserName`, \
				`Bio`, \
				`DateJoined`, \
				`ProfilePicture`, \
				`HashedPassword`, \
				`Salt`) \
				VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)'

			parameters = (
				id, 'test@email.com', '', 'testuser', '', date_joined, '', hashed_password, salt
			)

			mysql_client.execute(query, parameters)

		finally:

			mysql_client.close()

	def tearDown(self):
		pass

	def test_get_user(self):
		'''
		test User.get_user()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			user = User.get_user(mysql_client, 'testuser')

			self.assertEqual(user.get_email(), 'test@email.com')

		finally:

			mysql_client.close()

	def test_update_user(self):
		'''
		test User.update_user()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			username = 'testuser'
			first_name = 'Test'
			bio = 'This is my bio.'

			User.update_user(mysql_client, username, first_name, bio)

			query = 'SELECT `UserName`, `FirstName`, `Bio` FROM `AppUser` WHERE `UserName` = %s'

			response = mysql_client.execute_query(query, ('testuser'))[0]

			self.assertEqual(response[0], 'testuser')
			self.assertEqual(response[1], 'Test')
			self.assertEqual(response[2], 'This is my bio.')

		finally:

			mysql_client.close()

	def test_save_photo_url_to_db(self):
		'''
		test User.save_photo_url_to_db()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			User.save_photo_url_to_db(mysql_client, 'testuser', 'picture.png')

			query = 'SELECT `ProfilePicture` FROM `AppUser` WHERE `UserName` = %s'

			response = mysql_client.execute_query(query, ('testuser'))[0]

			self.assertEqual(response[0], 'picture.png')

		finally:

			mysql_client.close()

	def test_delete_user(self):
		'''
		test User.delete_user()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			User.delete_user(mysql_client, 'testuser')

			query = 'SELECT * FROM `AppUser`'

			response = mysql_client.execute_query(query, ())

			self.assertEqual(len(response), 0)

		finally:

			mysql_client.close()