import os
import unittest

from services.mysql_client import MySQLClient
from services.product import Product

from models.product_model import ProductModel

from utilities.uuid_utilities import UuidUtilities

from exceptions.product_exceptions import CompanyNotFoundException
from exceptions.product_exceptions import ProductExistsException
from exceptions.product_exceptions import ProductNotFoundException


class ProductTests(unittest.TestCase):

	def setUp(self):
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			mysql_client.execute('DELETE FROM AppFavorite', ())
			mysql_client.execute('DELETE FROM AppFollowing', ())
			mysql_client.execute('DELETE FROM AppProduct', ())
			mysql_client.execute('DELETE FROM AppCompany', ())
			mysql_client.execute('DELETE FROM AppUser', ())

			# Create a company

			self.__company_id = UuidUtilities.generate_uuid()
			self.__product_id = UuidUtilities.generate_uuid()

			query = 'INSERT INTO `AppCompany` \
				(`AppCompanyId`, `Name`) \
				VALUES (%s, %s)'

			parameters = (
				self.__company_id,
				'Test Company'
			)

			mysql_client.execute(query, parameters)

			# Create a product for that company

			query = 'INSERT INTO `AppProduct` \
				(`AppProductId`, `CompanyId`, `Name`, `Barcode`, \
				`Picture`, `EnvironmentRating`, `EthicalRating`, \
				`HealthRating`, `Flag`, `FlagDescription`) \
				VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'

			parameters = (
				self.__product_id,
				self.__company_id,
				'Test Product',
				'000000000000',
				'',
				0,
				0,
				0,
				0,
				''
			)

			mysql_client.execute(query, parameters)

		finally:

			mysql_client.close()

	def tearDown(self):
		pass

	def test_add_product(self):
		'''
		test Product.add_product()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			# Try to create a unique product for a company that exists

			product = ProductModel(
				UuidUtilities.generate_uuid(),
				UuidUtilities.format_uuid(self.__company_id),
				'000000000000',
				'Test Product 2',
				'',
				0,
				0,
				0,
				0,
				''
			)

			Product.add_product(mysql_client, product)

			query = 'SELECT * FROM `AppProduct` WHERE `CompanyId` = %s AND `Name` = %s'

			response = mysql_client.execute_query(query, (self.__company_id, 'Test Product 2'))[0]

			self.assertNotEqual(len(response), 0)

			# Try to create a non-unique product for a company that exists

			product = ProductModel(
				UuidUtilities.generate_uuid(),
				UuidUtilities.format_uuid(self.__company_id),
				'000000000000',
				'Test Product',
				'',
				0,
				0,
				0,
				0,
				''
			)

			with self.assertRaises(ProductExistsException) as exception:
				Product.add_product(mysql_client, product)

			# Try to create a product for a company that doesn't exist

			product = ProductModel(
				UuidUtilities.generate_uuid(),
				UuidUtilities.format_uuid(UuidUtilities.generate_uuid()),	# Generate new company UUID
				'000000000000',
				'Test Product',
				'',
				0,
				0,
				0,
				0,
				''
			)

			with self.assertRaises(CompanyNotFoundException) as exception:
				Product.add_product(mysql_client, product)

		finally:

			mysql_client.close()

	def test_get_product(self):
		'''
		test Product.get_product()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			# Try to get an existing product by its barcode

			product = Product.get_product(mysql_client, '000000000000')

			self.assertEqual(product['product'].get_name(), 'Test Product')
			self.assertEqual(product['company'].get_name(), 'Test Company')

			# Try to get a non-existing product by its barcode

			with self.assertRaises(ProductNotFoundException) as exception:
				product = Product.get_product(mysql_client, '000000000001')

		finally:

			mysql_client.close()

	def test_remove_product(self):
		'''
		test Product.remove_product()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			# Try to remove an existing product by its ID

			Product.remove_product(mysql_client, UuidUtilities.format_uuid(self.__product_id))

			query = 'SELECT * FROM `AppProduct` WHERE `CompanyId` = %s AND `Name` = %s'

			response = mysql_client.execute_query(query, (self.__company_id, 'Test Product'))

			self.assertEqual(len(response), 0)

		finally:

			mysql_client.close()