import os
import unittest
from datetime import datetime

from services.mysql_client import MySQLClient
from services.auth import Auth

from models.credential_model import CredentialModel
from models.new_user_model import NewUserModel
from models.user_model import UserModel

from utilities.uuid_utilities import UuidUtilities
from utilities.auth_utilities import AuthUtilities

from exceptions.user_exceptions import EmailExistsException
from exceptions.user_exceptions import UsernameExistsException
from exceptions.user_exceptions import IncorrectCredentialsException
from exceptions.user_exceptions import UnverifiedTokenException


class AuthTests(unittest.TestCase):

	def setUp(self):
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			mysql_client.execute('DELETE FROM AppFavorite', ())
			mysql_client.execute('DELETE FROM AppFollowing', ())
			mysql_client.execute('DELETE FROM AppProduct', ())
			mysql_client.execute('DELETE FROM AppCompany', ())
			mysql_client.execute('DELETE FROM AppUser', ())

		finally:

			mysql_client.close()

	def tearDown(self):
		pass

	def test_sign_up(self):
		'''
		test Auth.sign_up()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			# Create a user in empty database (assert user exists).

			credentials = CredentialModel('testuser', 'testpass')
			new_user = NewUserModel(credentials, 'test@email.com', '')

			Auth.sign_up(mysql_client, new_user)

			query = 'SELECT * FROM `AppUser` WHERE `UserName` = %s'

			response = mysql_client.execute_query(query, ('testuser'))[0]

			self.assertNotEqual(len(response), 0)

			# Create a user with existing username (assert raises UsernameExistsException).

			credentials = CredentialModel('testuser', 'testpass')
			new_user = NewUserModel(credentials, 'test2@email.com', '')

			with self.assertRaises(UsernameExistsException) as exception:
				Auth.sign_up(mysql_client, new_user)

			# Create a user with existing email (assert raises EmailExistsException).

			credentials = CredentialModel('testuser2', 'testpass')
			new_user = NewUserModel(credentials, 'test@email.com', '')

			with self.assertRaises(EmailExistsException) as exception:
				Auth.sign_up(mysql_client, new_user)

		finally:

			mysql_client.close()

	def test_log_in(self):
		'''
		test Auth.log_in()
		'''
		mysql_client = MySQLClient(
			os.environ.get('MYSQL_HOST'),
			os.environ.get('MYSQL_DB'),
			os.environ.get('MYSQL_USER'),
			os.environ.get('MYSQL_PASS')
		)

		try:

			# Create a user

			id = UuidUtilities.generate_uuid()
			salt = AuthUtilities.generate_salt()
			hashed_password = AuthUtilities.hash_password('testpass', salt)
			date_joined = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

			query = 'INSERT INTO `AppUser` \
				(`AppUserId`, \
				`Email`, \
				`FirstName`, \
				`UserName`, \
				`Bio`, \
				`DateJoined`, \
				`ProfilePicture`, \
				`HashedPassword`, \
				`Salt`) \
				VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)'

			parameters = (
				id, 'test@email.com', '', 'testuser', '', date_joined, '', hashed_password, salt
			)

			mysql_client.execute(query, parameters)

			# Try to log in with correct credentials

			credentials = CredentialModel('testuser', 'testpass')

			token_object = Auth.log_in(mysql_client, credentials)

			self.assertIn('hashedBlob', token_object.keys())

			# Try to log in with incorrect credentials

			credentials = CredentialModel('testuser', 'wrong')

			with self.assertRaises(IncorrectCredentialsException) as exception:
				Auth.log_in(mysql_client, credentials)

		finally:

			mysql_client.close()