from models.company_model import CompanyModel

from db.dao.company_dao import CompanyDao

from utilities.uuid_utilities import UuidUtilities

from exceptions.company_exceptions import CompanyExistsException
from exceptions.company_exceptions import CompanyNotFoundException
from exceptions.company_exceptions import ProductsInCompanyException


class Company():

    @staticmethod
    def add_company(mysql_client, name):
        '''
        mysql_client (MySQLClient)
        name         (str)
        '''
        company_dao = CompanyDao(mysql_client)

        name_in_use = company_dao.name_exists(name)

        if name_in_use:
            raise CompanyExistsException

        id = UuidUtilities.generate_uuid()

        company = CompanyModel(id, name)

        company_dao.add_company(company)

    @staticmethod
    def get_companies(mysql_client):
        '''
        mysql_client (MySQLClient)
        '''
        company_dao = CompanyDao(mysql_client)

        companies = company_dao.get_companies()

        return companies

    @staticmethod
    def get_company(mysql_client, name):
        '''
        mysql_client (MySQLClient)
        name         (str)
        '''
        company_dao = CompanyDao(mysql_client)

        company_exists = company_dao.name_exists(name)

        if not company_exists:
            raise CompanyNotFoundException

        company = company_dao.get_company(name)

        return company

    @staticmethod
    def remove_company(mysql_client, name):
        '''
        mysql_client (MySQLClient)
        name         (str)
        '''
        company_dao = CompanyDao(mysql_client)

        company_exists = company_dao.name_exists(name)

        if not company_exists:
            raise CompanyNotFoundException

        company = company_dao.get_company(name)

        unformatted_uuid = UuidUtilities.unformat_uuid(company.get_id())
        company.set_id(unformatted_uuid)

        products_in_company = company_dao.products_in_company(company)

        if products_in_company:
            raise ProductsInCompanyException

        company_dao.remove_company(company)
