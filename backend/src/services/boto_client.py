import base64

import boto3
from botocore.exceptions import ClientError


class BotoClient():

    def __init__(self, key_id, secret_key, arn_address):
        self.__client = boto3.client('s3',
                                     aws_access_key_id=key_id,
                                     aws_secret_access_key=secret_key)

        self.__arn_address = arn_address

    def upload_to_bucket(self, picture, name, access_point, object_name):
        address = self.__arn_address + ':accesspoint/' + access_point

        if object_name is None:
            object_name = name

        try:
            response = self.__client.put_object(Body=base64.b64decode(picture),
                                                Bucket=address,
                                                Key=name)

            return response['ResponseMetadata']['HTTPStatusCode']

        except ClientError:
            raise ClientError

    def delete_from_bucket(self, name, access_point, object_name):
        address = self.__arn_address + ':accesspoint/' + access_point

        if object_name is None:
            object_name = name

        try:
            response = self.__client.delete_object(Bucket=address,
                                                   Key=name)

            return response['ResponseMetadata']['HTTPStatusCode']

        except ClientError:
            raise ClientError
