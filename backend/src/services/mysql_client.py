import pymysql as mariadb


class MySQLClient():

    def __init__(self, host, database, user, password):
        self.__connection = mariadb.connect(user=user,
                                            password=password,
                                            db=database,
                                            host=host)

    def execute_query(self, query, parameters):
        result = []

        cursor = self.__connection.cursor()
        cursor.execute(query, parameters)
        result = cursor.fetchall()

        self.__connection.commit()

        return result

    def execute(self, query, parameters):
        cursor = self.__connection.cursor()
        cursor.execute(query, parameters)

        self.__connection.commit()

    def close(self):
        self.__connection.close()
