from botocore.exceptions import ClientError

from db.dao.user_dao import UserDao

from models.user_model import UserModel

from utilities.uuid_utilities import UuidUtilities
from utilities.picture_utilities import PictureUtilities

from exceptions.user_exceptions import DeletePhotoException
from exceptions.user_exceptions import UploadPhotoException


class User():

    @staticmethod
    def get_user(mysql_client, username):
        user_dao = UserDao(mysql_client)

        user = UserModel(None, None, None, username, None, None, None, None,
                         None)

        user = user_dao.get_user(user)

        formatted_uuid = UuidUtilities.format_uuid(user.get_id())

        return UserModel(formatted_uuid,
                         user.get_email(),
                         user.get_first_name(),
                         user.get_username(),
                         user.get_bio(),
                         user.get_date_joined(),
                         user.get_profile_picture(),
                         None,
                         None)

    @staticmethod
    def update_user(mysql_client, username, first_name, bio):
        user_dao = UserDao(mysql_client)

        user = UserModel(None, None, first_name, username, bio, None, None,
                         None, None)

        user_dao.update_user(user)

    @staticmethod
    def upload_photo_to_s3(boto_client, data, type, user):
        '''
        Parameters:
        - boto_client (BotoClient)  : Amazon S3 Boto library client
        - data        (bytes)       : base64 encoded photo data
        - type        (string)      : type of data (e.g., 'image/jpeg')
        - user        (dict)        : dictionary containing user data
        '''
        photo = data.split('data:image/jpeg;base64,')[-1]
        name = PictureUtilities.generate_random_file_name(user.get_id(), type)

        try:
            if user.get_profile_picture() != 'newuser.jpg':
                boto_client.delete_from_bucket(user.get_profile_picture(),
                                               'delete-picture',
                                               None)
        except ClientError:
            raise DeletePhotoException

        try:
            boto_client.upload_to_bucket(photo, name, 'upload-picture', None)

            return name

        except ClientError:
            raise UploadPhotoException

    @staticmethod
    def save_photo_url_to_db(mysql_client, username, file_name):
        user_dao = UserDao(mysql_client)

        user = UserModel(None, None, None, username, None, None, None, None,
                         None)

        user_dao.update_user_photo(user, file_name)

    @staticmethod
    def get_user_uuid(mysql_client, username):
        user_dao = UserDao(mysql_client)
        user = user_dao.get_user(username)
        uuid = user.get_id()
        formatted_uuid = UuidUtilities.format_uuid(uuid)

        return formatted_uuid

    @staticmethod
    def delete_user(mysql_client, username):
        user_dao = UserDao(mysql_client)

        user = UserModel(None, None, None, username, None, None, None, None,
                         None)

        user_dao.delete_user(user)
