import sys

from services.auth import Auth
from services.user import User
from services.company import Company
from services.product import Product

from exceptions.user_exceptions import EmailExistsException
from exceptions.user_exceptions import UsernameExistsException
from exceptions.company_exceptions import CompanyExistsException
from exceptions.product_exceptions import ProductExistsException


class Importer():

    def __init__(self, mysql_client):
        self.__mysql_client = mysql_client

    def import_user(self, new_user):
        try:
            Auth.sign_up(self.__mysql_client, new_user)

        except EmailExistsException:
            print('Email already exists.', file=sys.stderr)

        except UsernameExistsException:
            print('Username already exists.', file=sys.stderr)

    def update_user(self, username, first_name, bio):
        User.update_user(self.__mysql_client, username, first_name, bio)

    def import_company(self, company):
        try:
            Company.add_company(self.__mysql_client, company)

        except CompanyExistsException:
            print('Company already exists.', file=sys.stderr)

        return Company.get_company(self.__mysql_client, company)

    def import_product(self, product):
        try:
            Product.add_product(self.__mysql_client, product)

        except ProductExistsException:
            print('Product already exists.', file=sys.stderr)
