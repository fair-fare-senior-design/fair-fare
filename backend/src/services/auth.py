from datetime import datetime

import jwt

from db.dao.user_dao import UserDao

from models.user_model import UserModel

from utilities.auth_utilities import AuthUtilities
from utilities.uuid_utilities import UuidUtilities

from exceptions.user_exceptions import EmailExistsException
from exceptions.user_exceptions import UsernameExistsException
from exceptions.user_exceptions import IncorrectCredentialsException
from exceptions.user_exceptions import UnverifiedTokenException


class Auth():

    @staticmethod
    def sign_up(mysql_client, new_user):
        '''
        new_user (NewUserModel)
        '''
        username = new_user.get_credentials().get_username()
        email = new_user.get_email()
        first_name = new_user.get_first_name()

        user_dao = UserDao(mysql_client)

        email_in_use = user_dao.email_exists(email)
        username_in_use = user_dao.username_exists(username)

        if email_in_use:
            raise EmailExistsException

        if username_in_use:
            raise UsernameExistsException

        id = UuidUtilities.generate_uuid()
        date_joined = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        # Need to hash plaintext password with a random salt
        password = new_user.get_credentials().get_password()
        salt = AuthUtilities.generate_salt()
        hashed_password = AuthUtilities.hash_password(password, salt)
        profile_picture = 'newuser.jpg'

        user = UserModel(id, email, first_name, username, '', date_joined,
                         profile_picture, hashed_password, salt)

        user_dao.add_user(user)

    @staticmethod
    def log_in(mysql_client, credential_model):
        '''
        credential_model (CredentialModel)
        '''
        username = credential_model.get_username()
        password = credential_model.get_password()

        user_dao = UserDao(mysql_client)

        username_exists = user_dao.username_exists(username)

        if not username_exists:
            raise IncorrectCredentialsException

        db_hashed_password = user_dao.get_hashed_password(username)
        hashed_password = AuthUtilities.hash_password(
            password,
            db_hashed_password.encode('utf-8')
        ).decode('utf-8')

        if hashed_password != db_hashed_password:
            raise IncorrectCredentialsException

        else:
            user = UserModel(None, None, None, username, None, None, None,
                             None, None)

            user = user_dao.get_user(user)

            date_joined = user.get_date_joined().strftime('%Y-%m-%d %H:%M:%S')

            blob = {
                'username': user.get_username(),
                'issuer': 'FairFare',
                'email': user.get_email(),
                'dateJoined': date_joined
            }

            hashed_blob = jwt.encode(blob, 'secret', algorithm='HS256')

            return {
                'blob': blob,
                'hashedBlob': hashed_blob.decode('utf-8')
            }

    @staticmethod
    def verify_token(token):
        try:
            payload = jwt.decode(token, 'secret', algorithms='HS256')
            return payload['username']
        except jwt.ExpiredSignatureError:
            raise UnverifiedTokenException
