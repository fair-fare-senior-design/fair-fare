import os
import urllib

from db.dao.following_dao import FollowingDao
from db.dao.user_dao import UserDao

from models.following_model import FollowingModel
from models.user_model import UserModel

from utilities.uuid_utilities import UuidUtilities


class Following():

    @staticmethod
    def follow(mysql_client, followee, user):
        following_dao = FollowingDao(mysql_client)

        followee_id = UuidUtilities.unformat_uuid(followee.get_id())
        follower_id = UuidUtilities.unformat_uuid(user.get_id())

        following_id = UuidUtilities.generate_uuid()

        following = FollowingModel(following_id, followee_id, follower_id)

        following_dao.follow(following)

    @staticmethod
    def unfollow(mysql_client, followee, user):
        following_dao = FollowingDao(mysql_client)

        followee_id = UuidUtilities.unformat_uuid(followee.get_id())
        follower_id = UuidUtilities.unformat_uuid(user.get_id())

        following = FollowingModel(None, followee_id, follower_id)

        following_with_id = following_dao.get_following(following)

        following_dao.unfollow(following_with_id)

    @staticmethod
    def get_followers(mysql_client, user):
        following_dao = FollowingDao(mysql_client)
        user_dao = UserDao(mysql_client)

        followers = following_dao.get_followers(user)

        follower_dicts = []

        for follower in followers:
            user = UserModel(follower.get_follower_id(), None, None, None,
                             None, None, None, None, None)

            user = user_dao.get_user_by_id(user)

            following_id = UuidUtilities.format_uuid(follower.get_id())
            user_id = UuidUtilities.format_uuid(follower.get_follower_id())

            picture = urllib.parse.quote_plus(user.get_profile_picture())
            picture_url = os.environ.get('AWS_S3_ADDRESS') + '/' + picture

            follower_dicts.append({
                'followingId': following_id,
                'userId': user_id,
                'username': user.get_username(),
                'firstName': user.get_first_name(),
                'bio': user.get_bio(),
                'dateJoined': user.get_date_joined(),
                'profilePictureUrl': picture_url
            })

        return follower_dicts

    @staticmethod
    def get_followees(mysql_client, user):
        following_dao = FollowingDao(mysql_client)
        user_dao = UserDao(mysql_client)

        followees = following_dao.get_followees(user)

        followee_dicts = []

        for followee in followees:
            user = UserModel(followee.get_followee_id(), None, None, None,
                             None, None, None, None, None)

            user = user_dao.get_user_by_id(user)

            following_id = UuidUtilities.format_uuid(followee.get_id())
            user_id = UuidUtilities.format_uuid(followee.get_followee_id())

            picture = urllib.parse.quote_plus(user.get_profile_picture())
            picture_url = os.environ.get('AWS_S3_ADDRESS') + '/' + picture

            followee_dicts.append({
                'followingId': following_id,
                'userId': user_id,
                'username': user.get_username(),
                'firstName': user.get_first_name(),
                'bio': user.get_bio(),
                'dateJoined': user.get_date_joined(),
                'profilePictureUrl': picture_url
            })

        return followee_dicts

    @staticmethod
    def check_following(mysql_client, followee, user):
        following_dao = FollowingDao(mysql_client)

        following = FollowingModel(None, followee.get_id(), user.get_id())

        return following_dao.check_following(following)
