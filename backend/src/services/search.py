import os
import urllib

from db.dao.company_dao import CompanyDao
from db.dao.product_dao import ProductDao
from db.dao.user_dao import UserDao

from utilities.uuid_utilities import UuidUtilities

from exceptions.search_exceptions import InvalidSearchCategoryException


class Search():

    @staticmethod
    def search(mysql_client, category, search_term):
        '''
        mysql_client (MySQLClient)
        category     (string)
        search_term  (string)
        '''
        result = []

        if category == 'company':
            company_dao = CompanyDao(mysql_client)
            company = company_dao.search_for_company(search_term)
            unformatted_uuid = UuidUtilities.unformat_uuid(
                company.get_id()
            )
            company.set_id(unformatted_uuid)

            products = company_dao.get_products_in_company(company)

            for product in products:
                picture = urllib.parse.quote_plus(product.get_picture())
                picture_url = (
                    os.environ.get('AWS_S3_ADDRESS') +
                    '/products/' +
                    picture
                )

                result.append({
                    'name': product.get_name(),
                    'id': product.get_id(),
                    'company': company.get_name(),
                    'pictureUrl': picture_url,
                    'environmentRating': product.get_environment_rating(),
                    'ethicalRating': product.get_ethical_rating(),
                    'healthRating': product.get_health_rating(),
                    'barcode': product.get_barcode(),
                    'flag': product.get_flag(),
                    'flagDescription': product.get_flag_description()
                })

        elif category == 'product':
            company_dao = CompanyDao(mysql_client)
            product_dao = ProductDao(mysql_client)

            products = product_dao.search_for_product(search_term)

            for product in products:
                unformatted_company_id = UuidUtilities.unformat_uuid(
                    product.get_company_id()
                )
                company_name = company_dao.get_company_by_id(
                    unformatted_company_id
                ).get_name()

                result.append({
                    'id': product.get_id(),
                    'name': product.get_name(),
                    'company': company_name,
                    'pictureUrl': product.get_picture(),
                    'environmentRating': product.get_environment_rating(),
                    'ethicalRating': product.get_ethical_rating(),
                    'healthRating': product.get_health_rating(),
                    'barcode': product.get_barcode(),
                    'flag': product.get_flag(),
                    'flagDescription': product.get_flag_description()
                })

        elif category == 'person':
            user_dao = UserDao(mysql_client)
            users = user_dao.search_for_user(search_term)

            for user in users:
                encoded_picture = urllib.parse.quote_plus(
                    user.get_profile_picture()
                )
                picture_url = (
                    os.environ.get('AWS_S3_ADDRESS') +
                    '/' +
                    encoded_picture
                )

                formatted_user_id = UuidUtilities.format_uuid(user.get_id())

                result.append({
                    'id': formatted_user_id,
                    'firstName': user.get_first_name(),
                    'username': user.get_username(),
                    'pictureUrl': picture_url
                })

        else:
            raise InvalidSearchCategoryException

        return result
