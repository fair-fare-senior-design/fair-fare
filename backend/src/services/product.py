from db.dao.product_dao import ProductDao
from db.dao.company_dao import CompanyDao

from models.company_model import CompanyModel
from models.product_model import ProductModel

from utilities.uuid_utilities import UuidUtilities

from exceptions.product_exceptions import CompanyNotFoundException
from exceptions.product_exceptions import ProductExistsException
from exceptions.product_exceptions import ProductNotFoundException


class Product():

    @staticmethod
    def add_product(mysql_client, product):
        '''
        mysql_client (MySQLClient)
        product      (ProductModel)
        '''
        product_dao = ProductDao(mysql_client)
        company_dao = CompanyDao(mysql_client)

        product.set_company_id(
            UuidUtilities.unformat_uuid(product.get_company_id())
        )

        company_exists = company_dao.id_exists(product.get_company_id())
        product_exists = product_dao.product_exists(product)

        if not company_exists:
            raise CompanyNotFoundException

        if product_exists:
            raise ProductExistsException

        product_id = UuidUtilities.generate_uuid()

        product.set_id(product_id)

        product_dao.add_product(product)

    @staticmethod
    def get_product(mysql_client, barcode):
        product_dao = ProductDao(mysql_client)
        company_dao = CompanyDao(mysql_client)

        if barcode == '0' or barcode == 0:
            raise ProductNotFoundException

        product = product_dao.get_product_by_barcode(barcode)

        if product is None:
            raise ProductNotFoundException

        else:
            unformatted_company_id = UuidUtilities.unformat_uuid(
                product.get_company_id()
            )
            company_name = company_dao.get_company_by_id(
                unformatted_company_id
            ).get_name()

            return {
                'company': CompanyModel(product.get_company_id(),
                                        company_name),
                'product': ProductModel(product.get_id(),
                                        product.get_company_id(),
                                        product.get_barcode(),
                                        product.get_name(),
                                        product.get_picture(),
                                        product.get_environment_rating(),
                                        product.get_ethical_rating(),
                                        product.get_health_rating(),
                                        product.get_flag(),
                                        product.get_flag_description())
            }

    @staticmethod
    def remove_product(mysql_client, product_id):
        product_dao = ProductDao(mysql_client)

        unformatted_uuid = UuidUtilities.unformat_uuid(product_id)

        product_model = product_dao.get_product(unformatted_uuid)

        product_model.set_id(unformatted_uuid)

        product_dao.remove_product(product_model)
