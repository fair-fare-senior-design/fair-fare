class Error(Exception):
    '''Base class for other exceptions'''
    pass


class CompanyNotFoundException(Error):
    '''Company doesn't exist.'''
    pass


class ProductExistsException(Error):
    '''Product already exists.'''
    pass


class ProductNotFoundException(Error):
    '''Product doesn't exist.'''
    pass
