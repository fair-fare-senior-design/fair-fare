class Error(Exception):
    '''Base class for other exceptions'''
    pass


class EmailExistsException(Error):
    '''Email address is already in use.'''
    pass


class UsernameExistsException(Error):
    '''Username is already in use.'''
    pass


class IncorrectCredentialsException(Error):
    '''Credentials do not match anything in the database.'''
    pass


class UnverifiedTokenException(Error):
    '''Token could not be verified.'''
    pass


class DeletePhotoException(Error):
    '''Photo could not be deleted from Amazon S3 bucket.'''
    pass


class UploadPhotoException(Error):
    '''Photo could not be uploaded to Amazon S3 bucket.'''
    pass
