class Error(Exception):
    '''Base class for other exceptions'''
    pass


class InvalidSearchCategoryException(Error):
    '''Invalid search category'''
    pass
