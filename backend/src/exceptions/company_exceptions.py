class Error(Exception):
    '''Base class for other exceptions'''
    pass


class CompanyExistsException(Error):
    '''Company already exists.'''
    pass


class CompanyNotFoundException(Error):
    '''Company doesn't exist.'''
    pass


class ProductsInCompanyException(Error):
    '''Company contains at least one product.'''
    pass
