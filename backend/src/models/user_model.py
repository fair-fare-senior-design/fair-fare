class UserModel():
    '''
    id                   (bytes)
    email                (str)
    first_name           (str)
    username             (str)
    bio                  (str)
    date_joined          (datetime)
    profile_picture      (str)
    hashed_password      (str)
    salt                 (str)
    '''

    def __init__(self, id, email, first_name, username, bio, date_joined, profile_picture, hashed_password, salt):    # noqa
        self.__id = id
        self.__email = email
        self.__first_name = first_name
        self.__username = username
        self.__bio = bio
        self.__date_joined = date_joined
        self.__profile_picture = profile_picture
        self.__hashed_password = hashed_password
        self.__salt = salt

    def set_id(self, id):
        self.__id = id

    def set_email(self, email):
        self.__email = email

    def set_first_name(self, first_name):
        self.__first_name = first_name

    def set_username(self, username):
        self.__username = username

    def set_bio(self, bio):
        self.__bio = bio

    def set_date_joined(self, date_joined):
        self.__date_joined = date_joined

    def set_profile_picture(self, profile_picture):
        self.__profile_picture = profile_picture

    def set_hashed_password(self, hashed_password):
        self.__hashed_password = hashed_password

    def set_salt(self, salt):
        self.__salt = salt

    def get_id(self):
        return self.__id

    def get_email(self):
        return self.__email

    def get_first_name(self):
        return self.__first_name

    def get_username(self):
        return self.__username

    def get_bio(self):
        return self.__bio

    def get_date_joined(self):
        return self.__date_joined

    def get_profile_picture(self):
        return self.__profile_picture

    def get_hashed_password(self):
        return self.__hashed_password

    def get_salt(self):
        return self.__salt
