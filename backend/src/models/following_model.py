class FollowingModel():
    '''
    id              (bytes)
    followee_id     (bytes)
    follower_id     (bytes)
    '''
    def __init__(self, id, followee_id, follower_id):
        self.__id = id
        self.__followee_id = followee_id
        self.__follower_id = follower_id

    def set_id(self, id):
        self.__id = id

    def set_followee_id(self, followee_id):
        self.__followee_id = followee_id

    def set_follower_id(self, follower_id):
        self.__follower_id = follower_id

    def get_id(self):
        return self.__id

    def get_followee_id(self):
        return self.__followee_id

    def get_follower_id(self):
        return self.__follower_id
