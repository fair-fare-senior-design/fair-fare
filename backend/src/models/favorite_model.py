class FavoriteModel():
    '''
    id              (bytes)
    user_id         (bytes)
    product_id      (bytes)
    '''
    def __init__(self, id, user_id, product_id):
        self.__id = id
        self.__user_id = user_id
        self.__product_id = product_id

    def set_id(self, id):
        self.__id = id

    def set_user_id(self, user_id):
        self.__user_id = user_id

    def set_product_id(self, product_id):
        self.__product_id = product_id

    def get_id(self):
        return self.__id

    def get_user_id(self):
        return self.__user_id

    def get_product_id(self):
        return self.__product_id
