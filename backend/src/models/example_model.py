class ExampleModel():

    def __init__(self, member_one, member_two):
        self.__member_one = member_one      # private variables have __ prefix
        self.__member_two = member_two

    def set_member_one(self, value):
        self.__member_one = value

    def set_member_two(self, value):
        self.__member_two = value

    def get_member_one(self):
        return self.__member_one

    def get_member_two(self):
        return self.__member_two

    def get_dict(self):
        return {
            'member_one': self.__member_one,
            'member_two': self.__member_two
        }
