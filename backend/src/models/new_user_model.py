class NewUserModel():
    '''
    credentials (CredentialModel)
    email       (str)
    first_name  (str)
    '''

    def __init__(self, credentials=None, email=None, first_name=None):
        self.__credentials = credentials
        self.__email = email
        self.__first_name = first_name

    def set_credentials(self, credentials):
        self.__credentials = credentials

    def set_email(self, email):
        self.__email = email

    def set_first_name(self, first_name):
        self.__first_name = first_name

    def get_credentials(self):
        return self.__credentials

    def get_email(self):
        return self.__email

    def get_first_name(self):
        return self.__first_name
