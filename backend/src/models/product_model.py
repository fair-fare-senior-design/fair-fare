class ProductModel():
    '''
    id                  (bytes)
    company_id          (bytes)
    barcode             (int)
    name                (str)
    picture             (str)
    environment_rating  (int)
    ethical_rating      (int)
    health_rating       (int)
    flag                (bool)
    flag_description    (str)
    '''
    def __init__(
        self, id, company_id, barcode, name, picture, environment_rating,
        ethical_rating, health_rating, flag, flag_description
    ):
        self.__id = id
        self.__company_id = company_id
        self.__barcode = barcode
        self.__name = name
        self.__picture = picture
        self.__environment_rating = int(environment_rating)
        self.__ethical_rating = int(ethical_rating)
        self.__health_rating = int(health_rating)
        self.__flag = int(flag)
        self.__flag_description = flag_description

    def set_id(self, id):
        self.__id = id

    def set_company_id(self, company_id):
        self.__company_id = company_id

    def set_barcode(self, barcode):
        self.__barcode = barcode

    def set_name(self, name):
        self.__name = name

    def set_picture(self, picture):
        self.__picture = picture

    def set_environment_rating(self, rating):
        self.__environment_rating = rating

    def set_ethical_rating(self, rating):
        self.__ethical_rating = rating

    def set_health_rating(self, rating):
        self.__health_rating = rating

    def set_flag(self, flag):
        self.__flag = flag

    def set_flag_description(self, flag_description):
        self.__flag_description = flag_description   

    def get_id(self):
        return self.__id

    def get_company_id(self):
        return self.__company_id

    def get_barcode(self):
        return self.__barcode

    def get_name(self):
        return self.__name

    def get_picture(self):
        return self.__picture

    def get_environment_rating(self):
        return self.__environment_rating

    def get_ethical_rating(self):
        return self.__ethical_rating

    def get_health_rating(self):
        return self.__health_rating

    def get_flag(self):
        return self.__flag

    def get_flag_description(self):
        return self.__flag_description

    def get_dict(self):
        return {
            'id': self.__id,
            'company_id': self.__company_id,
            'barcode': self.__barcode,
            'name': self.__name,
            'picture': self.__picture,
            'environment_rating': self.__environment_rating,
            'ethical_rating': self.__ethical_rating,
            'health_rating': self.__health_rating,
            'flag': self.__flag,
            'flag_description': self.__flag_description
        }
