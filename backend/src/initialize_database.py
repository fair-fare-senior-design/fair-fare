import os

from services.mysql_client import MySQLClient
from services.importer import Importer

from db.compile_company_data import get_companies

from models.credential_model import CredentialModel
from models.new_user_model import NewUserModel
from models.product_model import ProductModel


if __name__ == '__main__':
    '''
    1. Add Admin user to database (given environment variables).
    2. Add each company to database (save IDs + names).
    3. For each company, add its associated products.
    '''
    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    try:
        importer = Importer(client)

        initial_users = [
            {
                'username': os.environ.get('ADMIN_USER'),
                'password': os.environ.get('ADMIN_PASS'),
                'email': 'admin@something.com',
                'bio': 'this is my bio'
            },
            {
                'username': 'john',
                'password': 'password',
                'email': 'john@something.com',
                'bio': 'this is my bio'
            },
            {
                'username': 'kellen',
                'password': 'password',
                'email': 'kellen@something.com',
                'bio': 'looking to become more aware of the environment impact of the products i consume'
            },
            {
                'username': 'jane',
                'password': 'password',
                'email': 'jane@something.com',
                'bio': 'this is my bio'
            }
        ]

        for user in initial_users:
            credentials = CredentialModel(user['username'], user['password'])

            new_user = NewUserModel(credentials, user['email'], '')

            importer.import_user(new_user)

            importer.update_user(user['username'], '', user['bio'])

        data = get_companies()

        for company in data:
            company_model = importer.import_company(company['name'])

            for product in company['products']:
                company_id = company_model.get_id()
                name = product['name']
                barcode = product['barcode']
                picture = product['picture']
                environment_rating = product['environmentRating']
                ethical_rating = product['ethicalRating']
                health_rating = product['healthRating']
                flag = product['flag']
                flag_description = product['flagDescription']

                product_model = ProductModel('',
                                             company_id,
                                             barcode,
                                             name,
                                             picture,
                                             environment_rating,
                                             ethical_rating,
                                             health_rating,
                                             flag,
                                             flag_description)

                importer.import_product(product_model)

    finally:
        client.close()
