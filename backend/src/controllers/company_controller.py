import os

from flask import Blueprint
from flask import request
from flask import jsonify

from services.mysql_client import MySQLClient
from services.company import Company

from exceptions.company_exceptions import CompanyExistsException
from exceptions.company_exceptions import CompanyNotFoundException
from exceptions.company_exceptions import ProductsInCompanyException


company_controller = Blueprint('companies', __name__)


@company_controller.route('/add-company', methods=['POST'])
def add_company():
    '''
    Assume we will receive data like the following through a POST request:
    { 'name': <> }
    '''
    data = request.get_json()

    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    try:
        Company.add_company(client, data['name'])

        return jsonify('Company successfully added!'), 200

    except CompanyExistsException:
        return jsonify('Company already exists.'), 500

    finally:
        client.close()


@company_controller.route('/get-companies', methods=['GET'])
def get_companies():
    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    try:
        companies = Company.get_companies(client)

        return jsonify(companies), 200

    finally:
        client.close()


@company_controller.route('/remove-company', methods=['POST'])
def remove_company():
    '''
    Assume we will receive data like the following through a POST request:
    { 'name': <> }
    '''
    data = request.get_json()

    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    try:
        Company.remove_company(client, data['name'])

        return jsonify('Company successfully removed!'), 200

    except CompanyNotFoundException:
        return jsonify('Company doesn\'t exist.'), 500

    except ProductsInCompanyException:
        return jsonify('This company contains at least one product. All products must be removed before the company can be.'), 500      # noqa

    finally:
        client.close()
