import os

from flask import Blueprint
from flask import request
from flask import jsonify

from services.mysql_client import MySQLClient
from models.example_model import ExampleModel
from db.dao.example_dao import ExampleDao


example_controller = Blueprint('example-path', __name__)


@example_controller.route('/insert-data', methods=['POST'])
def insert_data():
    '''
    Assume we will receive data like the following through a POST request:
    {
        'something': <str>,
        'somethingElse': <str>
    }
    '''
    data = request.get_json()

    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    example_model = ExampleModel(data['something'], data['somethingElse'])
    example_dao = ExampleDao(client)

    try:
        example_dao.insert_data(example_model)
        return jsonify('Success!')

    finally:
        client.close()


@example_controller.route('/get-data', methods=['GET'])
def get_data():
    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    example_dao = ExampleDao(client)

    try:
        result = example_dao.get_data()
        return jsonify(result)

    finally:
        client.close()
