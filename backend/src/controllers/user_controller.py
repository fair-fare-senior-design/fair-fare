import os
import urllib

from flask import Blueprint
from flask import request
from flask import jsonify

from services.mysql_client import MySQLClient
from services.boto_client import BotoClient
from services.user import User
from services.auth import Auth
from services.following import Following

from utilities.picture_utilities import PictureUtilities
from utilities.uuid_utilities import UuidUtilities

from exceptions.user_exceptions import UnverifiedTokenException
from exceptions.user_exceptions import DeletePhotoException
from exceptions.user_exceptions import UploadPhotoException


user_controller = Blueprint('users', __name__)


@user_controller.route('/get-user', methods=['POST'])
def get_user():
    '''
    Assume we will receive data like the following through a POST request:
    {
        'username': <string>
    }
    '''
    token = request.headers['Authorization'].split(' ')[1]
    data = request.get_json()

    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    try:
        Auth.verify_token(token)

        username = data['username']
        user = User.get_user(client, username)

        user_id = UuidUtilities.unformat_uuid(user.get_id())
        user.set_id(user_id)

        followers = Following.get_followers(client, user)
        followees = Following.get_followees(client, user)

        encoded_picture = urllib.parse.quote_plus(user.get_profile_picture())
        picture_url = os.environ.get('AWS_S3_ADDRESS') + '/' + encoded_picture

        user_dict = {
            'userId': UuidUtilities.format_uuid(user.get_id()),
            'username': user.get_username(),
            'firstName': user.get_first_name(),
            'bio': user.get_bio(),
            'email': user.get_email(),
            'dateJoined': user.get_date_joined(),
            'pictureUrl': picture_url,
            'followerCount': len(followers),
            'followeeCount': len(followees),
            'followers': followers,
            'followees': followees
        }

        return jsonify(user_dict), 200

    except UnverifiedTokenException:
        return jsonify('Token could not be verified.'), 401

    finally:
        client.close()


@user_controller.route('/update-account', methods=['POST'])
def update_account():
    '''
    Assume we will receive data like the following through a POST request:
    - FormData (key-value pairs):
      * {'base64': <bytes>}
      * {'type': <string>} [could also be empty string if not uploading pic]
      * {'firstName': <string>}
      * {'bio': <string>}
    '''
    token = request.headers['Authorization'].split(' ')[1]
    photo_data = request.form['base64']
    photo_type = request.form['type']
    first_name = request.form['firstName']
    bio = request.form['bio']

    mysql_client = MySQLClient(os.environ.get('MYSQL_HOST'),
                               os.environ.get('MYSQL_DB'),
                               os.environ.get('MYSQL_USER'),
                               os.environ.get('MYSQL_PASS'))

    boto_client = BotoClient(os.environ.get('AWS_KEY_ID'),
                             os.environ.get('AWS_SECRET_KEY'),
                             os.environ.get('AWS_ARN_ADDRESS'))

    try:
        username = Auth.verify_token(token)

        User.update_user(mysql_client, username, first_name, bio)

        allowed = PictureUtilities.file_type_allowed(photo_type)

        if allowed:
            user = User.get_user(mysql_client, username)

            try:
                file_name = User.upload_photo_to_s3(boto_client,
                                                    photo_data,
                                                    photo_type,
                                                    user)

                User.save_photo_url_to_db(mysql_client, username, file_name)

                return jsonify('Updated user successfully!'), 200

            except DeletePhotoException:
                return jsonify('Photo could not be deleted from S3.'), 500

            except UploadPhotoException:
                return jsonify('Photo could not be uploaded to S3.'), 500

        else:
            return jsonify('Updated user successfully!'), 200

    except UnverifiedTokenException:
        return jsonify('Token could not be verified.'), 401

    finally:
        mysql_client.close()


@user_controller.route('/delete-account', methods=['POST'])
def delete_account():
    '''
    Assume we will receive data like the following through a POST request:
    {}
    '''
    token = request.headers['Authorization'].split(' ')[1]

    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    try:
        username = Auth.verify_token(token)

        User.delete_user(client, username)

        return jsonify('Deleted user successfully.'), 200

    except UnverifiedTokenException:
        return jsonify('Token could not be verified'), 401

    finally:
        client.close()
