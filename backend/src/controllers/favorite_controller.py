import os

from flask import Blueprint
from flask import request
from flask import jsonify

from services.mysql_client import MySQLClient
from services.favorite import Favorite
from services.auth import Auth
from services.user import User

from utilities.uuid_utilities import UuidUtilities

from exceptions.user_exceptions import UnverifiedTokenException


favorite_controller = Blueprint('favorites', __name__)


@favorite_controller.route('/add-favorite', methods=['POST'])
def add_product():
    '''
    Assume we will receive data like the following through a POST request:
    {
        'productId': <string>
    }
    '''
    token = request.headers['Authorization'].split(' ')[1]
    data = request.get_json()

    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    try:
        product_id = data['productId']

        product_uuid = UuidUtilities.unformat_uuid(product_id)

        username = Auth.verify_token(token)

        user = User.get_user(client, username)
        user_id = UuidUtilities.unformat_uuid(user.get_id())

        Favorite.add_favorite(client, user_id, product_uuid)

        return jsonify('Favorite successfully added!'), 200

    except KeyError:
        return jsonify('You must include a product id in your request.'), 500

    except UnverifiedTokenException:
        return jsonify('Token could not be verified.'), 401

    finally:
        client.close()


@favorite_controller.route('/get-favorites', methods=['POST'])
def get_favorites():
    '''
    Assume we will receive data like the following through a POST request:
    {
        'username': <string>
    }
    '''
    token = request.headers['Authorization'].split(' ')[1]
    data = request.get_json()

    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    try:
        username = data['username']

        Auth.verify_token(token)

        user = User.get_user(client, username)
        user_id = UuidUtilities.unformat_uuid(user.get_id())

        products = Favorite.get_favorites_for_user(client, user_id)

        return jsonify(products), 200

    except UnverifiedTokenException:
        return jsonify('Token could not be verified.'), 401

    finally:
        client.close()


@favorite_controller.route('/check-favorite', methods=['POST'])
def check_favorite():
    '''
    Assume we will receive data like the following through a POST request:
    {
        'productId': <string>
    }
    '''
    token = request.headers['Authorization'].split(' ')[1]
    data = request.get_json()

    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    try:
        product_id = data['productId']

        username = Auth.verify_token(token)

        user = User.get_user(client, username)
        user_id = UuidUtilities.unformat_uuid(user.get_id())

        products = Favorite.get_favorites_for_user(client, user_id)

        is_favorite = False

        for product in products:
            if product['id'] == product_id:
                is_favorite = True
                break

        return jsonify({'isFavorite': is_favorite}), 200

    except UnverifiedTokenException:
        return jsonify('Token could not be verified.'), 401

    finally:
        client.close()


@favorite_controller.route('/remove-favorite', methods=['POST'])
def remove_favorite():
    '''
    Assume we will receive data like the following through a POST request:
    {
        'productId': <string>
    }
    '''
    token = request.headers['Authorization'].split(' ')[1]
    data = request.get_json()

    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    try:
        product_id = data['productId']

        product_uuid = UuidUtilities.unformat_uuid(product_id)

        username = Auth.verify_token(token)

        user = User.get_user(client, username)
        user_id = UuidUtilities.unformat_uuid(user.get_id())

        Favorite.remove_favorite(client, user_id, product_uuid)

        return jsonify('Favorite successfully removed!'), 200

    except KeyError:
        return jsonify('You must include a product id in your request.'), 500

    except UnverifiedTokenException:
        return jsonify('Token could not be verified.'), 401

    finally:
        client.close()
