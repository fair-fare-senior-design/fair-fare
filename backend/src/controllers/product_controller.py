import os
import urllib

from flask import Blueprint
from flask import request
from flask import jsonify

from services.mysql_client import MySQLClient
from services.product import Product

from models.product_model import ProductModel

from exceptions.product_exceptions import CompanyNotFoundException
from exceptions.product_exceptions import ProductExistsException
from exceptions.product_exceptions import ProductNotFoundException


product_controller = Blueprint('products', __name__)


@product_controller.route('/add-product', methods=['POST'])
def add_product():
    '''
    Assume we will receive data like the following through a POST request:
    {
        'name': <string>,
        'companyId': <string>,
        'barcode': <12-digit string>,
        'pictureUrl': <string>,
        'environmentRating': <digit string>,
        'ethicalRating': <digit string>,
        'healthRating': <digit string>,
        'flag': <boolean>,
        'flagDescription': <string>
    }
    '''
    data = request.get_json()

    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    try:
        company_id = data['companyId']      # noqa

    except KeyError:
        return jsonify('You must include a company id in your request.'), 500

    else:
        product = ProductModel('',
                               data['companyId'],
                               int(data['barcode']),
                               data['name'],
                               data['pictureUrl'],
                               data['environmentRating'],
                               data['ethicalRating'],
                               data['healthRating'],
                               data['flag'],
                               data['flagDescription'])

        try:
            Product.add_product(client, product)

            return jsonify('Product was successfully added!'), 200

        except CompanyNotFoundException:
            return jsonify('Company could not be found.'), 500

        except ProductExistsException:
            return jsonify('Product already exists.'), 500

    finally:
        client.close()


@product_controller.route('/get-product-by-barcode', methods=['POST'])
def get_product():
    '''
    Assume we will receive data like the following through a POST request:
    {
        'barcode': <string>
    }
    '''
    data = request.get_json()

    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    try:
        response = Product.get_product(client, data['barcode'])

        picture = urllib.parse.quote_plus(response['product'].get_picture())
        picture_url = os.environ.get('AWS_S3_ADDRESS') + '/products/' + picture

        product_dict = {
            'productId': response['product'].get_id(),
            'productName': response['product'].get_name(),
            'companyName': response['company'].get_name(),
            'barcode': response['product'].get_barcode(),
            'pictureUrl': picture_url,
            'environmentRating': response['product'].get_environment_rating(),
            'ethicalRating': response['product'].get_ethical_rating(),
            'healthRating': response['product'].get_health_rating(),
            'flag': response['product'].get_flag(),
            'flagDescription': response['product'].get_flag_description()
        }

        return jsonify(product_dict), 200

    except ProductNotFoundException:
        return jsonify('Product couldn\'t be found in the database.'), 500

    finally:
        client.close()


@product_controller.route('/remove-product', methods=['POST'])
def remove_product():
    '''
    Assume we will receive data like the following through a POST request:
    {
        'id': <string>
    }
    '''
    data = request.get_json()

    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    try:
        Product.remove_product(client, data['id'])

        return jsonify('Product was successfully removed.'), 200

    finally:
        client.close()
