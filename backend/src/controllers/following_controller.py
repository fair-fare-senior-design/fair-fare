import os

from flask import Blueprint
from flask import request
from flask import jsonify

from services.mysql_client import MySQLClient
from services.following import Following
from services.auth import Auth
from services.user import User

from utilities.uuid_utilities import UuidUtilities

from exceptions.user_exceptions import UnverifiedTokenException


following_controller = Blueprint('following', __name__)


@following_controller.route('/follow', methods=['POST'])
def follow():
    '''
    Assume we will receive data like the following through a POST request:
    {
        'username': <string>
    }
    '''
    token = request.headers['Authorization'].split(' ')[1]
    data = request.get_json()

    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    try:
        followee_username = data['username']
        followee = User.get_user(client, followee_username)

        follower_username = Auth.verify_token(token)
        follower = User.get_user(client, follower_username)

        Following.follow(client, followee, follower)

        return jsonify('Favorite successfully added!'), 200

    except KeyError:
        return jsonify('You must include a product id in your request.'), 500

    except UnverifiedTokenException:
        return jsonify('Token could not be verified.'), 401

    finally:
        client.close()


@following_controller.route('/unfollow', methods=['POST'])
def unfollow():
    '''
    Assume we will receive data like the following through a POST request:
    {
        'username': <string>
    }
    '''
    token = request.headers['Authorization'].split(' ')[1]
    data = request.get_json()

    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    try:
        followee_username = data['username']
        followee = User.get_user(client, followee_username)

        username = Auth.verify_token(token)
        user = User.get_user(client, username)

        Following.unfollow(client, followee, user)

        return jsonify('User successfully unfollowed!'), 200

    except KeyError:
        return jsonify('You must include a username in your request.'), 500

    except UnverifiedTokenException:
        return jsonify('Token could not be verified.'), 401

    finally:
        client.close()


@following_controller.route('/check-following', methods=['POST'])
def check_following():
    '''
    Assume we will receive data like the following through a POST request:
    {
        'username': <string>
    }
    '''
    token = request.headers['Authorization'].split(' ')[1]
    data = request.get_json()

    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    try:
        followee_username = data['username']
        followee = User.get_user(client, followee_username)
        followee.set_id(UuidUtilities.unformat_uuid(followee.get_id()))

        username = Auth.verify_token(token)
        user = User.get_user(client, username)
        user.set_id(UuidUtilities.unformat_uuid(user.get_id()))

        is_following = Following.check_following(client, followee, user)

        return jsonify({'isFollowing': is_following}), 200

    except UnverifiedTokenException:
        return jsonify('Token could not be verified.'), 401

    finally:
        client.close()
