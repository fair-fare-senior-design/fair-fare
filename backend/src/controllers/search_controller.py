import os

from flask import Blueprint
from flask import request
from flask import jsonify

from services.mysql_client import MySQLClient
from services.auth import Auth
from services.search import Search

from exceptions.user_exceptions import UnverifiedTokenException
from exceptions.search_exceptions import InvalidSearchCategoryException


search_controller = Blueprint('search', __name__)


@search_controller.route('/search-category', methods=['POST'])
def search_category():
    '''
    Assume we will receive data like the following through a POST request:
    {
        'category': 'company' | 'product' | 'person',
        'searchTerm': <string>
    }
    '''
    token = request.headers['Authorization'].split(' ')[1]
    data = request.get_json()

    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    try:
        category = data['category']
        search_term = '%' + data['searchTerm'] + '%'

        Auth.verify_token(token)

        result = Search.search(client, category, search_term)

        return jsonify(result), 200

    except KeyError:
        return jsonify(
            'You must include a category and search term in your request.'
        ), 500

    except InvalidSearchCategoryException:
        return jsonify('Invalid category. Must be company/product/person.')

    except UnverifiedTokenException:
        return jsonify('Token could not be verified.'), 401

    finally:
        client.close()
