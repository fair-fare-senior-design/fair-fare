import os

from flask import Blueprint
from flask import request
from flask import jsonify

from services.mysql_client import MySQLClient
from services.auth import Auth

from models.credential_model import CredentialModel
from models.new_user_model import NewUserModel

from exceptions.user_exceptions import EmailExistsException
from exceptions.user_exceptions import UsernameExistsException
from exceptions.user_exceptions import IncorrectCredentialsException


auth_controller = Blueprint('auth', __name__)


@auth_controller.route('/sign-up', methods=['POST'])
def sign_up():
    '''
    Assume we will receive data like the following through a POST request:
    {
        'username': <str>,
        'password': <str>,
        'email': <str>
    }
    '''
    data = request.get_json()

    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    credentials = CredentialModel(data['username'], data['password'])
    new_user = NewUserModel(credentials, data['email'], '')

    try:
        try:
            Auth.sign_up(client, new_user)
            return jsonify('User successfully signed up!'), 200

        except EmailExistsException:
            return jsonify('Email already in use.'), 500

        except UsernameExistsException:
            return jsonify('Username already in use.'), 500

    finally:
        client.close()


@auth_controller.route('/log-in', methods=['POST'])
def log_in():
    '''
    Assume we will receive data like the following through a POST request:
    {
        'username': <str>,
        'password': <str>
    }
    '''
    data = request.get_json()

    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    credential_model = CredentialModel(data['username'], data['password'])

    try:
        try:
            response = Auth.log_in(client, credential_model)
            return jsonify(response), 200

        except IncorrectCredentialsException:
            return jsonify('Credentials do not exist.'), 500

    finally:
        client.close()
