import os

from services.mysql_client import MySQLClient


if __name__ == '__main__':
    '''
    1. Drop all database tables from fair-fare.
    '''
    client = MySQLClient(os.environ.get('MYSQL_HOST'),
                         os.environ.get('MYSQL_DB'),
                         os.environ.get('MYSQL_USER'),
                         os.environ.get('MYSQL_PASS'))

    try:
        client.execute('DELETE FROM AppFavorite', ())
        client.execute('DELETE FROM AppFollowing', ())
        client.execute('DELETE FROM AppProduct', ())
        client.execute('DELETE FROM AppCompany', ())
        client.execute('DELETE FROM AppUser', ())

    finally:
        client.close()
