import bcrypt


class AuthUtilities():

    @staticmethod
    def generate_salt():
        return bcrypt.gensalt()

    @staticmethod
    def hash_password(password, salt):
        return bcrypt.hashpw(password.encode('utf-8'), salt)
