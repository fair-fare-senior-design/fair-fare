import uuid
import base64


class UuidUtilities():

    @staticmethod
    def generate_uuid():
        return uuid.uuid1().bytes

    @staticmethod
    def format_uuid(uuid):
        encoded = base64.b64encode(uuid)
        return encoded.decode('utf-8')

    @staticmethod
    def unformat_uuid(uuid):
        encoded = uuid.encode('utf-8')
        return base64.b64decode(encoded)
