import random
import string
import base64


class PictureUtilities():

    @staticmethod
    def file_type_allowed(type):
        allowed = ['image/png', 'image/jpg', 'image/jpeg']

        if type in allowed:
            return True
        else:
            return False

    @staticmethod
    def generate_random_file_name(uuid, type):
        random_chars = ''.join(
            random.choices(string.ascii_letters + string.digits, k=6)
        )

        extension = type.split('/')[-1]

        return uuid + '_' + random_chars + '.' + extension

    @staticmethod
    def encode_picture(picture):
        return base64.b64encode(picture)
