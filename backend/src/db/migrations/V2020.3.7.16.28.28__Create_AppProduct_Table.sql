CREATE TABLE AppProduct (
  AppProductId               VARBINARY(16)              NOT NULL,
  CompanyId                  VARBINARY(16)              NOT NULL,
  Barcode                    VARCHAR(15)                NOT NULL,
  Name                       VARCHAR(256)               NOT NULL,
  Picture                    VARCHAR(256)               NOT NULL,
  EnvironmentRating          VARCHAR(1)                 NOT NULL,
  EthicalRating              VARCHAR(1)                 NOT NULL,
  HealthRating               VARCHAR(1)                 NOT NULL,

  PRIMARY KEY (AppProductId),
  FOREIGN KEY (CompanyId) REFERENCES AppCompany (AppCompanyId)
);
