CREATE TABLE AppCompany (
  AppCompanyId               VARBINARY(16)              NOT NULL,
  Name                       VARCHAR(256)               NOT NULL,

  PRIMARY KEY (AppCompanyId),
  CONSTRAINT UniqueName UNIQUE (Name)
);
