CREATE TABLE AppFavorite (
  AppFavoriteId              VARBINARY(16)              NOT NULL,
  UserId                     VARBINARY(16)              NOT NULL,
  ProductId                  VARBINARY(16)              NOT NULL,

  PRIMARY KEY (AppFavoriteId),
  FOREIGN KEY (UserId) REFERENCES AppUser (AppUserId),
  FOREIGN KEY (ProductId) REFERENCES AppProduct (AppProductId)
);
