CREATE TABLE AppFollowing (
  AppFollowingId             VARBINARY(16)              NOT NULL,
  FolloweeId                 VARBINARY(16)              NOT NULL,
  FollowerId                 VARBINARY(16)              NOT NULL,

  PRIMARY KEY (AppFollowingId),
  FOREIGN KEY (FolloweeId) REFERENCES AppUser (AppUserId),
  FOREIGN KEY (FollowerId) REFERENCES AppUser (AppUserId)
);
