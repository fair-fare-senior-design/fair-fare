CREATE TABLE AppUser (
  AppUserId               VARBINARY(16)              NOT NULL,
  Email                   VARCHAR(256)               NOT NULL,
  FirstName               VARCHAR(64)                NOT NULL,
  UserName                VARCHAR(16)                NOT NULL,
  Bio                     VARCHAR(256)               NOT NULL,
  DateJoined              DATETIME                   NOT NULL,
  ProfilePicture          VARCHAR(256)               NOT NULL,
  HashedPassword          CHAR(64)                   NOT NULL,
  Salt                    CHAR(32)                   NOT NULL,

  PRIMARY KEY (AppUserId),
  CONSTRAINT UniqueEmail UNIQUE (Email),
  CONSTRAINT UniqueUserName UNIQUE (UserName)
);
