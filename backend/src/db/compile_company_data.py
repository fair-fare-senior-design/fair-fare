import os

from pandas import read_csv


def get_companies():
    companies = []

    # Preserve leading zeros from barcodes
    dataframe = read_csv(os.path.join('db', 'products.csv'),
                         converters={'Barcode': lambda x: str(x)})

    for row in dataframe.values:
        company = {
            'name': '',
            'products': []
        }

        product = {
            'name': row[1],
            'barcode': row[0],
            'picture': row[3],
            'environmentRating': row[4],
            'ethicalRating': row[5],
            'healthRating': row[6],
            'flag': row[7],
            'flagDescription': row[8]
        }

        company_exists = False

        for company in companies:
            if company['name'] == row[2]:
                company_exists = True

        if not company_exists:
            companies.append({
                'name': row[2],
                'products': []
            })

            for company in companies:
                if company['name'] == row[2]:
                    company['products'].append(product)

        else:
            for company in companies:
                if company['name'] == row[2]:
                    product = {
                        'name': row[1],
                        'barcode': row[0],
                        'picture': row[3],
                        'environmentRating': str(row[4]),
                        'ethicalRating': str(row[5]),
                        'healthRating': str(row[6]),
                        'flag': str(row[7]),
                        'flagDescription': str(row[8])
                    }

                    company['products'].append(product)

    return companies
