class ExampleDao():

    def __init__(self, mysql_client):
        self.__mysql_client = mysql_client

    def insert_data(self, model):
        '''
        Parameter(s):
        - model (ExampleModel): contains data to be inserted into database
                                table
        '''
        query = 'INSERT INTO `ExampleTable` (`ColumnOne`, `ColumnTwo`) \
                 VALUES (%s, %s)'

        parameters = (model.get_member_one(), model.get_member_two())

        self.__mysql_client.execute(query, parameters)

    def get_data(self):
        query = 'SELECT * FROM `ExampleTable`'

        return self.__mysql_client.execute_query(query, ())
