from models.following_model import FollowingModel


class FollowingDao():

    def __init__(self, mysql_client):
        self.__mysql_client = mysql_client

    def follow(self, following):
        '''
        Parameter(s):
        - following (FollowingModel)
        '''
        query = 'INSERT INTO `AppFollowing` \
                    (`AppFollowingId`, \
                     `FolloweeId`, \
                     `FollowerId`) \
                 VALUES (%s, %s, %s)'

        parameters = (
            following.get_id(),
            following.get_followee_id(),
            following.get_follower_id(),
        )

        self.__mysql_client.execute(query, parameters)

    def unfollow(self, following):
        '''
        Parameter(s):
        - following (FollowingModel)
        '''
        query = 'DELETE FROM `AppFollowing` \
                 WHERE `AppFollowingId` = %s'

        self.__mysql_client.execute(query, (following.get_id()))

    def get_followers(self, user):
        query = 'SELECT * from `AppFollowing` \
                 WHERE `FolloweeId` = %s'

        result = self.__mysql_client.execute_query(query, (user.get_id()))

        followers = []

        for row in result:
            following = FollowingModel(row[0], row[1], row[2])
            followers.append(following)

        return followers

    def get_followees(self, user):
        query = 'SELECT * from `AppFollowing` \
                 WHERE `FollowerId` = %s'

        result = self.__mysql_client.execute_query(query, (user.get_id()))

        followees = []

        for row in result:
            following = FollowingModel(row[0], row[1], row[2])
            followees.append(following)

        return followees

    def get_following(self, following):
        query = 'SELECT * from `AppFollowing` \
                 WHERE `FolloweeId` = %s \
                 AND `FollowerId` = %s'

        parameters = (
            following.get_followee_id(),
            following.get_follower_id()
        )

        result = self.__mysql_client.execute_query(query, parameters)[0]

        return FollowingModel(result[0], result[1], result[2])

    def check_following(self, following):
        query = 'SELECT * from `AppFollowing` \
                 WHERE `FolloweeId` = %s \
                 AND `FollowerId` = %s'

        parameters = (
            following.get_followee_id(),
            following.get_follower_id()
        )

        result = self.__mysql_client.execute_query(query, parameters)

        return len(result) != 0
