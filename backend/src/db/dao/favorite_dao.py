from models.favorite_model import FavoriteModel


class FavoriteDao():

    def __init__(self, mysql_client):
        self.__mysql_client = mysql_client

    def add_favorite(self, favorite):
        '''
        Parameter(s):
        - favorite (FavoriteModel)
        '''
        query = 'INSERT INTO `AppFavorite` \
                    (`AppFavoriteId`, `UserId`, `ProductId`) \
                 VALUES (%s, %s, %s)'

        parameters = (
            favorite.get_id(),
            favorite.get_user_id(),
            favorite.get_product_id(),
        )

        self.__mysql_client.execute(query, parameters)

    def get_favorite(self, favorite):
        query = 'SELECT `AppFavoriteId`, `UserId`, `ProductId` \
                 FROM `AppFavorite` \
                 WHERE `UserId` = %s \
                 AND `ProductId` = %s'

        parameters = (
            favorite.get_user_id(),
            favorite.get_product_id()
        )

        result = self.__mysql_client.execute_query(query, parameters)

        for row in result:
            favorite_id = row[0]
            user_id = row[1]
            product_id = row[2]

            return FavoriteModel(favorite_id, user_id, product_id)

    def get_favorites_for_user(self, user):
        query = 'SELECT `ProductId` from `AppFavorite` \
                 WHERE `UserId` = %s'

        result = self.__mysql_client.execute_query(query, (user.get_id()))

        return [row[0] for row in result]

    def remove_favorite(self, favorite):
        '''
        Parameter(s):
        - favorite (FavoriteModel)
        '''
        query = 'DELETE FROM `AppFavorite` \
                 WHERE `AppFavoriteId` = %s'

        self.__mysql_client.execute(query, (favorite.get_id()))
