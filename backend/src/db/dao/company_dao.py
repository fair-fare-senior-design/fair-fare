from utilities.uuid_utilities import UuidUtilities

from models.company_model import CompanyModel
from models.product_model import ProductModel


class CompanyDao():

    def __init__(self, mysql_client):
        self.__mysql_client = mysql_client

    def add_company(self, company):
        '''
        Parameter(s):
        - company (CompanyModel)
        '''
        query = 'INSERT INTO `AppCompany` \
                    (`AppCompanyId`, `Name`) \
                 VALUES (%s, %s)'

        parameters = (
            company.get_id(),
            company.get_name()
        )

        self.__mysql_client.execute(query, parameters)

    def name_exists(self, name):
        query = 'SELECT * FROM `AppCompany` \
                 WHERE `Name` = %s'

        result = self.__mysql_client.execute_query(query, (name))

        return len(result) != 0

    def id_exists(self, id):
        query = 'SELECT * FROM `AppCompany` \
                 WHERE `AppCompanyId` = %s'

        result = self.__mysql_client.execute_query(query, (id))

        return len(result) != 0

    def get_companies(self):
        query = 'SELECT * from `AppCompany`'

        result = self.__mysql_client.execute_query(query, ())

        companies = []

        for row in result:
            formatted = UuidUtilities.format_uuid(row[0])

            companies.append({
                'id': formatted,
                'name': row[1]
            })

        return companies

    def products_in_company(self, company):
        query = 'SELECT * from `AppProduct` \
                 WHERE `CompanyId` = %s'

        result = self.__mysql_client.execute_query(query, (company.get_id()))

        return len(result) != 0

    def get_company(self, name):
        query = 'SELECT * from `AppCompany` \
                 WHERE `Name` = %s'

        result = self.__mysql_client.execute_query(query, (name))

        for row in result:
            formatted = UuidUtilities.format_uuid(row[0])
            name = row[1]

            return CompanyModel(formatted, name)

    def get_company_by_id(self, id):
        query = 'SELECT * from `AppCompany` \
                 WHERE `AppCompanyId` = %s'

        result = self.__mysql_client.execute_query(query, (id))

        for row in result:
            return CompanyModel(row[0], row[1])

    def get_products_in_company(self, company):
        query = 'SELECT * from `AppProduct` \
                 WHERE `CompanyId` = %s'

        result = self.__mysql_client.execute_query(query, (company.get_id()))

        products = []

        for row in result:
            product_id = UuidUtilities.format_uuid(row[0])
            company_id = UuidUtilities.format_uuid(row[1])
            barcode = row[2]
            name = row[3]
            picture = row[4]
            environment_rating = row[5]
            ethical_rating = row[6]
            health_rating = row[7]
            flag = row[8]
            flag_description = row[9]

            products.append(
                ProductModel(product_id, company_id, barcode, name, picture,
                             environment_rating, ethical_rating, health_rating,
                             flag, flag_description)
            )

        return products

    def search_for_company(self, search_term):
        query = 'SELECT * from `AppCompany` \
                 WHERE `Name` LIKE %s'

        result = self.__mysql_client.execute_query(query, (search_term))

        for row in result:
            formatted = UuidUtilities.format_uuid(row[0])
            name = row[1]

            return CompanyModel(formatted, name)

    def remove_company(self, company):
        '''
        Parameter(s):
        - company (CompanyModel)
        '''
        query = 'DELETE FROM `AppCompany` \
                 WHERE `AppCompanyId` = %s'

        self.__mysql_client.execute(query, (company.get_id()))
