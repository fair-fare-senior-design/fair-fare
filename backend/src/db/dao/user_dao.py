from models.user_model import UserModel


class UserDao():

    def __init__(self, mysql_client):
        self.__mysql_client = mysql_client

    def add_user(self, user_model):
        '''
        Parameter(s):
        - user_model (UserModel)
        '''
        query = 'INSERT INTO `AppUser` \
                    (`AppUserId`, \
                     `Email`, \
                     `FirstName`, \
                     `UserName`, \
                     `Bio`, \
                     `DateJoined`, \
                     `ProfilePicture`, \
                     `HashedPassword`, \
                     `Salt`) \
                 VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)'

        parameters = (
            user_model.get_id(),
            user_model.get_email(),
            user_model.get_first_name(),
            user_model.get_username(),
            user_model.get_bio(),
            user_model.get_date_joined(),
            user_model.get_profile_picture(),
            user_model.get_hashed_password(),
            user_model.get_salt()
        )

        self.__mysql_client.execute(query, parameters)

    def email_exists(self, email):
        query = 'SELECT * FROM `AppUser` \
                 WHERE `Email` = %s'

        result = self.__mysql_client.execute_query(query, (email))

        return len(result) != 0

    def username_exists(self, username):
        query = 'SELECT * FROM `AppUser` \
                 WHERE `UserName` = %s'

        result = self.__mysql_client.execute_query(query, (username))

        return len(result) != 0

    def get_user(self, user):
        query = 'SELECT `AppUserId`, `FirstName`, `Email`, `DateJoined`, `Bio`,\
                    `ProfilePicture`\
                 FROM `AppUser` \
                 WHERE `UserName` = %s'

        parameters = (user.get_username())

        result = self.__mysql_client.execute_query(query, (parameters))[0]

        return UserModel(
            result[0], result[2], result[1], user.get_username(), result[4],
            result[3], result[5], None, None
        )

    def get_user_by_id(self, user):
        query = 'SELECT `AppUserId`, `FirstName`, `Email`, `UserName`, \
                    `DateJoined`, `Bio`, `ProfilePicture`\
                 FROM `AppUser` \
                 WHERE `AppUserId` = %s'

        parameters = (user.get_id())

        result = self.__mysql_client.execute_query(query, (parameters))[0]

        return UserModel(
            result[0], result[2], result[1], result[3], result[5],
            result[4], result[6], None, None
        )

    def update_user(self, user):
        query = 'UPDATE `AppUser` \
            SET `FirstName` = %s, \
                `Bio` = %s \
            WHERE `UserName` = %s'

        parameters = (
            user.get_first_name(),
            user.get_bio(),
            user.get_username()
        )

        self.__mysql_client.execute(query, parameters)

    def update_user_photo(self, user, picture):
        query = 'UPDATE `AppUser` \
            SET `ProfilePicture` = %s \
            WHERE `UserName` = %s'

        parameters = (picture, user.get_username())

        self.__mysql_client.execute(query, parameters)

    def delete_user(self, user):
        query = 'DELETE FROM `AppUser` \
                 WHERE `UserName` = %s'

        self.__mysql_client.execute(query, (user.get_username()))

    def get_users(self):
        query = 'SELECT `UserName`, `Email`, `FirstName`, `ProfilePicture`\
                 FROM `AppUser`'

        return self.__mysql_client.execute_query(query, ())

    def get_hashed_password(self, username):
        query = 'SELECT `HashedPassword` FROM `AppUser` \
                 WHERE `UserName` = %s'

        return self.__mysql_client.execute_query(query, (username))[0][0]

    def search_for_user(self, search_term):
        query = 'SELECT * from `AppUser` \
                 WHERE `FirstName` LIKE %s \
                 OR `UserName` LIKE %s'

        parameters = (search_term, search_term)

        result = self.__mysql_client.execute_query(query, parameters)

        users = []

        for item in result:
            users.append(UserModel(item[0], item[1], item[2], item[3], item[4],
                                   item[5], item[6], None, None))

        return users

    def remove_user(self, user):
        query = 'DELETE FROM `AppUser` \
                 WHERE `UserName` = %s'

        self.__mysql_client.execute(query, (user.get_username()))
