from utilities.uuid_utilities import UuidUtilities

from models.product_model import ProductModel


class ProductDao():

    def __init__(self, mysql_client):
        self.__mysql_client = mysql_client

    def add_product(self, product):
        '''
        Parameter(s):
        - product (ProductModel)
        '''
        query = 'INSERT INTO `AppProduct` \
                    (`AppProductId`, `CompanyId`, `Name`, `Barcode`, \
                    `Picture`, `EnvironmentRating`, `EthicalRating`, \
                    `HealthRating`, `Flag`, `FlagDescription`) \
                 VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'

        parameters = (
            product.get_id(),
            product.get_company_id(),
            product.get_name(),
            product.get_barcode(),
            product.get_picture(),
            product.get_environment_rating(),
            product.get_ethical_rating(),
            product.get_health_rating(),
            product.get_flag(),
            product.get_flag_description()
        )

        self.__mysql_client.execute(query, parameters)

    def product_exists(self, product):
        query = 'SELECT * FROM `AppProduct` \
                 WHERE `CompanyId` = %s \
                 AND `Name` = %s'

        parameters = (
            product.get_company_id(),
            product.get_name()
        )

        result = self.__mysql_client.execute_query(query, parameters)

        return len(result) != 0

    def get_products(self):
        query = 'SELECT *  from `AppProduct`'

        result = self.__mysql_client.execute_query(query, ())

        products = []

        for row in result:
            product_id = UuidUtilities.format_uuid(row[0])
            company_id = UuidUtilities.format_uuid(row[1])
            barcode = row[2]
            name = row[3]
            picture = row[4]
            environment_rating = row[5]
            ethical_rating = row[6]
            health_rating = row[7]
            flag = row[8]
            flag_description = row[9]

            products.append(
                ProductModel(product_id, company_id, barcode, name, picture,
                             environment_rating, ethical_rating, health_rating,
                             flag, flag_description)
            )

        return products

    def get_product(self, id):
        query = 'SELECT * from `AppProduct` \
                 WHERE `AppProductId` = %s'

        result = self.__mysql_client.execute_query(query, (id))

        for row in result:
            product_id = UuidUtilities.format_uuid(row[0])
            company_id = UuidUtilities.format_uuid(row[1])
            barcode = row[2]
            name = row[3]
            picture = row[4]
            environment_rating = row[5]
            ethical_rating = row[6]
            health_rating = row[7]
            flag = row[8]
            flag_description = row[9]

            return ProductModel(product_id, company_id, barcode, name, picture,
                                environment_rating, ethical_rating,
                                health_rating, flag, flag_description)

    def get_product_by_barcode(self, barcode):
        query = 'SELECT * from `AppProduct` \
                 WHERE `Barcode` = %s'

        result = self.__mysql_client.execute_query(query, (barcode))

        if len(result) != 0:
            for row in result:
                product_id = UuidUtilities.format_uuid(row[0])
                company_id = UuidUtilities.format_uuid(row[1])
                barcode = row[2]
                name = row[3]
                picture = row[4]
                environment_rating = row[5]
                ethical_rating = row[6]
                health_rating = row[7]
                flag = row[8]
                flag_description = row[9]

                return ProductModel(product_id, company_id, barcode, name, picture,
                                    environment_rating, ethical_rating,
                                    health_rating, flag, flag_description)

        else:
            return None

    def search_for_product(self, search_term):
        query = 'SELECT * from `AppProduct` \
                 WHERE `Name` LIKE %s'

        result = self.__mysql_client.execute_query(query, (search_term))

        products = []

        for row in result:
            product_id = UuidUtilities.format_uuid(row[0])
            company_id = UuidUtilities.format_uuid(row[1])
            barcode = row[2]
            name = row[3]
            picture = row[4]
            environment_rating = row[5]
            ethical_rating = row[6]
            health_rating = row[7]
            flag = row[8]
            flag_description = row[9]


            products.append(
                ProductModel(product_id, company_id, barcode, name, picture,
                             environment_rating, ethical_rating, health_rating,
                             flag, flag_description)
            )

        return products

    def remove_product(self, product):
        query = 'DELETE FROM `AppProduct` \
                 WHERE `AppProductId` = %s'

        self.__mysql_client.execute(query, (product.get_id()))
