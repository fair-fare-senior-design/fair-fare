# Fair Fare Senior Design

## Table of Contents
* [Project Abstract](https://gitlab.com/fair-fare-senior-design/fair-fare/wikis/Project-Abstract)
* [Project Description](https://gitlab.com/fair-fare-senior-design/fair-fare/wikis/Project-Description)
* [User Stories and Design Diagrams](https://gitlab.com/fair-fare-senior-design/fair-fare/wikis/User-Stories-and-Design-Diagrams)
* [Project Tasks and Timeline](https://gitlab.com/fair-fare-senior-design/fair-fare/wikis/Project-Tasks-and-Timeline)
* [Slideshow](https://gitlab.com/fair-fare-senior-design/fair-fare/wikis/Slideshow)
* [Self-Assessment Essays](https://gitlab.com/fair-fare-senior-design/fair-fare/wikis/Self-Assessment-Essays)
* [Professional Biographies](https://gitlab.com/fair-fare-senior-design/fair-fare/wikis/Professional-Biographies)
* [Budget](https://gitlab.com/fair-fare-senior-design/fair-fare/wikis/Budget)
* [Appendix](https://gitlab.com/fair-fare-senior-design/fair-fare/wikis/Appendix)
* [User Guide](https://gitlab.com/fair-fare-senior-design/fair-fare/blob/master/assignments/Fair-Fare-User-Guide.md)