import React from 'react';
import $ from 'jquery';

import AppTemplate from './AppTemplate';

class App extends React.Component {

  componentDidMount() {
    $(document).ready(function () {
      $('.js-scroll-down').click(function () {
        var section = $(this).closest('.section').next();
        var app = $(this).closest('.app');
        app.animate(
          { scrollTop: app.scrollTop() + section.offset().top },
          700
        );
      });

      $('.js-scroll-up').click(function () {
        var section = $(this).closest('.section').prev();
        var app = $(this).closest('.app');
        app.animate(
          { scrollTop: app.scrollTop() + section.offset().top },
          700
        );
      });
    });
  }

  render() {
    return AppTemplate();
  }
};

export default App;
