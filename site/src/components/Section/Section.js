import React from 'react';

import SectionTemplate from './SectionTemplate';

class Section extends React.Component {

  render() {
    const options = {
      contents: this.props.contents
    };

    return SectionTemplate(options);
  }
};

export default Section;
