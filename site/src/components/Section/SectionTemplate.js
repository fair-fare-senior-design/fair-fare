import React from 'react';

import './Section.css';

export default function(options) {
  return (
    <div className="app__section__content">
      <div className="app__section__title">{options.contents.title}</div>
      <div className="app__section__body">{options.contents.body}</div>
    </div>
  );
};
