import React from 'react';

import './Navigation.css';

export default function(options) {
  return (
    <div className="navigation">
      <div className="navigation__title">Fair Fare</div>
      <div className="navigation__links">
        <div
          className="navigation__link"
          id="js-mission-statement"
          onClick={(e) => options.scrollToSection(e, 1)}
        >
          Mission Statement
        </div>
        <div
          className="navigation__link"
          id="js-rating-algorithm"
          onClick={(e) => options.scrollToSection(e, 2)}
        >
          Rating Algorithm
        </div>
        <div
          className="navigation__link"
          id="js-data-sources"
          onClick={(e) => options.scrollToSection(e, 3)}
        >
          Data Sources
        </div>
        <div
          className="navigation__link"
          id="js-project-members"
          onClick={(e) => options.scrollToSection(e, 4)}
        >
          Project Members
        </div>
      </div>
    </div>
  );
};
