import React from 'react';

import NavigationTemplate from './NavigationTemplate';

class Navigation extends React.Component {

  componentDidMount() {}

  scrollToSection = (e, num) => {
    // Change active tab color
    let links = document.getElementsByClassName('navigation__link');
    for (let i = 0; i < links.length; i++) {
      links[i].className = links[i].className.replace('active', '');
    }

    document.getElementById(e.target.id).classList.add('active');

    let id = 'js-section-' + num;
    document.getElementById(id).scrollIntoView(
      {
        block: 'start',
        behavior: 'smooth'
      }
    );
  };

  render() {
    const options = {
      scrollToSection: this.scrollToSection
    };

    return NavigationTemplate(options);
  }
};

export default Navigation;
