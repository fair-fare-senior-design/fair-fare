import React from 'react';

import ContentPaneTemplate from './ContentPaneTemplate';

class ContentPane extends React.Component {

  componentDidMount() {}

  componentWillUnmount() {}

  render() {
    const options = {
      sectionOne: {
        title: 'Mission Statement',
        body: 'This is our mission statement.'
      },
      sectionTwo: {
        title: 'Rating Algorithm',
        body: 'This is our rating algorithm.'
      },
      sectionThree: {
        title: 'Data Sources',
        body: 'These are our data sources.'
      },
      sectionFour: {
        title: 'Project Members',
        body: 'These are the project members.'
      }
    };

    return ContentPaneTemplate(options);
  }
};

export default ContentPane;
