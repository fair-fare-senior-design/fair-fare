import React from 'react';

import Section from '../Section/Section';
import './ContentPane.css';

export default function(options) {
  return (
    <div className="content__pane">
      <div className="content__section" id="js-section-1">
        <Section contents={options.sectionOne} />
      </div>
      <div className="content__section" id="js-section-2">
        <Section contents={options.sectionTwo} />
      </div>
      <div className="content__section" id="js-section-3">
        <Section contents={options.sectionThree} />
      </div>
      <div className="content__section" id="js-section-4">
        <Section contents={options.sectionFour} />
      </div>
    </div>
  );
};
