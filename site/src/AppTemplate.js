import React from 'react';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';

import './App.css';

export default function(options) {

  return (
    <div className="app">
      <div className="section one">
        <div className="section__body">
          <div className="app__title">
            <div className="app__title__text">Fair Fare</div>
          </div>
          <div className="app__content">CONSUME WITH CARE</div>
          <div className="section__icon js-scroll-down">
            <KeyboardArrowDownIcon style={{ fontSize: 125, color: 'rgb(242, 239, 234)', opacity: 0.5 }} />
          </div>
        </div>
      </div>
      <div className="section two">
        <div className="section__body">
          <div className="section__icon js-scroll-up">
            <KeyboardArrowUpIcon style={{ fontSize: 125, color: 'rgb(67, 110, 79)', opacity: 0.5 }} />
          </div>
          <div className="title">MISSION STATEMENT</div>
          <div className="content">
            Labels on certain products are often misleading to consumers
            in areas such as ethical sourcing and environmental impact. We are
            empowering consumers to make educated choices by converting
            the overabundance of data into clear and concise food choices.
            <br/>
            <br/>
            The industries behind meat, fish, and dairy products have a
            significant impact on the environment, and they often raise ethical
            concerns. Consumers deserve to know where their products originate
            and how they are manufactured.
          </div>
          <div className="section__icon js-scroll-down">
            <KeyboardArrowDownIcon style={{ fontSize: 125, color: 'rgb(67, 110, 79)', opacity: 0.5 }} />
          </div>
        </div>
      </div>
      <div className="section three">
        <div className="section__body">
          <div className="section__icon js-scroll-up">
            <KeyboardArrowUpIcon style={{ fontSize: 125, color: 'rgb(242, 239, 234)', opacity: 0.5 }} />
          </div>
          <div className="title">WHAT IS FAIR FARE</div>
          <div className="content">
          Fair Fare is a mobile application that promotes conscious consumerism
          by putting knowledge of companies’ ethics along with the environmental
          impact and nutritional value of their products all in the in user’s
          hands.
          <br/>
          <br/>
          Our custom algorithm provides users with a product score out of ten.
          This score contains three factors: company ethics, environmental impact,
          and nutritional value. Each of these three categories is then broken
          into smaller sub-categories.
          <br/>
          <br/>
          The ethical score is a factor of animal living conditions, feed, and
          the level to which the products are modified. The environmental impact
          is based on the product’s water consumption, land use, and carbon
          emission. The nutritional value is a combination of a product’s
          macro-nutrients and the vitamins and minerals it provides.
          </div>
          <div className="section__icon js-scroll-down">
            <KeyboardArrowDownIcon style={{ fontSize: 125, color: 'rgb(242, 239, 234)', opacity: 0.5 }} />
          </div>
        </div>
      </div>
      <div className="section four">
        <div className="section__body">
          <div className="section__icon js-scroll-up">
            <KeyboardArrowUpIcon style={{ fontSize: 125, color: 'rgb(245, 173, 129)', opacity: 0.5 }} />
          </div>
          <div className="title">THE IMPORTANCE</div>
          <div className="content">
            In recent years, you may have heard about the ongoing debate of the
            environmental impact of meat. It is true that livestock farming and meat
            manufacturing plays a vital role in greenhouse gas emissions and natural
            resource consumption. Commercial livestock farming that has become
            commonplace because of its low cost and claims to efficiency not only
            raises environmental concerns, but also concerns of health and ethics.
            <br/>
            <br/>
            As consumers, we have the power to seek clarity and take action to
            positively impact the environment, our health, and our morals by choosing
            respectable products.
            <br/>
            <br/>
            <div className="quote">
            “Pigs were never meant to live on concrete never to feel the light of
            the sun on their backs. Never to graze on green plants or root in the mud.”
            </div>
          </div>
        </div>
      </div>
      <div className="section nine">
        <div className="section__body">
          <div className="content">
            <div className="quote">
            - Danny Losekamp
            <br/>
            <br/>
            </div>
            Labels implying ethical standards are often misleading and lack any
            federal regulations at all. Danny Losekamp, Livestock and Pastures
            Manager at a local farm speaks candidly on the commercial meat industry.
            He says, “The notion that we can increase ‘efficiency’ and keep food
            prices cheap by ignoring [an animal’s] natural habits has not only
            resulted in poor animal welfare but also a public health crisis.” As
            consumers, it is our duty to ensure that we are investing in responsible products.
            <br/>
            <br/>
            There is such a thing as sustainable, ethical, and nutritious meat -
            it is just a matter of sorting through claims, marketing language,
            and labels to find it. FairFare is here to help by providing an easier
            way to shop with conscious intent without being met by prevarication along the way.
          </div>
          <div className="section__icon js-scroll-down">
            <KeyboardArrowDownIcon style={{ fontSize: 125, color: 'rgb(245, 173, 129)', opacity: 0.5 }} />
          </div>
        </div>
      </div>
      <div className="section five">
        <div className="section__body">
          <div className="section__icon js-scroll-up">
            <KeyboardArrowUpIcon style={{ fontSize: 125, color: 'rgb(245, 173, 129)', opacity: 0.5 }} />
          </div>
          <div className="title">REACHING HIGHER</div>
          <div className="content">
            So the question remains, what is the most sustainable, ethical, and
            nutritious form of meat? What kind of meat is trustworthy enough to invest in?
            <br/>
            <br/>
            After intensive research and expert interviews, regenerative meat
            seems to be the answer. Regenerative grazing is a proper way to raise
            livestock. It involves appropriate rotation of livestock from one
            pasture to the next, ensuring that the grass has sufficient time and
            resources for regrowth in between animal grazing. 
            <br/>
            <br/>
            White Oak Pastures, a leader in responsible farming, conducted a study
            to show the environmental impacts of regenerative meat. In the study,
            they broke down emissions of all farm activities to calculate a
            comprehensive carbon footprint of their beef. The net total carbon
            emissions was -3.5 Kg CO2-eq emissions per Kg.
          </div>
        </div>
      </div>
      <div className="section ten">
        <div className="section__body">
          <div className="content">
            <div className="quote">
            Yes, that is carbon negative!
            </div>
            <br/>
            <br/>
            By allowing the grass to regrow, they are able to sequester carbon
            dioxide from the atmosphere and put it into the ground. This is incredibly
            progressive, especially when compared to the net total of 33 Kg CO2-eq
            emissions per Kg of conventional beef - that is a 111% decrease in carbon emissions! 
            <br/>
            <br/>
            In addition to environmental benefits, this beef is also healthier
            than conventional beef with much richer content of vitamin and minerals,
            and more ethical as it allows cows to live the best life possible while
            comfortably grazing. By promoting conscious consumerism, we hope to
            eventually see a higher demand and thus higher supply of regenerative meat in the future.
          </div>
          <div className="section__icon js-scroll-up">
            <KeyboardArrowDownIcon style={{ fontSize: 125, color: 'rgb(245, 173, 129)', opacity: 0.5 }} />
          </div>
        </div>
      </div>
      <div className="section six">
        <div className="section__body">
          <div className="section__icon js-scroll-up">
            <KeyboardArrowUpIcon style={{ fontSize: 125, color: 'rgb(19, 19, 19)', opacity: 0.5 }} />
          </div>
          <div className="title">HOW TO HELP</div>
          <div className="content">
          You can do your part by becoming a conscious consumer. It's is easy and
          Fair Fare is there to help you! We know that not every person values
          the same things in their products. While the app provides you with an
          overall score, feel free to use the breakdown scoring to suit your own
          personal needs.
          <br/>
          <div className="quote">
          “There is an untold renaissance happening in America. Be a part of it.
          If you are unsure, let the soil guide you.”
          <br/>
          <br/>
          - Danny Losekamp
          </div>
          <br/>
          Encourage others to join your journey! With the social aspect of the
          app, you and your friends can stay in tune with what others are favoriting.
          The road to consumerism is a life long one as your priorities may change,
          but Fair Fare will support you the whole way there!
          </div>
          <div className="section__icon js-scroll-down">
            <KeyboardArrowDownIcon style={{ fontSize: 125, color: 'rgb(19, 19, 19)', opacity: 0.5 }} />
          </div>
        </div>
      </div>
      <div className="section seven">
        <div className="section__body">
          <div className="title">TEAM MEMBERS</div>
          <div className="content">
            <img className="member__cutouts" src="/images/Team-01.png" alt="Team" />
            <div className="member__block"></div>
          </div>
          <div className="section__arrows">
            <div className="section__icon up js-scroll-up">
              <KeyboardArrowUpIcon style={{ fontSize: 125, color: 'rgb(242, 239, 234)', opacity: 0.5 }} />
            </div>
            <div className="section__icon down js-scroll-down">
              <KeyboardArrowDownIcon style={{ fontSize: 125, color: 'rgb(242, 239, 234)', opacity: 0.5 }} />
            </div>
          </div>
        </div>
      </div>
      <div className="section eight">
        <div className="section__body">
          <div className="section__icon js-scroll-up">
            <KeyboardArrowUpIcon style={{ fontSize: 125, color: 'rgb(19, 19, 19)', opacity: 0.5 }} />
          </div>
          <div className="title">DOWNLOAD THE APP</div>
          <div className="left_content">
            Simplify your grocery routine and support ethical and environmentally
            friendly companies with easy product comparisons.  Join FairFare and
            become the consumer you want to be.
          </div>
          <div className="contents">
            <img className="screen_shots" src="/images/ScreenShotFinal.png" alt="Screen shots"/>
          </div>
          <div className="contents">
            <img className="app_button" src="/images/AppStore.png" alt="Screen shots"/>
          </div>
        </div>
      </div>
    </div>
  );
};
